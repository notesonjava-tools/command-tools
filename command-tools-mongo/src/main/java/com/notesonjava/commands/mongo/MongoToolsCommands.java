package com.notesonjava.commands.mongo;

import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.CommandRunner;

public class MongoToolsCommands {
	
	public static CommandResult importJsonArray(String fileName, String db, String collection) {
		return CommandRunner.execute("mongoimport", "--jsonArray","--db", db, "--collection", collection, "--file", fileName);	
	}
	
	public static CommandResult importJsonArray(String fileName, String host, String db, String collection) {
		return CommandRunner.execute("mongoimport", "--host", host, "--jsonArray","--db", "verbs", "--collection", collection, "--file", fileName);	
	}
	
	public static CommandResult importJsonArray(String fileName, String host, int port) {
		return CommandRunner.execute("mongoimport", "--host", host, "--port", Integer.toString(port), "--jsonArray","--file", fileName);	
	}
	
	public static CommandResult restore(String directory) {
		return CommandRunner.execute("mongorestore", directory);	
	}
	
	public static CommandResult binaryDump(String directory) {
		return CommandRunner.execute("mongodump", "--out", directory);	
	}
	
	public static CommandResult archiveDump(String file) {
		return CommandRunner.execute("mongodump", "--archive="+file, "--gzip");
	}
	
	public static CommandResult archiveDump(String archiveName, String host, int port) {
		return CommandRunner.execute("mongodump", "--host", host,"--port", Integer.toString(port),"--archive="+archiveName, "--gzip");	
		
	}
	
	public static CommandResult restoreArchive(String archive, String host) {
		return CommandRunner.execute("mongorestore", "--host", host, "--archive="+archive, "--gzip");	
		
	}
	
	public static CommandResult restoreArchive(String archive, String host, int port) {
		return CommandRunner.execute("mongorestore", "--host", host, "--port", Integer.toString(port), "--archive="+archive, "--gzip");	
		
	}
	
	public static CommandResult bsonDump(String directory) {
		return CommandRunner.execute("bsondump", "--out", directory);	
	}
}
