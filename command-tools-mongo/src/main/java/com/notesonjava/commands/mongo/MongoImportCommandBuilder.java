package com.notesonjava.commands.mongo;

import java.util.ArrayList;
import java.util.List;


public class MongoImportCommandBuilder {

	
	private String host;
	private int port;
	private String db;
	private String collection;
	private String file;
	private boolean dropBefore;
	private boolean array;

	
	public MongoImportCommandBuilder host(String host) {
		this.host = host;
		return this;
	}
	
	public MongoImportCommandBuilder port(int port) {
		this.port = port;
		return this;
	}
	
	public MongoImportCommandBuilder file(String file) {
		this.file = file;
		return this;
	}
	
	public MongoImportCommandBuilder database(String database) {
		this.db = database;
		return this;
	}
	
	public MongoImportCommandBuilder collection(String collection) {
		this.collection = collection;
		return this;
	}
	
	public MongoImportCommandBuilder dropBefore(boolean drop) {
		this.dropBefore = drop;
		return this;
	}
	
	
	public MongoImportCommandBuilder isJsonArray(boolean isJsonArray) {
		this.array = isJsonArray;
		return this;
	}
	
	
	// mongoimport --host mongodb --jsonArray --db verbs --collection verbs --drop --file converted.json

	public String[] build() {
		List<String> list = new ArrayList<>();
		list.add("mongoimport");
		if(host != null) {
			list.add("--host");
			list.add(host);
		}
		if(port>0) {
			list.add("--port");
			list.add(String.valueOf(port));
		}
		if(array) {
			list.add("--jsonArray");
		}
		if(db != null) {
			list.add("--db");
			list.add(db);
		}
		if(collection != null) {
			list.add("--collection");
			list.add(collection);
		}
		if(dropBefore) {
			list.add("--drop");
		}
		
		if(file != null) {
			list.add("--file");
			list.add(file);
		}
		return list.toArray(new String[list.size()]);
	}
}
