package com.notesonjava.commands.mongo;

import java.util.ArrayList;
import java.util.List;

public class MongoRestoreCommandBuilder {
	
	private String host;
	private int port;
	private String db;
	private String collection;
	private String directory;
	
	
	public MongoRestoreCommandBuilder host(String host) {
		this.host = host;
		return this;
	}
	

	public MongoRestoreCommandBuilder port(int port) {
		this.port = port;
		return this;
	}
	
	public MongoRestoreCommandBuilder database(String database) {
		this.db = database;
		return this;
	}
	
	public MongoRestoreCommandBuilder collection(String collection) {
		this.collection = collection;
		return this;
	}

	public MongoRestoreCommandBuilder directory(String directory) {
		this.directory = directory;
		return this;
	}

	
	//mongorestore --collection people --db accounts dump/
	
	public String[] build() {
		List<String> list = new ArrayList<>();
		list.add("mongoimport");
		if(host != null) {
			list.add("--host");
			list.add(host);
		}
		if(port>0) {
			list.add("--port");
			list.add(String.valueOf(port));
		}
		
		if(db != null) {
			list.add("--db");
			list.add(db);
		}
		if(collection != null) {
			list.add("--collection");
			list.add(collection);
		}
		
		if(directory != null) {
			list.add(directory);
		}
		return list.toArray(new String[list.size()]);
	}

}
