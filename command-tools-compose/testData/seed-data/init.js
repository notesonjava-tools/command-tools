db.verbs.insertMany([ {
  "spanish" : "ir",
  "translations" : {
    "ENGLISH" : [ "go" ],
    "FRENCH" : [ "aller" ]
  },
  "regular" : false,
  "views" : 1112016
}, {
  "spanish" : "ser",
  "translations" : {
    "ENGLISH" : [ "be" ],
    "FRENCH" : [ "être" ]
  },
  "regular" : false,
  "views" : 164181
}, {
  "spanish" : "tener",
  "translations" : {
    "ENGLISH" : [ "have" ],
    "FRENCH" : [ "avoir" ]
  },
  "regular" : false,
  "views" : 148838
}, {
  "spanish" : "hacer",
  "translations" : {
    "ENGLISH" : [ "do", "make" ],
    "FRENCH" : [ "faire" ]
  },
  "regular" : false,
  "views" : 147560
}, {
  "spanish" : "estar",
  "translations" : {
    "ENGLISH" : [ "be" ],
    "FRENCH" : [ "être" ]
  },
  "regular" : false,
  "views" : 138188
}, {
  "spanish" : "decir",
  "translations" : {
    "ENGLISH" : [ "tell", "say" ],
    "FRENCH" : [ "dire" ]
  },
  "regular" : false,
  "views" : 133814
}, {
  "spanish" : "ver",
  "translations" : {
    "ENGLISH" : [ "see" ],
    "FRENCH" : [ "voir" ]
  },
  "regular" : false,
  "views" : 133258
}, {
  "spanish" : "haber",
  "translations" : {
    "ENGLISH" : [ "have" ],
    "FRENCH" : [ "avoir" ]
  },
  "regular" : false,
  "views" : 30490
}, {
  "spanish" : "poder",
  "translations" : {
    "ENGLISH" : [ "be able to" ],
    "FRENCH" : [ "pouvoir" ]
  },
  "regular" : false,
  "views" : 129924
}, {
  "spanish" : "dar",
  "translations" : {
    "ENGLISH" : [ "give" ],
    "FRENCH" : [ "donner" ]
  },
  "regular" : false,
  "views" : 29851
}, {
  "spanish" : "hablar",
  "translations" : {
    "ENGLISH" : [ "speak" ],
    "FRENCH" : [ "parler" ]
  },
  "regular" : true,
  "views" : 128994
}, {
  "spanish" : "querer",
  "translations" : {
    "ENGLISH" : [ "like", "want" ],
    "FRENCH" : [ "vouloir" ]
  },
  "regular" : false,
  "views" : 127588
}, {
  "spanish" : "venir",
  "translations" : {
    "ENGLISH" : [ "come" ],
    "FRENCH" : [ "venir" ]
  },
  "regular" : false,
  "views" : 26270
}, {
  "spanish" : "comer",
  "translations" : {
    "ENGLISH" : [ "eat" ],
    "FRENCH" : [ "manger" ]
  },
  "regular" : true,
  "views" : 124771
}, {
  "spanish" : "saber",
  "translations" : {
    "ENGLISH" : [ "know" ],
    "FRENCH" : [ "savoir" ]
  },
  "regular" : false,
  "views" : 124162
}, {
  "spanish" : "poner",
  "translations" : {
    "ENGLISH" : [ "put" ],
    "FRENCH" : [ "mettre" ]
  },
  "regular" : false,
  "views" : 123861
}, {
  "spanish" : "salir",
  "translations" : {
    "ENGLISH" : [ "leave", "go out" ],
    "FRENCH" : [ "sortir" ]
  },
  "regular" : false,
  "views" : 122234
}, {
  "spanish" : "leer",
  "translations" : {
    "ENGLISH" : [ "read" ],
    "FRENCH" : [ "lire" ]
  },
  "regular" : false,
  "views" : 120727
}, {
  "spanish" : "dormir",
  "translations" : {
    "ENGLISH" : [ "sleep" ],
    "FRENCH" : [ "dormir" ]
  },
  "regular" : false,
  "views" : 119870
}, {
  "spanish" : "volver",
  "translations" : {
    "ENGLISH" : [ "return" ],
    "FRENCH" : [ "retourner" ]
  },
  "regular" : false,
  "views" : 18789
}, {
  "spanish" : "pedir",
  "translations" : {
    "ENGLISH" : [ "ask", "ask for" ],
    "FRENCH" : [ "demander" ]
  },
  "regular" : false,
  "views" : 115178
}, {
  "spanish" : "vivir",
  "translations" : {
    "ENGLISH" : [ "live" ],
    "FRENCH" : [ "vivre" ]
  },
  "regular" : true,
  "views" : 113665
}, {
  "spanish" : "traer",
  "translations" : {
    "ENGLISH" : [ "bring" ],
    "FRENCH" : [ "apporter" ]
  },
  "regular" : false,
  "views" : 112771
}, {
  "spanish" : "jugar",
  "translations" : {
    "ENGLISH" : [ "play" ],
    "FRENCH" : [ "jouer" ]
  },
  "regular" : false,
  "views" : 111729
}, {
  "spanish" : "llevar",
  "translations" : {
    "ENGLISH" : [ "take", "wear", "carry" ],
    "FRENCH" : [ "porter" ]
  },
  "regular" : true,
  "views" : 11626
}, {
  "spanish" : "llegar",
  "translations" : {
    "ENGLISH" : [ "arrive" ],
    "FRENCH" : [ "arriver" ]
  },
  "regular" : false,
  "views" : 11614
}, {
  "spanish" : "oir",
  "translations" : {
    "ENGLISH" : [ "hear" ],
    "FRENCH" : [ "écouter" ]
  },
  "regular" : false,
  "views" : 111560
}, {
  "spanish" : "tomar",
  "translations" : {
    "ENGLISH" : [ "take" ],
    "FRENCH" : [ "prendre" ]
  },
  "regular" : true,
  "views" : 110187
}, {
  "spanish" : "empezar",
  "translations" : {
    "ENGLISH" : [ "start", "begin" ],
    "FRENCH" : [ "commencer" ]
  },
  "regular" : false,
  "views" : 10100
}, {
  "spanish" : "conocer",
  "translations" : {
    "ENGLISH" : [ "know" ],
    "FRENCH" : [ "connaître" ]
  },
  "regular" : false,
  "views" : 19879
}, {
  "spanish" : "seguir",
  "translations" : {
    "ENGLISH" : [ "follow" ],
    "FRENCH" : [ "suivre" ]
  },
  "regular" : false,
  "views" : 9789
}, {
  "spanish" : "escribir",
  "translations" : {
    "ENGLISH" : [ "write" ],
    "FRENCH" : [ "écrire" ]
  },
  "regular" : false,
  "views" : 9546
}, {
  "spanish" : "pensar",
  "translations" : {
    "ENGLISH" : [ "think" ],
    "FRENCH" : [ "penser" ]
  },
  "regular" : false,
  "views" : 19102
}, {
  "spanish" : "sentir",
  "translations" : {
    "ENGLISH" : [ "feel" ],
    "FRENCH" : [ "sentir" ]
  },
  "regular" : false,
  "views" : 19085
}, {
  "spanish" : "comprar",
  "translations" : {
    "ENGLISH" : [ "buy" ],
    "FRENCH" : [ "acheter" ]
  },
  "regular" : true,
  "views" : 19008
}, {
  "spanish" : "beber",
  "translations" : {
    "ENGLISH" : [ "drink" ],
    "FRENCH" : [ "boire" ]
  },
  "regular" : true,
  "views" : 18857
}, {
  "spanish" : "abrir",
  "translations" : {
    "ENGLISH" : [ "open" ],
    "FRENCH" : [ "ouvrir" ]
  },
  "regular" : false,
  "views" : 18717
}, {
  "spanish" : "gustar",
  "translations" : {
    "ENGLISH" : [ "like" ],
    "FRENCH" : [ "aimer" ]
  },
  "regular" : true,
  "views" : 8516
}, {
  "spanish" : "caer",
  "translations" : {
    "ENGLISH" : [ "fall" ],
    "FRENCH" : [ "tomber" ]
  },
  "regular" : false,
  "views" : 18048
}, {
  "spanish" : "quejar",
  "translations" : {
    "ENGLISH" : [ "complain" ],
    "FRENCH" : [ "plaindre" ]
  },
  "regular" : true,
  "views" : 17966
}, {
  "spanish" : "cerrar",
  "translations" : {
    "ENGLISH" : [ "close" ],
    "FRENCH" : [ "fermer" ]
  },
  "regular" : false,
  "views" : 17894
}, {
  "spanish" : "creer",
  "translations" : {
    "ENGLISH" : [ "think", "believe" ],
    "FRENCH" : [ "croire" ]
  },
  "regular" : false,
  "views" : 17702
}, {
  "spanish" : "encontrar",
  "translations" : {
    "ENGLISH" : [ "find", "encounter" ],
    "FRENCH" : [ "trouver" ]
  },
  "regular" : false,
  "views" : 17689
}, {
  "spanish" : "preferir",
  "translations" : {
    "ENGLISH" : [ "prefer" ],
    "FRENCH" : [ "préférer" ]
  },
  "regular" : false,
  "views" : 7606
}, {
  "spanish" : "perder",
  "translations" : {
    "ENGLISH" : [ "lose" ],
    "FRENCH" : [ "perdre" ]
  },
  "regular" : false,
  "views" : 17597
}, {
  "spanish" : "quedar",
  "translations" : {
    "ENGLISH" : [ "meet", "stay" ],
    "FRENCH" : [ "rester" ]
  },
  "regular" : true,
  "views" : 7331
}, {
  "spanish" : "morir",
  "translations" : {
    "ENGLISH" : [ "die" ],
    "FRENCH" : [ "mourir" ]
  },
  "regular" : false,
  "views" : 7246
}, {
  "spanish" : "servir",
  "translations" : {
    "ENGLISH" : [ "serve" ],
    "FRENCH" : [ "servir" ]
  },
  "regular" : false,
  "views" : 7194
}, {
  "spanish" : "buscar",
  "translations" : {
    "ENGLISH" : [ "look for" ],
    "FRENCH" : [ "chercher" ]
  },
  "regular" : false,
  "views" : 16908
}, {
  "spanish" : "estudiar",
  "translations" : {
    "ENGLISH" : [ "study" ],
    "FRENCH" : [ "étudier" ]
  },
  "regular" : true,
  "views" : 16777
}, {
  "spanish" : "trabajar",
  "translations" : {
    "ENGLISH" : [ "work" ],
    "FRENCH" : [ "travailler" ]
  },
  "regular" : true,
  "views" : 16700
}, {
  "spanish" : "sentar",
  "translations" : {
    "ENGLISH" : [ "seat", "sit" ],
    "FRENCH" : [ "assoir" ]
  },
  "regular" : false,
  "views" : 16592
}, {
  "spanish" : "conducir",
  "translations" : {
    "ENGLISH" : [ "drive" ],
    "FRENCH" : [ "conduire" ]
  },
  "regular" : false,
  "views" : 16412
}, {
  "spanish" : "reir",
  "translations" : {
    "ENGLISH" : [ "laugh" ],
    "FRENCH" : [ "rire" ]
  },
  "regular" : false,
  "views" : 6315
}, {
  "spanish" : "comenzar",
  "translations" : {
    "ENGLISH" : [ "start", "begin" ],
    "FRENCH" : [ "commencer" ]
  },
  "regular" : false,
  "views" : 16278
}, {
  "spanish" : "sacar",
  "translations" : {
    "ENGLISH" : [ "take out" ],
    "FRENCH" : [ "sortir(un objet)" ]
  },
  "regular" : false,
  "views" : 6196
}, {
  "spanish" : "aprender",
  "translations" : {
    "ENGLISH" : [ "learn" ],
    "FRENCH" : [ "apprendre" ]
  },
  "regular" : true,
  "views" : 16141
}, {
  "spanish" : "llamar",
  "translations" : {
    "ENGLISH" : [ "call", "call oneself" ],
    "FRENCH" : [ "appeler" ]
  },
  "regular" : true,
  "views" : 6014
}, {
  "spanish" : "pagar",
  "translations" : {
    "ENGLISH" : [ "pay" ],
    "FRENCH" : [ "payer" ]
  },
  "regular" : false,
  "views" : 15976
}, {
  "spanish" : "correr",
  "translations" : {
    "ENGLISH" : [ "run" ],
    "FRENCH" : [ "courir" ]
  },
  "regular" : true,
  "views" : 15949
}, {
  "spanish" : "limpiar",
  "translations" : {
    "ENGLISH" : [ "clean" ],
    "FRENCH" : [ "nettoyer" ]
  },
  "regular" : true,
  "views" : 15928
}, {
  "spanish" : "andar",
  "translations" : {
    "ENGLISH" : [ "walk" ],
    "FRENCH" : [ "marcher" ]
  },
  "regular" : false,
  "views" : 5924
}, {
  "spanish" : "pasar",
  "translations" : {
    "ENGLISH" : [ "pass" ],
    "FRENCH" : [ "passer" ]
  },
  "regular" : true,
  "views" : 5921
}, {
  "spanish" : "bailar",
  "translations" : {
    "ENGLISH" : [ "dance" ],
    "FRENCH" : [ "danser" ]
  },
  "regular" : true,
  "views" : 15912
}, {
  "spanish" : "entender",
  "translations" : {
    "ENGLISH" : [ "understand" ],
    "FRENCH" : [ "comprendre" ]
  },
  "regular" : false,
  "views" : 15899
}, {
  "spanish" : "dejar",
  "translations" : {
    "ENGLISH" : [ "leave", "borrow", "lend" ],
    "FRENCH" : [ "laisser" ]
  },
  "regular" : true,
  "views" : 15799
}, {
  "spanish" : "vestir",
  "translations" : {
    "ENGLISH" : [ "dress" ],
    "FRENCH" : [ "vêtir" ]
  },
  "regular" : false,
  "views" : 5778
}, {
  "spanish" : "terminar",
  "translations" : {
    "ENGLISH" : [ "finish", "end" ],
    "FRENCH" : [ "terminer" ]
  },
  "regular" : true,
  "views" : 15646
}, {
  "spanish" : "almorzar",
  "translations" : {
    "ENGLISH" : [ "have lunch" ],
    "FRENCH" : [ "diner", "déjeuner" ]
  },
  "regular" : false,
  "views" : 5617
}, {
  "spanish" : "visitar",
  "translations" : {
    "ENGLISH" : [ "visit" ],
    "FRENCH" : [ "visiter" ]
  },
  "regular" : true,
  "views" : 5574
}, {
  "spanish" : "llover",
  "translations" : {
    "ENGLISH" : [ "rain" ],
    "FRENCH" : [ "pleuvoir" ]
  },
  "regular" : false,
  "views" : 15552
}, {
  "spanish" : "divertir",
  "translations" : {
    "ENGLISH" : [ "entertain", "amuse" ],
    "FRENCH" : [ "divertire" ]
  },
  "regular" : false,
  "views" : 5506
}, {
  "spanish" : "conseguir",
  "translations" : {
    "ENGLISH" : [ "obtain" ],
    "FRENCH" : [ "obtenir" ]
  },
  "regular" : false,
  "views" : 5473
}, {
  "spanish" : "repetir",
  "translations" : {
    "ENGLISH" : [ "repeat" ],
    "FRENCH" : [ "répéter" ]
  },
  "regular" : false,
  "views" : 5452
}, {
  "spanish" : "caminar",
  "translations" : {
    "ENGLISH" : [ "walk" ],
    "FRENCH" : [ "marcher", "cheminer" ]
  },
  "regular" : true,
  "views" : 15392
}, {
  "spanish" : "esperar",
  "translations" : {
    "ENGLISH" : [ "wait", "hope" ],
    "FRENCH" : [ "espérer", "attendre" ]
  },
  "regular" : true,
  "views" : 15327
}, {
  "spanish" : "mirar",
  "translations" : {
    "ENGLISH" : [ "watch", "look at" ],
    "FRENCH" : [ "regarder" ]
  },
  "regular" : true,
  "views" : 15252
}, {
  "spanish" : "tocar",
  "translations" : {
    "ENGLISH" : [ "play an instrument", "touch" ],
    "FRENCH" : [ "toucher", "jouer un instrument" ]
  },
  "regular" : false,
  "views" : 15250
}, {
  "spanish" : "caber",
  "translations" : {
    "ENGLISH" : [ "fit" ],
    "FRENCH" : [ "adapter" ]
  },
  "regular" : false,
  "views" : 15228
}, {
  "spanish" : "deber",
  "translations" : {
    "ENGLISH" : [ "must" ],
    "FRENCH" : [ "devoir" ]
  },
  "regular" : true,
  "views" : 5225
}, {
  "spanish" : "elegir",
  "translations" : {
    "ENGLISH" : [ "choose" ],
    "FRENCH" : [ "choisir" ]
  },
  "regular" : false,
  "views" : 5214
}, {
  "spanish" : "cambiar",
  "translations" : {
    "ENGLISH" : [ "change" ],
    "FRENCH" : [ "changer" ]
  },
  "regular" : true,
  "views" : 15201
}, {
  "spanish" : "recibir",
  "translations" : {
    "ENGLISH" : [ "receive" ],
    "FRENCH" : [ "recevoir" ]
  },
  "regular" : true,
  "views" : 5111
}, {
  "spanish" : "acostar",
  "translations" : {
    "ENGLISH" : [ "go to bed" ],
    "FRENCH" : [ "coucher" ]
  },
  "regular" : false,
  "views" : 5090
}, {
  "spanish" : "levantar",
  "translations" : {
    "ENGLISH" : [ "raise" ],
    "FRENCH" : [ "élever" ]
  },
  "regular" : true,
  "views" : 5009
}, {
  "spanish" : "cantar",
  "translations" : {
    "ENGLISH" : [ "sing" ],
    "FRENCH" : [ "chanter" ]
  },
  "regular" : true,
  "views" : 14945
}, {
  "spanish" : "mover",
  "translations" : {
    "ENGLISH" : [ "move" ],
    "FRENCH" : [ "bouger" ]
  },
  "regular" : false,
  "views" : 4927
}, {
  "spanish" : "contar",
  "translations" : {
    "ENGLISH" : [ "tell", "count" ],
    "FRENCH" : [ "compter" ]
  },
  "regular" : false,
  "views" : 14814
}, {
  "spanish" : "ayudar",
  "translations" : {
    "ENGLISH" : [ "help" ],
    "FRENCH" : [ "aider" ]
  },
  "regular" : true,
  "views" : 4808
}, {
  "spanish" : "vender",
  "translations" : {
    "ENGLISH" : [ "sell" ],
    "FRENCH" : [ "vendre" ]
  },
  "regular" : true,
  "views" : 14666
}, {
  "spanish" : "viajar",
  "translations" : {
    "ENGLISH" : [ "travel" ],
    "FRENCH" : [ "voyager" ]
  },
  "regular" : true,
  "views" : 14648
}, {
  "spanish" : "escuchar",
  "translations" : {
    "ENGLISH" : [ "listen" ],
    "FRENCH" : [ "écouter" ]
  },
  "regular" : true,
  "views" : 14637
}, {
  "spanish" : "hervir",
  "translations" : {
    "ENGLISH" : [ "boil" ],
    "FRENCH" : [ "bouillir" ]
  },
  "regular" : false,
  "views" : 4605
}, {
  "spanish" : "coger",
  "translations" : {
    "ENGLISH" : [ "pick up", "take", "catch" ],
    "FRENCH" : [ "prendre" ]
  },
  "regular" : false,
  "views" : 4574
}, {
  "spanish" : "despertar",
  "translations" : {
    "ENGLISH" : [ "wake", "arouse" ],
    "FRENCH" : [ "réveiller" ]
  },
  "regular" : false,
  "views" : 14564
}, {
  "spanish" : "ganar",
  "translations" : {
    "ENGLISH" : [ "earn", "win" ],
    "FRENCH" : [ "gagner" ]
  },
  "regular" : true,
  "views" : 4555
}, {
  "spanish" : "necesitar",
  "translations" : {
    "ENGLISH" : [ "need" ],
    "FRENCH" : [ "avoir besoin" ]
  },
  "regular" : true,
  "views" : 14538
}, {
  "spanish" : "recordar",
  "translations" : {
    "ENGLISH" : [ "remember" ],
    "FRENCH" : [ "rappeler" ]
  },
  "regular" : false,
  "views" : 4537
}, {
  "spanish" : "huir",
  "translations" : {
    "ENGLISH" : [ "escape" ],
    "FRENCH" : [ "fuir" ]
  },
  "regular" : false,
  "views" : 4494
}, {
  "spanish" : "decidir",
  "translations" : {
    "ENGLISH" : [ "decide" ],
    "FRENCH" : [ "décider" ]
  },
  "regular" : true,
  "views" : 4479
}, {
  "spanish" : "desear",
  "translations" : {
    "ENGLISH" : [ "wish" ],
    "FRENCH" : [ "souhaiter" ]
  },
  "regular" : true,
  "views" : 4405
}, {
  "spanish" : "subir",
  "translations" : {
    "ENGLISH" : [ "come up", "go up" ],
    "FRENCH" : [ "remonter" ]
  },
  "regular" : true,
  "views" : 4358
}, {
  "spanish" : "parecer",
  "translations" : {
    "ENGLISH" : [ "look like", "seem" ],
    "FRENCH" : [ "paraître" ]
  },
  "regular" : false,
  "views" : 4346
}, {
  "spanish" : "nadar",
  "translations" : {
    "ENGLISH" : [ "swim" ],
    "FRENCH" : [ "nager" ]
  },
  "regular" : true,
  "views" : 14340
}, {
  "spanish" : "lavar",
  "translations" : {
    "ENGLISH" : [ "wash" ],
    "FRENCH" : [ "laver" ]
  },
  "regular" : true,
  "views" : 4329
}, {
  "spanish" : "traducir",
  "translations" : {
    "ENGLISH" : [ "translate" ],
    "FRENCH" : [ "traduire" ]
  },
  "regular" : false,
  "views" : 14323
}, {
  "spanish" : "despedir",
  "translations" : {
    "ENGLISH" : [ "fire" ],
    "FRENCH" : [ "rejeter" ]
  },
  "regular" : false,
  "views" : 4320
}, {
  "spanish" : "mentir",
  "translations" : {
    "ENGLISH" : [ "lie" ],
    "FRENCH" : [ "mentir" ]
  },
  "regular" : false,
  "views" : 4265
}, {
  "spanish" : "sonreir",
  "translations" : {
    "ENGLISH" : [ "smile" ],
    "FRENCH" : [ "sourire" ]
  },
  "regular" : false,
  "views" : 4263
}, {
  "spanish" : "herir",
  "translations" : {
    "ENGLISH" : [ "hurt" ],
    "FRENCH" : [ "blesser" ]
  },
  "regular" : false,
  "views" : 14160
}, {
  "spanish" : "practicar",
  "translations" : {
    "ENGLISH" : [ "practise" ],
    "FRENCH" : [ "pratiquer" ]
  },
  "regular" : false,
  "views" : 4159
}, {
  "spanish" : "usar",
  "translations" : {
    "ENGLISH" : [ "use" ],
    "FRENCH" : [ "user" ]
  },
  "regular" : true,
  "views" : 14142
}, {
  "spanish" : "casar",
  "translations" : {
    "ENGLISH" : [ "get married", "marry" ],
    "FRENCH" : [ "marier" ]
  },
  "regular" : true,
  "views" : 14124
}, {
  "spanish" : "romper",
  "translations" : {
    "ENGLISH" : [ "break" ],
    "FRENCH" : [ "briser" ]
  },
  "regular" : false,
  "views" : 14120
}, {
  "spanish" : "sugerir",
  "translations" : {
    "ENGLISH" : [ "suggest" ],
    "FRENCH" : [ "suggérer" ]
  },
  "regular" : false,
  "views" : 4115
}, {
  "spanish" : "ofrecer",
  "translations" : {
    "ENGLISH" : [ "offer" ],
    "FRENCH" : [ "offrir" ]
  },
  "regular" : false,
  "views" : 4041
}, {
  "spanish" : "desayunar",
  "translations" : {
    "ENGLISH" : [ "have breakfast" ],
    "FRENCH" : [ "petit déjeuner", "déjeuner" ]
  },
  "regular" : true,
  "views" : 4039
}, {
  "spanish" : "devolver",
  "translations" : {
    "ENGLISH" : [ "return" ],
    "FRENCH" : [ "retourner" ]
  },
  "regular" : false,
  "views" : 3919
}, {
  "spanish" : "oler",
  "translations" : {
    "ENGLISH" : [ "smell" ],
    "FRENCH" : [ "sentir" ]
  },
  "regular" : false,
  "views" : 3900
}, {
  "spanish" : "mandar",
  "translations" : {
    "ENGLISH" : [ "tell sb to do sth", "order" ],
    "FRENCH" : [ "envoyer" ]
  },
  "regular" : true,
  "views" : 3868
}, {
  "spanish" : "construir",
  "translations" : {
    "ENGLISH" : [ "manufacture", "build" ],
    "FRENCH" : [ "construire" ]
  },
  "regular" : false,
  "views" : 3858
}, {
  "spanish" : "cubrir",
  "translations" : {
    "ENGLISH" : [ "cover" ],
    "FRENCH" : [ "couvrir" ]
  },
  "regular" : false,
  "views" : 3854
}, {
  "spanish" : "mantener",
  "translations" : {
    "ENGLISH" : [ "maintain" ],
    "FRENCH" : [ "maintenir" ]
  },
  "regular" : false,
  "views" : 3853
}, {
  "spanish" : "hackear",
  "translations" : {
    "ENGLISH" : [ "hack" ],
    "FRENCH" : [ "pirater(hack)" ]
  },
  "regular" : true,
  "views" : 3836
}, {
  "spanish" : "comprender",
  "translations" : {
    "ENGLISH" : [ "understand" ],
    "FRENCH" : [ "comprendre" ]
  },
  "regular" : false,
  "views" : 3820
}, {
  "spanish" : "nacer",
  "translations" : {
    "ENGLISH" : [ "be born" ],
    "FRENCH" : [ "naître" ]
  },
  "regular" : false,
  "views" : 3812
}, {
  "spanish" : "olvidar",
  "translations" : {
    "ENGLISH" : [ "forget" ],
    "FRENCH" : [ "oublier" ]
  },
  "regular" : true,
  "views" : 13745
}, {
  "spanish" : "costar",
  "translations" : {
    "ENGLISH" : [ "cost" ],
    "FRENCH" : [ "coûter" ]
  },
  "regular" : false,
  "views" : 3740
}, {
  "spanish" : "enviar",
  "translations" : {
    "ENGLISH" : [ "send" ],
    "FRENCH" : [ "envoyer" ]
  },
  "regular" : false,
  "views" : 13738
}, {
  "spanish" : "asistir",
  "translations" : {
    "ENGLISH" : [ "attend", "assist" ],
    "FRENCH" : [ "assister" ]
  },
  "regular" : true,
  "views" : 3710
}, {
  "spanish" : "preparar",
  "translations" : {
    "ENGLISH" : [ "prepare" ],
    "FRENCH" : [ "préparer" ]
  },
  "regular" : true,
  "views" : 3708
}, {
  "spanish" : "abanderar",
  "translations" : {
    "ENGLISH" : [ "register" ],
    "FRENCH" : [ "enregistrer(champion??)" ]
  },
  "regular" : true,
  "views" : 3708
}, {
  "spanish" : "recomendar",
  "translations" : {
    "ENGLISH" : [ "recommend" ],
    "FRENCH" : [ "recommender" ]
  },
  "regular" : false,
  "views" : 3680
}, {
  "spanish" : "cocinar",
  "translations" : {
    "ENGLISH" : [ "cook" ],
    "FRENCH" : [ "cuisiner" ]
  },
  "regular" : true,
  "views" : 3662
}, {
  "spanish" : "preguntar",
  "translations" : {
    "ENGLISH" : [ "ask" ],
    "FRENCH" : [ "demander" ]
  },
  "regular" : true,
  "views" : 13596
}, {
  "spanish" : "compartir",
  "translations" : {
    "ENGLISH" : [ "share" ],
    "FRENCH" : [ "partager" ]
  },
  "regular" : true,
  "views" : 3588
}, {
  "spanish" : "resolver",
  "translations" : {
    "ENGLISH" : [ "resolve" ],
    "FRENCH" : [ "résoudre" ]
  },
  "regular" : false,
  "views" : 3565
}, {
  "spanish" : "recoger",
  "translations" : {
    "ENGLISH" : [ "pick up" ],
    "FRENCH" : [ "ranger", "ramasser" ]
  },
  "regular" : false,
  "views" : 3562
}, {
  "spanish" : "permitir",
  "translations" : {
    "ENGLISH" : [ "allow" ],
    "FRENCH" : [ "permettre" ]
  },
  "regular" : true,
  "views" : 3540
}, {
  "spanish" : "cuidar",
  "translations" : {
    "ENGLISH" : [ "care for", "look after" ],
    "FRENCH" : [ "faire attention", "garder", "préoccuper" ]
  },
  "regular" : true,
  "views" : 3529
}, {
  "spanish" : "mostrar",
  "translations" : {
    "ENGLISH" : [ "show" ],
    "FRENCH" : [ "montrer" ]
  },
  "regular" : false,
  "views" : 3501
}, {
  "spanish" : "callar",
  "translations" : {
    "ENGLISH" : [ "stop talking", "go quiet" ],
    "FRENCH" : [ "taire" ]
  },
  "regular" : true,
  "views" : 3483
}, {
  "spanish" : "helar",
  "translations" : {
    "ENGLISH" : [ "freeze" ],
    "FRENCH" : [ "geler" ]
  },
  "regular" : false,
  "views" : 3435
}, {
  "spanish" : "sonar",
  "translations" : {
    "ENGLISH" : [ "sound" ],
    "FRENCH" : [ "sonner" ]
  },
  "regular" : false,
  "views" : 3414
}, {
  "spanish" : "escoger",
  "translations" : {
    "ENGLISH" : [ "choose" ],
    "FRENCH" : [ "choisir" ]
  },
  "regular" : false,
  "views" : 3404
}, {
  "spanish" : "henchir",
  "translations" : {
    "ENGLISH" : [ "fillup", "fill" ],
    "FRENCH" : [ "remplir" ]
  },
  "regular" : false,
  "views" : 3380
}, {
  "spanish" : "rogar",
  "translations" : {
    "ENGLISH" : [ "beg" ],
    "FRENCH" : [ "quêter" ]
  },
  "regular" : false,
  "views" : 3366
}, {
  "spanish" : "habituar",
  "translations" : {
    "ENGLISH" : [ "get used to" ],
    "FRENCH" : [ "habituer" ]
  },
  "regular" : false,
  "views" : 3365
}, {
  "spanish" : "exigir",
  "translations" : {
    "ENGLISH" : [ "require", "demand" ],
    "FRENCH" : [ "exiger" ]
  },
  "regular" : false,
  "views" : 3360
}, {
  "spanish" : "doler",
  "translations" : {
    "ENGLISH" : [ "hurt" ],
    "FRENCH" : [ "blesser" ]
  },
  "regular" : false,
  "views" : 3353
}, {
  "spanish" : "volar",
  "translations" : {
    "ENGLISH" : [ "fly" ],
    "FRENCH" : [ "avion)", "voler(oiseau" ]
  },
  "regular" : false,
  "views" : 13350
}, {
  "spanish" : "explicar",
  "translations" : {
    "ENGLISH" : [ "explain" ],
    "FRENCH" : [ "expliquer" ]
  },
  "regular" : false,
  "views" : 13347
}, {
  "spanish" : "amar",
  "translations" : {
    "ENGLISH" : [ "love" ],
    "FRENCH" : [ "aimer(amour)" ]
  },
  "regular" : true,
  "views" : 3340
}, {
  "spanish" : "crecer",
  "translations" : {
    "ENGLISH" : [ "grow" ],
    "FRENCH" : [ "croître" ]
  },
  "regular" : false,
  "views" : 3325
}, {
  "spanish" : "entrar",
  "translations" : {
    "ENGLISH" : [ "go in" ],
    "FRENCH" : [ "entrer" ]
  },
  "regular" : true,
  "views" : 3317
}, {
  "spanish" : "destruir",
  "translations" : {
    "ENGLISH" : [ "wreck", "destroy", "demolish" ],
    "FRENCH" : [ "détruire" ]
  },
  "regular" : false,
  "views" : 3299
}, {
  "spanish" : "llorar",
  "translations" : {
    "ENGLISH" : [ "cry" ],
    "FRENCH" : [ "pleurer" ]
  },
  "regular" : true,
  "views" : 3294
}, {
  "spanish" : "navegar",
  "translations" : {
    "ENGLISH" : [ "surf (the internet)", "sail" ],
    "FRENCH" : [ "naviguer" ]
  },
  "regular" : false,
  "views" : 3277
}, {
  "spanish" : "probar",
  "translations" : {
    "ENGLISH" : [ "try on", "taste", "try" ],
    "FRENCH" : [ "tester" ]
  },
  "regular" : false,
  "views" : 3269
}, {
  "spanish" : "valer",
  "translations" : {
    "ENGLISH" : [ "be worth" ],
    "FRENCH" : [ "valoir" ]
  },
  "regular" : false,
  "views" : 3257
}, {
  "spanish" : "apagar",
  "translations" : {
    "ENGLISH" : [ "extinguish", "putout" ],
    "FRENCH" : [ "éteindre" ]
  },
  "regular" : false,
  "views" : 13253
}, {
  "spanish" : "obtener",
  "translations" : {
    "ENGLISH" : [ "obtain" ],
    "FRENCH" : [ "obtenir" ]
  },
  "regular" : false,
  "views" : 3241
}, {
  "spanish" : "manejar",
  "translations" : {
    "ENGLISH" : [ "use", "handle", "manage" ],
    "FRENCH" : [ "manoeuvrer" ]
  },
  "regular" : true,
  "views" : 3241
}, {
  "spanish" : "cortar",
  "translations" : {
    "ENGLISH" : [ "cut" ],
    "FRENCH" : [ "couper" ]
  },
  "regular" : true,
  "views" : 13226
}, {
  "spanish" : "halagar",
  "translations" : {
    "ENGLISH" : [ "flatter" ],
    "FRENCH" : [ "complimenter" ]
  },
  "regular" : false,
  "views" : 3217
}, {
  "spanish" : "gozar",
  "translations" : {
    "ENGLISH" : [ "enjoy" ],
    "FRENCH" : [ "apprécier" ]
  },
  "regular" : false,
  "views" : 3177
}, {
  "spanish" : "incluir",
  "translations" : {
    "ENGLISH" : [ "include" ],
    "FRENCH" : [ "inclure" ]
  },
  "regular" : false,
  "views" : 3172
}, {
  "spanish" : "hartar",
  "translations" : {
    "ENGLISH" : [ "gorge", "to get bored of" ],
    "FRENCH" : [ "assouvir" ]
  },
  "regular" : false,
  "views" : 3165
}, {
  "spanish" : "sufrir",
  "translations" : {
    "ENGLISH" : [ "suffer" ],
    "FRENCH" : [ "souffrir" ]
  },
  "regular" : true,
  "views" : 3159
}, {
  "spanish" : "aburrir",
  "translations" : {
    "ENGLISH" : [ "bore" ],
    "FRENCH" : [ "ennuyer" ]
  },
  "regular" : true,
  "views" : 3132
}, {
  "spanish" : "habilitar",
  "translations" : {
    "ENGLISH" : [ "fit out", "authorise" ],
    "FRENCH" : [ "habiliter" ]
  },
  "regular" : true,
  "views" : 3120
}, {
  "spanish" : "prestar",
  "translations" : {
    "ENGLISH" : [ "lend" ],
    "FRENCH" : [ "prêter" ]
  },
  "regular" : true,
  "views" : 13108
}, {
  "spanish" : "corregir",
  "translations" : {
    "ENGLISH" : [ "correct" ],
    "FRENCH" : [ "corriger" ]
  },
  "regular" : false,
  "views" : 3100
}, {
  "spanish" : "regresar",
  "translations" : {
    "ENGLISH" : [ "come back", "return" ],
    "FRENCH" : [ "retourner" ]
  },
  "regular" : true,
  "views" : 3095
}, {
  "spanish" : "echar",
  "translations" : {
    "ENGLISH" : [ "throw" ],
    "FRENCH" : [ "jetter" ]
  },
  "regular" : true,
  "views" : 3085
}, {
  "spanish" : "habitar",
  "translations" : {
    "ENGLISH" : [ "inhabit" ],
    "FRENCH" : [ "habiter" ]
  },
  "regular" : true,
  "views" : 3072
}, {
  "spanish" : "cruzar",
  "translations" : {
    "ENGLISH" : [ "cross" ],
    "FRENCH" : [ "croiser" ]
  },
  "regular" : false,
  "views" : 3071
}, {
  "spanish" : "secar",
  "translations" : {
    "ENGLISH" : [ "dry" ],
    "FRENCH" : [ "sécher" ]
  },
  "regular" : false,
  "views" : 3069
}, {
  "spanish" : "producir",
  "translations" : {
    "ENGLISH" : [ "produce" ],
    "FRENCH" : [ "produire" ]
  },
  "regular" : false,
  "views" : 3062
}, {
  "spanish" : "barrer",
  "translations" : {
    "ENGLISH" : [ "sweep" ],
    "FRENCH" : [ "balayer" ]
  },
  "regular" : true,
  "views" : 3057
}, {
  "spanish" : "defender",
  "translations" : {
    "ENGLISH" : [ "defend", "stand up for" ],
    "FRENCH" : [ "défendre" ]
  },
  "regular" : false,
  "views" : 3051
}, {
  "spanish" : "cenar",
  "translations" : {
    "ENGLISH" : [ "have an evening meal" ],
    "FRENCH" : [ "souper", "dîner" ]
  },
  "regular" : true,
  "views" : 3042
}, {
  "spanish" : "entregar",
  "translations" : {
    "ENGLISH" : [ "hand over" ],
    "FRENCH" : [ "livrer" ]
  },
  "regular" : false,
  "views" : 3040
}, {
  "spanish" : "acabar",
  "translations" : {
    "ENGLISH" : [ "finish" ],
    "FRENCH" : [ "terminer", "finir" ]
  },
  "regular" : true,
  "views" : 3039
}, {
  "spanish" : "empacar",
  "translations" : {
    "ENGLISH" : [ "bale", "pack" ],
    "FRENCH" : [ "emballer" ]
  },
  "regular" : false,
  "views" : 3031
}, {
  "spanish" : "hacendar",
  "translations" : {
    "ENGLISH" : [ "grant lands" ],
    "FRENCH" : [ "Grant land????" ]
  },
  "regular" : true,
  "views" : 3029
}, {
  "spanish" : "sumegir",
  "translations" : {
    "ENGLISH" : [ "submerge", "immerse", "dip" ],
    "FRENCH" : [ "submerger" ]
  },
  "regular" : false,
  "views" : 3027
}, {
  "spanish" : "organizar",
  "translations" : {
    "ENGLISH" : [ "organize" ],
    "FRENCH" : [ "organiser" ]
  },
  "regular" : false,
  "views" : 13020
}, {
  "spanish" : "hostigar",
  "translations" : {
    "ENGLISH" : [ "bother", "pester" ],
    "FRENCH" : [ "harceler" ]
  },
  "regular" : false,
  "views" : 3017
}, {
  "spanish" : "hinchar",
  "translations" : {
    "ENGLISH" : [ "puff up" ],
    "FRENCH" : [ "gonflrt" ]
  },
  "regular" : true,
  "views" : 3005
}, {
  "spanish" : "hospedar",
  "translations" : {
    "ENGLISH" : [ "accomodate" ],
    "FRENCH" : [ "accomoder" ]
  },
  "regular" : true,
  "views" : 2991
}, {
  "spanish" : "bajar",
  "translations" : {
    "ENGLISH" : [ "go down", "get off (bus)" ],
    "FRENCH" : [ "descendre" ]
  },
  "regular" : true,
  "views" : 2981
}, {
  "spanish" : "soler",
  "translations" : {
    "ENGLISH" : [ "usually do something" ],
    "FRENCH" : [ "?????" ]
  },
  "regular" : true,
  "views" : 2978
}, {
  "spanish" : "calentar",
  "translations" : {
    "ENGLISH" : [ "warm up", "heat up" ],
    "FRENCH" : [ "réchauffer" ]
  },
  "regular" : false,
  "views" : 2948
}, {
  "spanish" : "peinar",
  "translations" : {
    "ENGLISH" : [ "comb" ],
    "FRENCH" : [ "peigner" ]
  },
  "regular" : true,
  "views" : 12946
}, {
  "spanish" : "colgar",
  "translations" : {
    "ENGLISH" : [ "hang" ],
    "FRENCH" : [ "pendre" ]
  },
  "regular" : false,
  "views" : 2941
}, {
  "spanish" : "patinar",
  "translations" : {
    "ENGLISH" : [ "skate" ],
    "FRENCH" : [ "patiner" ]
  },
  "regular" : true,
  "views" : 2929
}, {
  "spanish" : "tender",
  "translations" : {
    "ENGLISH" : [ "tend", "hang out (washing)" ],
    "FRENCH" : [ "tendre" ]
  },
  "regular" : false,
  "views" : 2893
}, {
  "spanish" : "hipnotizar",
  "translations" : {
    "ENGLISH" : [ "hipnotize" ],
    "FRENCH" : [ "hypnotiser" ]
  },
  "regular" : false,
  "views" : 2889
}, {
  "spanish" : "encender",
  "translations" : {
    "ENGLISH" : [ "light", "inflame" ],
    "FRENCH" : [ "incendier" ]
  },
  "regular" : false,
  "views" : 12887
}, {
  "spanish" : "participar",
  "translations" : {
    "ENGLISH" : [ "participate" ],
    "FRENCH" : [ "participer" ]
  },
  "regular" : true,
  "views" : 2884
}, {
  "spanish" : "discutir",
  "translations" : {
    "ENGLISH" : [ "argue", "discuss" ],
    "FRENCH" : [ "discuter" ]
  },
  "regular" : true,
  "views" : 2871
}, {
  "spanish" : "quitar",
  "translations" : {
    "ENGLISH" : [ "take sth off", "remove" ],
    "FRENCH" : [ "enlever" ]
  },
  "regular" : true,
  "views" : 2868
}, {
  "spanish" : "contestar",
  "translations" : {
    "ENGLISH" : [ "answer" ],
    "FRENCH" : [ "répondre" ]
  },
  "regular" : true,
  "views" : 12862
}, {
  "spanish" : "crear",
  "translations" : {
    "ENGLISH" : [ "create" ],
    "FRENCH" : [ "créer" ]
  },
  "regular" : true,
  "views" : 2836
}, {
  "spanish" : "aceptar",
  "translations" : {
    "ENGLISH" : [ "accept" ],
    "FRENCH" : [ "accepter" ]
  },
  "regular" : true,
  "views" : 12829
}, {
  "spanish" : "hacinar",
  "translations" : {
    "ENGLISH" : [ "stack", "pile up" ]
  },
  "regular" : true,
  "views" : 2822
}, {
  "spanish" : "heredar",
  "translations" : {
    "ENGLISH" : [ "inherit" ],
    "FRENCH" : [ "hériter" ]
  },
  "regular" : true,
  "views" : 2814
}, {
  "spanish" : "dirigir",
  "translations" : {
    "ENGLISH" : [ "manage" ],
    "FRENCH" : [ "diriger" ]
  },
  "regular" : false,
  "views" : 2807
}, {
  "spanish" : "descubrir",
  "translations" : {
    "ENGLISH" : [ "discover" ],
    "FRENCH" : [ "découvrir" ]
  },
  "regular" : false,
  "views" : 2802
}, {
  "spanish" : "gastar",
  "translations" : {
    "ENGLISH" : [ "spend" ],
    "FRENCH" : [ "dépenser" ]
  },
  "regular" : true,
  "views" : 12793
}, {
  "spanish" : "abrazar",
  "translations" : {
    "ENGLISH" : [ "embrace" ],
    "FRENCH" : [ "embrasser" ]
  },
  "regular" : false,
  "views" : 2782
}, {
  "spanish" : "parar",
  "translations" : {
    "ENGLISH" : [ "stop" ],
    "FRENCH" : [ "arrêter" ]
  },
  "regular" : true,
  "views" : 12751
}, {
  "spanish" : "yacer",
  "translations" : {
    "ENGLISH" : [ "lie" ],
    "FRENCH" : [ "mentir" ]
  },
  "regular" : false,
  "views" : 2749
}, {
  "spanish" : "disfrutar",
  "translations" : {
    "ENGLISH" : [ "enjoy" ]
  },
  "regular" : true,
  "views" : 2748
}, {
  "spanish" : "negar",
  "translations" : {
    "ENGLISH" : [ "deny" ]
  },
  "regular" : false,
  "views" : 2740
}, {
  "spanish" : "obedecer",
  "translations" : {
    "ENGLISH" : [ "obey" ]
  },
  "regular" : false,
  "views" : 2733
}, {
  "spanish" : "proteger",
  "translations" : {
    "ENGLISH" : [ "protect" ]
  },
  "regular" : false,
  "views" : 2730
}, {
  "spanish" : "tratar",
  "translations" : {
    "ENGLISH" : [ "treat", "try" ]
  },
  "regular" : true,
  "views" : 2725
}, {
  "spanish" : "cumplir",
  "translations" : {
    "ENGLISH" : [ "carry out", "fulfil" ]
  },
  "regular" : true,
  "views" : 2722
}, {
  "spanish" : "interesar",
  "translations" : {
    "ENGLISH" : [ "be interested in" ]
  },
  "regular" : true,
  "views" : 2720
}, {
  "spanish" : "nevar",
  "translations" : {
    "ENGLISH" : [ "snow" ]
  },
  "regular" : true,
  "views" : 2704
}, {
  "spanish" : "medir",
  "translations" : {
    "ENGLISH" : [ "measure" ]
  },
  "regular" : false,
  "views" : 2695
}, {
  "spanish" : "abandonar",
  "translations" : {
    "ENGLISH" : [ "abandon", "leave" ]
  },
  "regular" : true,
  "views" : 2695
}, {
  "spanish" : "invertir",
  "translations" : {
    "ENGLISH" : [ "invert", "reverse" ]
  },
  "regular" : false,
  "views" : 2692
}, {
  "spanish" : "sofreir",
  "translations" : {
    "ENGLISH" : [ "saute", "fry lightly" ]
  },
  "regular" : false,
  "views" : 2690
}, {
  "spanish" : "arrepentir",
  "translations" : {
    "ENGLISH" : [ "regret" ]
  },
  "regular" : false,
  "views" : 2688
}, {
  "spanish" : "cansar",
  "translations" : {
    "ENGLISH" : [ "tire" ]
  },
  "regular" : true,
  "views" : 2685
}, {
  "spanish" : "encantar",
  "translations" : {
    "ENGLISH" : [ "love something" ]
  },
  "regular" : true,
  "views" : 2675
}, {
  "spanish" : "duchar",
  "translations" : {
    "ENGLISH" : [ "have a shower" ]
  },
  "regular" : true,
  "views" : 2665
}, {
  "spanish" : "fijar",
  "translations" : {
    "ENGLISH" : [ "fix" ]
  },
  "regular" : false,
  "views" : 2661
}, {
  "spanish" : "sacudir",
  "translations" : {
    "ENGLISH" : [ "shake" ]
  },
  "regular" : true,
  "views" : 2658
}, {
  "spanish" : "contribuir",
  "translations" : {
    "ENGLISH" : [ "contribute" ]
  },
  "regular" : false,
  "views" : 2651
}, {
  "spanish" : "acordar",
  "translations" : {
    "ENGLISH" : [ "haceralgo" ]
  },
  "regular" : false,
  "views" : 2630
}, {
  "spanish" : "regar",
  "translations" : {
    "ENGLISH" : [ "irrigate", "water" ]
  },
  "regular" : false,
  "views" : 2629
}, {
  "spanish" : "besar",
  "translations" : {
    "ENGLISH" : [ "kiss" ]
  },
  "regular" : true,
  "views" : 2625
}, {
  "spanish" : "llenar",
  "translations" : {
    "ENGLISH" : [ "fill" ]
  },
  "regular" : true,
  "views" : 12624
}, {
  "spanish" : "firmar",
  "translations" : {
    "ENGLISH" : [ "sign" ]
  },
  "regular" : true,
  "views" : 12622
}, {
  "spanish" : "utilizar",
  "translations" : {
    "ENGLISH" : [ "use" ]
  },
  "regular" : false,
  "views" : 2614
}, {
  "spanish" : "bendecir",
  "translations" : {
    "ENGLISH" : [ "bless" ]
  },
  "regular" : false,
  "views" : 2602
}, {
  "spanish" : "responder",
  "translations" : {
    "ENGLISH" : [ "answer" ]
  },
  "regular" : true,
  "views" : 12588
}, {
  "spanish" : "ahorrar",
  "translations" : {
    "ENGLISH" : [ "save" ]
  },
  "regular" : true,
  "views" : 2584
}, {
  "spanish" : "reciclar",
  "translations" : {
    "ENGLISH" : [ "reuse", "recycle" ]
  },
  "regular" : true,
  "views" : 2578
}, {
  "spanish" : "mezclar",
  "translations" : {
    "ENGLISH" : [ "mix" ]
  },
  "regular" : true,
  "views" : 2570
}, {
  "spanish" : "influir",
  "translations" : {
    "ENGLISH" : [ "influence" ]
  },
  "regular" : false,
  "views" : 2567
}, {
  "spanish" : "poseer",
  "translations" : {
    "ENGLISH" : [ "own", "possess", "hold" ]
  },
  "regular" : false,
  "views" : 2565
}, {
  "spanish" : "pasear",
  "translations" : {
    "ENGLISH" : [ "walk" ]
  },
  "regular" : true,
  "views" : 2558
}, {
  "spanish" : "juntar",
  "translations" : {
    "ENGLISH" : [ "join" ]
  },
  "regular" : false,
  "views" : 2537
}, {
  "spanish" : "malentender",
  "translations" : {
    "ENGLISH" : [ "misunderstand" ]
  },
  "regular" : false,
  "views" : 2535
}, {
  "spanish" : "continuar",
  "translations" : {
    "ENGLISH" : [ "continue" ]
  },
  "regular" : false,
  "views" : 2533
}, {
  "spanish" : "torcer",
  "translations" : {
    "ENGLISH" : [ "twist" ]
  },
  "regular" : false,
  "views" : 2502
}, {
  "spanish" : "suponer",
  "translations" : {
    "ENGLISH" : [ "suppose" ]
  },
  "regular" : false,
  "views" : 2499
}, {
  "spanish" : "intentar",
  "translations" : {
    "ENGLISH" : [ "try" ]
  },
  "regular" : true,
  "views" : 12491
}, {
  "spanish" : "vacar",
  "translations" : {
    "ENGLISH" : [ "become vacant" ]
  },
  "regular" : false,
  "views" : 2490
}, {
  "spanish" : "merendar",
  "translations" : {
    "ENGLISH" : [ "have a snack in the afternoon" ]
  },
  "regular" : false,
  "views" : 2486
}, {
  "spanish" : "montar",
  "translations" : {
    "ENGLISH" : [ "set up", "ride" ]
  },
  "regular" : true,
  "views" : 2485
}, {
  "spanish" : "graduar",
  "translations" : {
    "ENGLISH" : [ "adjust", "graduate" ]
  },
  "regular" : false,
  "views" : 2485
}, {
  "spanish" : "vencer",
  "translations" : {
    "ENGLISH" : [ "conquer", "overcome" ]
  },
  "regular" : false,
  "views" : 2474
}, {
  "spanish" : "partir",
  "translations" : {
    "ENGLISH" : [ "leave", "cut up", "depart" ]
  },
  "regular" : true,
  "views" : 2469
}, {
  "spanish" : "atraer",
  "translations" : {
    "ENGLISH" : [ "attract" ]
  },
  "regular" : false,
  "views" : 2465
}, {
  "spanish" : "descansar",
  "translations" : {
    "ENGLISH" : [ "rest" ]
  },
  "regular" : true,
  "views" : 2464
}, {
  "spanish" : "establecer",
  "translations" : {
    "ENGLISH" : [ "establish" ]
  },
  "regular" : false,
  "views" : 2440
}, {
  "spanish" : "verter",
  "translations" : {
    "ENGLISH" : [ "spill" ]
  },
  "regular" : false,
  "views" : 2427
}, {
  "spanish" : "invitar",
  "translations" : {
    "ENGLISH" : [ "invite" ]
  },
  "regular" : true,
  "views" : 2426
}, {
  "spanish" : "imprimir",
  "translations" : {
    "ENGLISH" : [ "print" ]
  },
  "regular" : true,
  "views" : 2426
}, {
  "spanish" : "bucear",
  "translations" : {
    "ENGLISH" : [ "dive" ]
  },
  "regular" : true,
  "views" : 2423
}, {
  "spanish" : "reconocer",
  "translations" : {
    "ENGLISH" : [ "recognise" ]
  },
  "regular" : false,
  "views" : 2421
}, {
  "spanish" : "temer",
  "translations" : {
    "ENGLISH" : [ "be afraid of", "fear" ]
  },
  "regular" : true,
  "views" : 2417
}, {
  "spanish" : "abanicar",
  "translations" : {
    "ENGLISH" : [ "fan (oneself)" ]
  },
  "regular" : false,
  "views" : 2410
}, {
  "spanish" : "insistir",
  "translations" : {
    "ENGLISH" : [ "insist" ]
  },
  "regular" : true,
  "views" : 2407
}, {
  "spanish" : "convencer",
  "translations" : {
    "ENGLISH" : [ "convince" ]
  },
  "regular" : false,
  "views" : 2404
}, {
  "spanish" : "zafar",
  "translations" : {
    "ENGLISH" : [ "escape" ]
  },
  "regular" : false,
  "views" : 2402
}, {
  "spanish" : "advertir",
  "translations" : {
    "ENGLISH" : [ "warn" ]
  },
  "regular" : false,
  "views" : 2398
}, {
  "spanish" : "ocurrir",
  "translations" : {
    "ENGLISH" : [ "happen" ]
  },
  "regular" : true,
  "views" : 2398
}, {
  "spanish" : "satisfacer",
  "translations" : {
    "ENGLISH" : [ "quench", "satisfy" ]
  },
  "regular" : false,
  "views" : 2396
}, {
  "spanish" : "gritar",
  "translations" : {
    "ENGLISH" : [ "shout" ]
  },
  "regular" : true,
  "views" : 2387
}, {
  "spanish" : "matar",
  "translations" : {
    "ENGLISH" : [ "kill" ]
  },
  "regular" : true,
  "views" : 2385
}, {
  "spanish" : "preocupar",
  "translations" : {
    "ENGLISH" : [ "worry" ]
  },
  "regular" : true,
  "views" : 12373
}, {
  "spanish" : "faltar",
  "translations" : {
    "ENGLISH" : [ "need" ]
  },
  "regular" : true,
  "views" : 2371
}, {
  "spanish" : "adelgazar",
  "translations" : {
    "ENGLISH" : [ "lose weight" ]
  },
  "regular" : false,
  "views" : 2365
}, {
  "spanish" : "girar",
  "translations" : {
    "ENGLISH" : [ "turn" ]
  },
  "regular" : true,
  "views" : 2363
}, {
  "spanish" : "abogar",
  "translations" : {
    "ENGLISH" : [ "defend someone" ]
  },
  "regular" : false,
  "views" : 2363
}, {
  "spanish" : "meter",
  "translations" : {
    "ENGLISH" : [ "put in" ]
  },
  "regular" : true,
  "views" : 2362
}, {
  "spanish" : "apetecer",
  "translations" : {
    "ENGLISH" : [ "fancy doing" ]
  },
  "regular" : false,
  "views" : 2360
}, {
  "spanish" : "fingir",
  "translations" : {
    "ENGLISH" : [ "pretend" ]
  },
  "regular" : false,
  "views" : 2351
}, {
  "spanish" : "saludar",
  "translations" : {
    "ENGLISH" : [ "greet", "say hello" ]
  },
  "regular" : true,
  "views" : 2337
}, {
  "spanish" : "tirar",
  "translations" : {
    "ENGLISH" : [ "waste", "throw" ]
  },
  "regular" : true,
  "views" : 2337
}, {
  "spanish" : "arreglar",
  "translations" : {
    "ENGLISH" : [ "repair", "fix" ]
  },
  "regular" : true,
  "views" : 2336
}, {
  "spanish" : "pelear",
  "translations" : {
    "ENGLISH" : [ "quarrel", "argue", "fight" ]
  },
  "regular" : true,
  "views" : 2328
}, {
  "spanish" : "picar",
  "translations" : {
    "ENGLISH" : [ "bite" ]
  },
  "regular" : false,
  "views" : 2320
}, {
  "spanish" : "sorprender",
  "translations" : {
    "ENGLISH" : [ "surprise" ]
  },
  "regular" : true,
  "views" : 2318
}, {
  "spanish" : "unir",
  "translations" : {
    "ENGLISH" : [ "unit", "join" ]
  },
  "regular" : true,
  "views" : 2316
}, {
  "spanish" : "cagar",
  "translations" : {
    "ENGLISH" : [ "shit" ]
  },
  "regular" : false,
  "views" : 2308
}, {
  "spanish" : "saltar",
  "translations" : {
    "ENGLISH" : [ "jump" ]
  },
  "regular" : true,
  "views" : 2303
}, {
  "spanish" : "quemar",
  "translations" : {
    "ENGLISH" : [ "burn" ]
  },
  "regular" : true,
  "views" : 2302
}, {
  "spanish" : "vaciar",
  "translations" : {
    "ENGLISH" : [ "clear", "empty" ]
  },
  "regular" : false,
  "views" : 2298
}, {
  "spanish" : "abatir",
  "translations" : {
    "ENGLISH" : [ "knock down", "overthrow" ]
  },
  "regular" : true,
  "views" : 2297
}, {
  "spanish" : "mudar",
  "translations" : {
    "ENGLISH" : [ "move house" ]
  },
  "regular" : true,
  "views" : 2294
}, {
  "spanish" : "actuar",
  "translations" : {
    "ENGLISH" : [ "act" ]
  },
  "regular" : false,
  "views" : 2293
}, {
  "spanish" : "dibujar",
  "translations" : {
    "ENGLISH" : [ "draw" ]
  },
  "regular" : true,
  "views" : 12293
}, {
  "spanish" : "dedicar",
  "translations" : {
    "ENGLISH" : [ "dedicate" ]
  },
  "regular" : false,
  "views" : 2289
}, {
  "spanish" : "agradecer",
  "translations" : {
    "ENGLISH" : [ "thank" ]
  },
  "regular" : false,
  "views" : 2280
}, {
  "spanish" : "aparecer",
  "translations" : {
    "ENGLISH" : [ "appear", "showup" ]
  },
  "regular" : false,
  "views" : 2279
}, {
  "spanish" : "guardar",
  "translations" : {
    "ENGLISH" : [ "keep" ]
  },
  "regular" : true,
  "views" : 2278
}, {
  "spanish" : "recomenzar",
  "translations" : {
    "ENGLISH" : [ "begin again" ]
  },
  "regular" : true,
  "views" : 2278
}, {
  "spanish" : "celebrar",
  "translations" : {
    "ENGLISH" : [ "celebrate" ]
  },
  "regular" : true,
  "views" : 2273
}, {
  "spanish" : "aprobar",
  "translations" : {
    "ENGLISH" : [ "approve" ]
  },
  "regular" : false,
  "views" : 2271
}, {
  "spanish" : "salvar",
  "translations" : {
    "ENGLISH" : [ "save" ]
  },
  "regular" : true,
  "views" : 2271
}, {
  "spanish" : "odiar",
  "translations" : {
    "ENGLISH" : [ "hate" ]
  },
  "regular" : true,
  "views" : 2268
}, {
  "spanish" : "fumar",
  "translations" : {
    "ENGLISH" : [ "smoke" ]
  },
  "regular" : true,
  "views" : 12265
}, {
  "spanish" : "reducir",
  "translations" : {
    "ENGLISH" : [ "reduce" ]
  },
  "regular" : false,
  "views" : 2263
}, {
  "spanish" : "reunir",
  "translations" : {
    "ENGLISH" : [ "reunite" ]
  },
  "regular" : false,
  "views" : 2261
}, {
  "spanish" : "impedir",
  "translations" : {
    "ENGLISH" : [ "prevent" ]
  },
  "regular" : false,
  "views" : 2259
}, {
  "spanish" : "recorrer",
  "translations" : {
    "ENGLISH" : [ "travel" ]
  },
  "regular" : true,
  "views" : 2258
}, {
  "spanish" : "quebrar",
  "translations" : {
    "ENGLISH" : [ "break" ]
  },
  "regular" : false,
  "views" : 2245
}, {
  "spanish" : "merecer",
  "translations" : {
    "ENGLISH" : [ "deserve" ]
  },
  "regular" : false,
  "views" : 2243
}, {
  "spanish" : "competir",
  "translations" : {
    "ENGLISH" : [ "compete" ]
  },
  "regular" : false,
  "views" : 2237
}, {
  "spanish" : "tardar",
  "translations" : {
    "ENGLISH" : [ "take time" ]
  },
  "regular" : true,
  "views" : 2236
}, {
  "spanish" : "marchar",
  "translations" : {
    "ENGLISH" : [ "leave", "go" ]
  },
  "regular" : true,
  "views" : 2232
}, {
  "spanish" : "chocar",
  "translations" : {
    "ENGLISH" : [ "crash" ]
  },
  "regular" : false,
  "views" : 2228
}, {
  "spanish" : "realizar",
  "translations" : {
    "ENGLISH" : [ "carry out" ]
  },
  "regular" : false,
  "views" : 2225
}, {
  "spanish" : "marcar",
  "translations" : {
    "ENGLISH" : [ "mark" ]
  },
  "regular" : false,
  "views" : 2220
}, {
  "spanish" : "averiguar",
  "translations" : {
    "ENGLISH" : [ "find out" ]
  },
  "regular" : false,
  "views" : 2218
}, {
  "spanish" : "ordenar",
  "translations" : {
    "ENGLISH" : [ "tidy up", "order" ]
  },
  "regular" : true,
  "views" : 2211
}, {
  "spanish" : "afeitar",
  "translations" : {
    "ENGLISH" : [ "shave" ]
  },
  "regular" : true,
  "views" : 2209
}, {
  "spanish" : "cancelar",
  "translations" : {
    "ENGLISH" : [ "cancel" ]
  },
  "regular" : true,
  "views" : 12209
}, {
  "spanish" : "ejercer",
  "translations" : {
    "ENGLISH" : [ "practise", "exert", "exercise" ]
  },
  "regular" : false,
  "views" : 2207
}, {
  "spanish" : "acercar",
  "translations" : {
    "ENGLISH" : [ "get near", "approach" ]
  },
  "regular" : false,
  "views" : 2205
}, {
  "spanish" : "pintar",
  "translations" : {
    "ENGLISH" : [ "paint" ]
  },
  "regular" : true,
  "views" : 2196
}, {
  "spanish" : "esconder",
  "translations" : {
    "ENGLISH" : [ "hide" ]
  },
  "regular" : true,
  "views" : 2193
}, {
  "spanish" : "convertir",
  "translations" : {
    "ENGLISH" : [ "convert" ]
  },
  "regular" : false,
  "views" : 2192
}, {
  "spanish" : "demostrar",
  "translations" : {
    "ENGLISH" : [ "demonstrate" ]
  },
  "regular" : false,
  "views" : 2186
}, {
  "spanish" : "lanzar",
  "translations" : {
    "ENGLISH" : [ "throw", "fling" ]
  },
  "regular" : false,
  "views" : 2182
}, {
  "spanish" : "abstener",
  "translations" : {
    "ENGLISH" : [ "abstain" ]
  },
  "regular" : false,
  "views" : 2179
}, {
  "spanish" : "pegar",
  "translations" : {
    "ENGLISH" : [ "hit", "stick", "paste" ]
  },
  "regular" : false,
  "views" : 2177
}, {
  "spanish" : "alquilar",
  "translations" : {
    "ENGLISH" : [ "rent" ]
  },
  "regular" : true,
  "views" : 2174
}, {
  "spanish" : "esquiar",
  "translations" : {
    "ENGLISH" : [ "ski" ]
  },
  "regular" : false,
  "views" : 2171
}, {
  "spanish" : "avisar",
  "translations" : {
    "ENGLISH" : [ "let somebody know" ]
  },
  "regular" : true,
  "views" : 2168
}, {
  "spanish" : "abolir",
  "translations" : {
    "ENGLISH" : [ "abolish" ]
  },
  "regular" : true,
  "views" : 2166
}, {
  "spanish" : "apoyar",
  "translations" : {
    "ENGLISH" : [ "lean" ]
  },
  "regular" : true,
  "views" : 2164
}, {
  "spanish" : "colegir",
  "translations" : {
    "ENGLISH" : [ "deduce" ]
  },
  "regular" : true,
  "views" : 2162
}, {
  "spanish" : "atender",
  "translations" : {
    "ENGLISH" : [ "attend to" ]
  },
  "regular" : false,
  "views" : 2154
}, {
  "spanish" : "gemir",
  "translations" : {
    "ENGLISH" : [ "groan", "moan" ]
  },
  "regular" : false,
  "views" : 2151
}, {
  "spanish" : "amenazar",
  "translations" : {
    "ENGLISH" : [ "threaten" ]
  },
  "regular" : false,
  "views" : 2149
}, {
  "spanish" : "enfocar",
  "translations" : {
    "ENGLISH" : [ "focus" ]
  },
  "regular" : false,
  "views" : 2149
}, {
  "spanish" : "kilometrar",
  "translations" : {
    "ENGLISH" : [ "measure in kilometres" ]
  },
  "regular" : true,
  "views" : 2149
}, {
  "spanish" : "desaparecer",
  "translations" : {
    "ENGLISH" : [ "disappear" ]
  },
  "regular" : false,
  "views" : 2148
}, {
  "spanish" : "planear",
  "translations" : {
    "ENGLISH" : [ "plan" ]
  },
  "regular" : true,
  "views" : 2139
}, {
  "spanish" : "cuestionar",
  "translations" : {
    "ENGLISH" : [ "question" ]
  },
  "regular" : true,
  "views" : 2137
}, {
  "spanish" : "abofetar",
  "translations" : {
    "ENGLISH" : [ "slap" ]
  },
  "regular" : true,
  "views" : 2135
}, {
  "spanish" : "borrar",
  "translations" : {
    "ENGLISH" : [ "delete", "rub" ]
  },
  "regular" : true,
  "views" : 2133
}, {
  "spanish" : "guiar",
  "translations" : {
    "ENGLISH" : [ "guide" ]
  },
  "regular" : false,
  "views" : 2132
}, {
  "spanish" : "cargar",
  "translations" : {
    "ENGLISH" : [ "load" ]
  },
  "regular" : false,
  "views" : 2130
}, {
  "spanish" : "abarcar",
  "translations" : {
    "ENGLISH" : [ "cover", "cope with" ]
  },
  "regular" : false,
  "views" : 2124
}, {
  "spanish" : "abocar",
  "translations" : {
    "ENGLISH" : [ "head for something" ]
  },
  "regular" : false,
  "views" : 2122
}, {
  "spanish" : "toser",
  "translations" : {
    "ENGLISH" : [ "cough" ]
  },
  "regular" : true,
  "views" : 12115
}, {
  "spanish" : "abdicar",
  "translations" : {
    "ENGLISH" : [ "abdicate" ]
  },
  "regular" : false,
  "views" : 2114
}, {
  "spanish" : "aterrizar",
  "translations" : {
    "ENGLISH" : [ "land" ]
  },
  "regular" : false,
  "views" : 2111
}, {
  "spanish" : "proponer",
  "translations" : {
    "ENGLISH" : [ "propose" ]
  },
  "regular" : false,
  "views" : 2109
}, {
  "spanish" : "ocupar",
  "translations" : {
    "ENGLISH" : [ "occupy", "take up" ]
  },
  "regular" : true,
  "views" : 2102
}, {
  "spanish" : "alcanzar",
  "translations" : {
    "ENGLISH" : [ "reach" ]
  },
  "regular" : false,
  "views" : 2101
}, {
  "spanish" : "describir",
  "translations" : {
    "ENGLISH" : [ "describe" ]
  },
  "regular" : false,
  "views" : 12097
}, {
  "spanish" : "aprovechar",
  "translations" : {
    "ENGLISH" : [ "take advantage of", "use" ]
  },
  "regular" : true,
  "views" : 2096
}, {
  "spanish" : "deletrear",
  "translations" : {
    "ENGLISH" : [ "spell" ]
  },
  "regular" : true,
  "views" : 12096
}, {
  "spanish" : "abastecer",
  "translations" : {
    "ENGLISH" : [ "supply" ]
  },
  "regular" : false,
  "views" : 2095
}, {
  "spanish" : "definir",
  "translations" : {
    "ENGLISH" : [ "define" ]
  },
  "regular" : false,
  "views" : 2094
}, {
  "spanish" : "calificar",
  "translations" : {
    "ENGLISH" : [ "grade", "mark" ]
  },
  "regular" : false,
  "views" : 2086
}, {
  "spanish" : "oponer",
  "translations" : {
    "ENGLISH" : [ "oppose" ]
  },
  "regular" : false,
  "views" : 2085
}, {
  "spanish" : "cepillar",
  "translations" : {
    "ENGLISH" : [ "brush" ]
  },
  "regular" : true,
  "views" : 2084
}, {
  "spanish" : "golpear",
  "translations" : {
    "ENGLISH" : [ "bang" ]
  },
  "regular" : true,
  "views" : 2084
}, {
  "spanish" : "cabalgar",
  "translations" : {
    "ENGLISH" : [ "ride" ]
  },
  "regular" : false,
  "views" : 2079
}, {
  "spanish" : "cocer",
  "translations" : {
    "ENGLISH" : [ "boil" ]
  },
  "regular" : false,
  "views" : 2077
}, {
  "spanish" : "morder",
  "translations" : {
    "ENGLISH" : [ "eat into", "bite" ]
  },
  "regular" : false,
  "views" : 2074
}, {
  "spanish" : "prohibir",
  "translations" : {
    "ENGLISH" : [ "forbid" ]
  },
  "regular" : false,
  "views" : 2074
}, {
  "spanish" : "depender",
  "translations" : {
    "ENGLISH" : [ "depend" ]
  },
  "regular" : true,
  "views" : 2073
}, {
  "spanish" : "sembrar",
  "translations" : {
    "ENGLISH" : [ "sow" ]
  },
  "regular" : false,
  "views" : 2072
}, {
  "spanish" : "juzgar",
  "translations" : {
    "ENGLISH" : [ "judge" ]
  },
  "regular" : false,
  "views" : 2068
}, {
  "spanish" : "colocar",
  "translations" : {
    "ENGLISH" : [ "place" ]
  },
  "regular" : false,
  "views" : 2065
}, {
  "spanish" : "publicar",
  "translations" : {
    "ENGLISH" : [ "publish" ]
  },
  "regular" : false,
  "views" : 2062
}, {
  "spanish" : "xerocopiar",
  "translations" : {
    "ENGLISH" : [ "photocopy" ]
  },
  "regular" : true,
  "views" : 2058
}, {
  "spanish" : "gobernar",
  "translations" : {
    "ENGLISH" : [ "govern" ]
  },
  "regular" : false,
  "views" : 2054
}, {
  "spanish" : "adquirir",
  "translations" : {
    "ENGLISH" : [ "aquire" ]
  },
  "regular" : false,
  "views" : 2047
}, {
  "spanish" : "madrugar",
  "translations" : {
    "ENGLISH" : [ "get up early" ]
  },
  "regular" : false,
  "views" : 2043
}, {
  "spanish" : "enamorar",
  "translations" : {
    "ENGLISH" : [ "fall in love" ]
  },
  "regular" : true,
  "views" : 2040
}, {
  "spanish" : "abonar",
  "translations" : {
    "ENGLISH" : [ "pay", "fertilize" ]
  },
  "regular" : true,
  "views" : 2040
}, {
  "spanish" : "imaginar",
  "translations" : {
    "ENGLISH" : [ "imagine" ]
  },
  "regular" : true,
  "views" : 2039
}, {
  "spanish" : "transferir",
  "translations" : {
    "ENGLISH" : [ "transfer" ]
  },
  "regular" : false,
  "views" : 2038
}, {
  "spanish" : "padecer",
  "translations" : {
    "ENGLISH" : [ "suffer" ]
  },
  "regular" : false,
  "views" : 2034
}, {
  "spanish" : "mejorar",
  "translations" : {
    "ENGLISH" : [ "improve" ]
  },
  "regular" : true,
  "views" : 2034
}, {
  "spanish" : "detener",
  "translations" : {
    "ENGLISH" : [ "delay", "arrest", "stop", "hold up" ]
  },
  "regular" : false,
  "views" : 2031
}, {
  "spanish" : "jurar",
  "translations" : {
    "ENGLISH" : [ "swear" ]
  },
  "regular" : true,
  "views" : 2031
}, {
  "spanish" : "perseguir",
  "translations" : {
    "ENGLISH" : [ "pursue" ]
  },
  "regular" : false,
  "views" : 2030
}, {
  "spanish" : "fascinar",
  "translations" : {
    "ENGLISH" : [ "fascinate" ]
  },
  "regular" : true,
  "views" : 2030
}, {
  "spanish" : "beneficiar",
  "translations" : {
    "ENGLISH" : [ "benefit" ]
  },
  "regular" : true,
  "views" : 2026
}, {
  "spanish" : "zampar",
  "translations" : {
    "ENGLISH" : [ "gobble down" ]
  },
  "regular" : false,
  "views" : 2022
}, {
  "spanish" : "dudar",
  "translations" : {
    "ENGLISH" : [ "doubt" ]
  },
  "regular" : true,
  "views" : 2021
}, {
  "spanish" : "malgastar",
  "translations" : {
    "ENGLISH" : [ "waste" ]
  },
  "regular" : true,
  "views" : 2020
}, {
  "spanish" : "regalar",
  "translations" : {
    "ENGLISH" : [ "give a present" ]
  },
  "regular" : true,
  "views" : 2015
}, {
  "spanish" : "pertenecer",
  "translations" : {
    "ENGLISH" : [ "belong" ]
  },
  "regular" : true,
  "views" : 2012
}, {
  "spanish" : "maquillar",
  "translations" : {
    "ENGLISH" : [ "put make up on" ]
  },
  "regular" : true,
  "views" : 2011
}, {
  "spanish" : "jubilar",
  "translations" : {
    "ENGLISH" : [ "retire" ]
  },
  "regular" : true,
  "views" : 2009
}, {
  "spanish" : "rechazar",
  "translations" : {
    "ENGLISH" : [ "reject" ]
  },
  "regular" : false,
  "views" : 2006
}, {
  "spanish" : "imponer",
  "translations" : {
    "ENGLISH" : [ "impose" ]
  },
  "regular" : false,
  "views" : 2003
}, {
  "spanish" : "introducir",
  "translations" : {
    "ENGLISH" : [ "introduce" ]
  },
  "regular" : false,
  "views" : 2000
}, {
  "spanish" : "molestar",
  "translations" : {
    "ENGLISH" : [ "bother" ]
  },
  "regular" : true,
  "views" : 1997
}, {
  "spanish" : "confesar",
  "translations" : {
    "ENGLISH" : [ "confess" ]
  },
  "regular" : false,
  "views" : 1996
}, {
  "spanish" : "obligar",
  "translations" : {
    "ENGLISH" : [ "force" ]
  },
  "regular" : false,
  "views" : 1996
}, {
  "spanish" : "bastar",
  "translations" : {
    "ENGLISH" : [ "be enough" ]
  },
  "regular" : true,
  "views" : 1989
}, {
  "spanish" : "temblar",
  "translations" : {
    "ENGLISH" : [ "tremble" ]
  },
  "regular" : false,
  "views" : 1988
}, {
  "spanish" : "bienquerer",
  "translations" : {
    "ENGLISH" : [ "like", "be fond of" ]
  },
  "regular" : false,
  "views" : 1987
}, {
  "spanish" : "bautizar",
  "translations" : {
    "ENGLISH" : [ "baptise" ]
  },
  "regular" : false,
  "views" : 1984
}, {
  "spanish" : "abominar",
  "translations" : {
    "ENGLISH" : [ "detest", "hate something" ]
  },
  "regular" : true,
  "views" : 1984
}, {
  "spanish" : "confiar",
  "translations" : {
    "ENGLISH" : [ "confide" ]
  },
  "regular" : false,
  "views" : 1982
}, {
  "spanish" : "eliminar",
  "translations" : {
    "ENGLISH" : [ "eliminate" ]
  },
  "regular" : true,
  "views" : 1978
}, {
  "spanish" : "enfriar",
  "translations" : {
    "ENGLISH" : [ "cool" ]
  },
  "regular" : false,
  "views" : 1978
}, {
  "spanish" : "identificar",
  "translations" : {
    "ENGLISH" : [ "identify" ]
  },
  "regular" : false,
  "views" : 1978
}, {
  "spanish" : "importar",
  "translations" : {
    "ENGLISH" : [ "import", "matter" ]
  },
  "regular" : true,
  "views" : 1978
}, {
  "spanish" : "distribuir",
  "translations" : {
    "ENGLISH" : [ "distribute" ]
  },
  "regular" : false,
  "views" : 1977
}, {
  "spanish" : "grabar",
  "translations" : {
    "ENGLISH" : [ "record" ]
  },
  "regular" : true,
  "views" : 1976
}, {
  "spanish" : "comentar",
  "translations" : {
    "ENGLISH" : [ "comment" ]
  },
  "regular" : true,
  "views" : 1972
}, {
  "spanish" : "distinguir",
  "translations" : {
    "ENGLISH" : [ "distinguish" ]
  },
  "regular" : false,
  "views" : 1971
}, {
  "spanish" : "machacar",
  "translations" : {
    "ENGLISH" : [ "crush" ]
  },
  "regular" : false,
  "views" : 1970
}, {
  "spanish" : "avanzar",
  "translations" : {
    "ENGLISH" : [ "advance" ]
  },
  "regular" : false,
  "views" : 1968
}, {
  "spanish" : "abaratar",
  "translations" : {
    "ENGLISH" : [ "reduce", "cut", "cheapen" ]
  },
  "regular" : true,
  "views" : 1967
}, {
  "spanish" : "fabricar",
  "translations" : {
    "ENGLISH" : [ "manufacture", "build", "make" ]
  },
  "regular" : false,
  "views" : 1965
}, {
  "spanish" : "percatar",
  "translations" : {
    "ENGLISH" : [ "be aware of", "notice" ]
  },
  "regular" : true,
  "views" : 1964
}, {
  "spanish" : "decorar",
  "translations" : {
    "ENGLISH" : [ "decorate" ]
  },
  "regular" : true,
  "views" : 1961
}, {
  "spanish" : "relajar",
  "translations" : {
    "ENGLISH" : [ "relax" ]
  },
  "regular" : true,
  "views" : 1961
}, {
  "spanish" : "yuxtaponer",
  "translations" : {
    "ENGLISH" : [ "juxtapose" ]
  },
  "regular" : false,
  "views" : 1958
}, {
  "spanish" : "atrever",
  "translations" : {
    "ENGLISH" : [ "dare" ]
  },
  "regular" : true,
  "views" : 1958
}, {
  "spanish" : "permanecer",
  "translations" : {
    "ENGLISH" : [ "stay" ]
  },
  "regular" : false,
  "views" : 1957
}, {
  "spanish" : "pelar",
  "translations" : {
    "ENGLISH" : [ "peel" ]
  },
  "regular" : true,
  "views" : 1957
}, {
  "spanish" : "fregar",
  "translations" : {
    "ENGLISH" : [ "wash" ]
  },
  "regular" : false,
  "views" : 1956
}, {
  "spanish" : "atravesar",
  "translations" : {
    "ENGLISH" : [ "go through" ]
  },
  "regular" : false,
  "views" : 1954
}, {
  "spanish" : "observar",
  "translations" : {
    "ENGLISH" : [ "observe" ]
  },
  "regular" : true,
  "views" : 1951
}, {
  "spanish" : "precalentar",
  "translations" : {
    "ENGLISH" : [ "preheat", "warm up" ]
  },
  "regular" : false,
  "views" : 1950
}, {
  "spanish" : "bloquear",
  "translations" : {
    "ENGLISH" : [ "block" ]
  },
  "regular" : true,
  "views" : 1947
}, {
  "spanish" : "bostezar",
  "translations" : {
    "ENGLISH" : [ "yawn" ]
  },
  "regular" : false,
  "views" : 1944
}, {
  "spanish" : "fallar",
  "translations" : {
    "ENGLISH" : [ "fail" ]
  },
  "regular" : true,
  "views" : 1943
}, {
  "spanish" : "ubicar",
  "translations" : {
    "ENGLISH" : [ "put in place" ]
  },
  "regular" : false,
  "views" : 1940
}, {
  "spanish" : "atar",
  "translations" : {
    "ENGLISH" : [ "tie" ]
  },
  "regular" : true,
  "views" : 1937
}, {
  "spanish" : "nombrar",
  "translations" : {
    "ENGLISH" : [ "name" ]
  },
  "regular" : true,
  "views" : 1937
}, {
  "spanish" : "zarpar",
  "translations" : {
    "ENGLISH" : [ "set sail" ]
  },
  "regular" : true,
  "views" : 1930
}, {
  "spanish" : "abastar",
  "translations" : {
    "ENGLISH" : [ "supply" ]
  },
  "regular" : true,
  "views" : 1927
}, {
  "spanish" : "agregar",
  "translations" : {
    "ENGLISH" : [ "add" ]
  },
  "regular" : false,
  "views" : 1925
}, {
  "spanish" : "zumbar",
  "translations" : {
    "ENGLISH" : [ "buzz" ]
  },
  "regular" : true,
  "views" : 1923
}, {
  "spanish" : "existir",
  "translations" : {
    "ENGLISH" : [ "exist" ]
  },
  "regular" : true,
  "views" : 1922
}, {
  "spanish" : "pescar",
  "translations" : {
    "ENGLISH" : [ "fish" ]
  },
  "regular" : false,
  "views" : 1922
}, {
  "spanish" : "maldecir",
  "translations" : {
    "ENGLISH" : [ "curse" ]
  },
  "regular" : false,
  "views" : 1919
}, {
  "spanish" : "prometer",
  "translations" : {
    "ENGLISH" : [ "promise" ]
  },
  "regular" : true,
  "views" : 1918
}, {
  "spanish" : "florecer",
  "translations" : {
    "ENGLISH" : [ "flower" ]
  },
  "regular" : false,
  "views" : 1917
}, {
  "spanish" : "revolver",
  "translations" : {
    "ENGLISH" : [ "stir" ]
  },
  "regular" : false,
  "views" : 1912
}, {
  "spanish" : "fiar",
  "translations" : {
    "ENGLISH" : [ "trust" ]
  },
  "regular" : false,
  "views" : 1912
}, {
  "spanish" : "alegrar",
  "translations" : {
    "ENGLISH" : [ "make happy or glad" ]
  },
  "regular" : true,
  "views" : 1911
}, {
  "spanish" : "caracterizar",
  "translations" : {
    "ENGLISH" : [ "characterize" ]
  },
  "regular" : false,
  "views" : 1911
}, {
  "spanish" : "recetar",
  "translations" : {
    "ENGLISH" : [ "prescribe" ]
  },
  "regular" : true,
  "views" : 1909
}, {
  "spanish" : "copiar",
  "translations" : {
    "ENGLISH" : [ "copy" ]
  },
  "regular" : true,
  "views" : 1909
}, {
  "spanish" : "educar",
  "translations" : {
    "ENGLISH" : [ "teach", "bring up", "educate" ]
  },
  "regular" : false,
  "views" : 1908
}, {
  "spanish" : "carecer",
  "translations" : {
    "ENGLISH" : [ "lack" ]
  },
  "regular" : false,
  "views" : 1907
}, {
  "spanish" : "justificar",
  "translations" : {
    "ENGLISH" : [ "justify" ]
  },
  "regular" : false,
  "views" : 1900
}, {
  "spanish" : "denegar",
  "translations" : {
    "ENGLISH" : [ "refuse" ]
  },
  "regular" : false,
  "views" : 1899
}, {
  "spanish" : "abjurar",
  "translations" : {
    "ENGLISH" : [ "abjure", "renounce" ]
  },
  "regular" : true,
  "views" : 1898
}, {
  "spanish" : "enojar",
  "translations" : {
    "ENGLISH" : [ "make angry" ]
  },
  "regular" : true,
  "views" : 1898
}, {
  "spanish" : "lamentar",
  "translations" : {
    "ENGLISH" : [ "regret" ]
  },
  "regular" : true,
  "views" : 1897
}, {
  "spanish" : "embarcar",
  "translations" : {
    "ENGLISH" : [ "embark", "board" ]
  },
  "regular" : false,
  "views" : 1895
}, {
  "spanish" : "ablandar",
  "translations" : {
    "ENGLISH" : [ "soften" ]
  },
  "regular" : true,
  "views" : 1891
}, {
  "spanish" : "abarrotar",
  "translations" : {
    "ENGLISH" : [ "fill sth to overflowing" ]
  },
  "regular" : true,
  "views" : 1888
}, {
  "spanish" : "añadir",
  "translations" : {
    "ENGLISH" : [ "add" ]
  },
  "regular" : true,
  "views" : 1886
}, {
  "spanish" : "significar",
  "translations" : {
    "ENGLISH" : [ "mean" ]
  },
  "regular" : false,
  "views" : 1885
}, {
  "spanish" : "aconsejar",
  "translations" : {
    "ENGLISH" : [ "advise" ]
  },
  "regular" : true,
  "views" : 1877
}, {
  "spanish" : "opinar",
  "translations" : {
    "ENGLISH" : [ "think", "opine" ]
  },
  "regular" : true,
  "views" : 1877
}, {
  "spanish" : "indagar",
  "translations" : {
    "ENGLISH" : [ "investigate" ]
  },
  "regular" : false,
  "views" : 1877
}, {
  "spanish" : "reservar",
  "translations" : {
    "ENGLISH" : [ "book" ]
  },
  "regular" : true,
  "views" : 1876
}, {
  "spanish" : "luchar",
  "translations" : {
    "ENGLISH" : [ "fight" ]
  },
  "regular" : true,
  "views" : 1875
}, {
  "spanish" : "concluir",
  "translations" : {
    "ENGLISH" : [ "conclude", "finish" ]
  },
  "regular" : false,
  "views" : 1874
}, {
  "spanish" : "equivocar",
  "translations" : {
    "ENGLISH" : [ "be wrong" ]
  },
  "regular" : false,
  "views" : 1874
}, {
  "spanish" : "ofender",
  "translations" : {
    "ENGLISH" : [ "offend" ]
  },
  "regular" : true,
  "views" : 1873
}, {
  "spanish" : "robar",
  "translations" : {
    "ENGLISH" : [ "rob" ]
  },
  "regular" : true,
  "views" : 1872
}, {
  "spanish" : "patrullar",
  "translations" : {
    "ENGLISH" : [ "patrol" ]
  },
  "regular" : true,
  "views" : 1871
}, {
  "spanish" : "convenir",
  "translations" : {
    "ENGLISH" : [ "be suitable" ]
  },
  "regular" : false,
  "views" : 1870
}, {
  "spanish" : "facturar",
  "translations" : {
    "ENGLISH" : [ "check in" ]
  },
  "regular" : true,
  "views" : 1870
}, {
  "spanish" : "presentar",
  "translations" : {
    "ENGLISH" : [ "introduce" ]
  },
  "regular" : true,
  "views" : 1866
}, {
  "spanish" : "doblar",
  "translations" : {
    "ENGLISH" : [ "fold" ]
  },
  "regular" : true,
  "views" : 1864
}, {
  "spanish" : "abocetar",
  "translations" : {
    "ENGLISH" : [ "sketch" ]
  },
  "regular" : true,
  "views" : 1864
}, {
  "spanish" : "declarar",
  "translations" : {
    "ENGLISH" : [ "declare" ]
  },
  "regular" : true,
  "views" : 1864
}, {
  "spanish" : "combinar",
  "translations" : {
    "ENGLISH" : [ "combine" ]
  },
  "regular" : true,
  "views" : 1863
}, {
  "spanish" : "maltraer",
  "translations" : {
    "ENGLISH" : [ "maltreat" ]
  },
  "regular" : false,
  "views" : 1861
}, {
  "spanish" : "calmar",
  "translations" : {
    "ENGLISH" : [ "calm" ]
  },
  "regular" : true,
  "views" : 1861
}, {
  "spanish" : "charlar",
  "translations" : {
    "ENGLISH" : [ "chat" ]
  },
  "regular" : true,
  "views" : 1861
}, {
  "spanish" : "vendar",
  "translations" : {
    "ENGLISH" : [ "bandage" ]
  },
  "regular" : true,
  "views" : 1858
}, {
  "spanish" : "narrar",
  "translations" : {
    "ENGLISH" : [ "narrate", "tell" ]
  },
  "regular" : true,
  "views" : 1857
}, {
  "spanish" : "abollar",
  "translations" : {
    "ENGLISH" : [ "dent" ]
  },
  "regular" : true,
  "views" : 1855
}, {
  "spanish" : "blocar",
  "translations" : {
    "ENGLISH" : [ "block" ]
  },
  "regular" : false,
  "views" : 1851
}, {
  "spanish" : "liar",
  "translations" : {
    "ENGLISH" : [ "tieup", "roll", "wrap" ]
  },
  "regular" : false,
  "views" : 1848
}, {
  "spanish" : "becar",
  "translations" : {
    "ENGLISH" : [ "give a grant to" ]
  },
  "regular" : false,
  "views" : 1848
}, {
  "spanish" : "portar",
  "translations" : {
    "ENGLISH" : [ "bear", "carry" ]
  },
  "regular" : true,
  "views" : 1845
}, {
  "spanish" : "manifestar",
  "translations" : {
    "ENGLISH" : [ "show" ]
  },
  "regular" : false,
  "views" : 1843
}, {
  "spanish" : "finalizar",
  "translations" : {
    "ENGLISH" : [ "end" ]
  },
  "regular" : false,
  "views" : 1840
}, {
  "spanish" : "comunicar",
  "translations" : {
    "ENGLISH" : [ "communicate" ]
  },
  "regular" : false,
  "views" : 1839
}, {
  "spanish" : "componer",
  "translations" : {
    "ENGLISH" : [ "be made up of" ]
  },
  "regular" : false,
  "views" : 1837
}, {
  "spanish" : "durar",
  "translations" : {
    "ENGLISH" : [ "last" ]
  },
  "regular" : true,
  "views" : 1837
}, {
  "spanish" : "pender",
  "translations" : {
    "ENGLISH" : [ "hang from something" ]
  },
  "regular" : true,
  "views" : 1837
}, {
  "spanish" : "enseñar",
  "translations" : {
    "ENGLISH" : [ "teach" ]
  },
  "regular" : true,
  "views" : 11834
}, {
  "spanish" : "investigar",
  "translations" : {
    "ENGLISH" : [ "investigate" ]
  },
  "regular" : false,
  "views" : 1834
}, {
  "spanish" : "rezar",
  "translations" : {
    "ENGLISH" : [ "pray" ]
  },
  "regular" : false,
  "views" : 1833
}, {
  "spanish" : "razonar",
  "translations" : {
    "ENGLISH" : [ "reason" ]
  },
  "regular" : true,
  "views" : 1833
}, {
  "spanish" : "idealizar",
  "translations" : {
    "ENGLISH" : [ "idealize" ]
  },
  "regular" : false,
  "views" : 1832
}, {
  "spanish" : "emigrar",
  "translations" : {
    "ENGLISH" : [ "emigrate" ]
  },
  "regular" : true,
  "views" : 1830
}, {
  "spanish" : "favorecer",
  "translations" : {
    "ENGLISH" : [ "favour" ]
  },
  "regular" : false,
  "views" : 1829
}, {
  "spanish" : "editar",
  "translations" : {
    "ENGLISH" : [ "edit" ]
  },
  "regular" : true,
  "views" : 1828
}, {
  "spanish" : "capturar",
  "translations" : {
    "ENGLISH" : [ "capture" ]
  },
  "regular" : true,
  "views" : 1828
}, {
  "spanish" : "variar",
  "translations" : {
    "ENGLISH" : [ "vary" ]
  },
  "regular" : false,
  "views" : 1828
}, {
  "spanish" : "respetar",
  "translations" : {
    "ENGLISH" : [ "respect" ]
  },
  "regular" : true,
  "views" : 1827
}, {
  "spanish" : "vagar",
  "translations" : {
    "ENGLISH" : [ "wander", "roam", "drift" ]
  },
  "regular" : false,
  "views" : 1827
}, {
  "spanish" : "lesionar",
  "translations" : {
    "ENGLISH" : [ "damage", "injure" ]
  },
  "regular" : true,
  "views" : 1827
}, {
  "spanish" : "criar",
  "translations" : {
    "ENGLISH" : [ "breed" ]
  },
  "regular" : false,
  "views" : 1826
}, {
  "spanish" : "felicitar",
  "translations" : {
    "ENGLISH" : [ "congratulate" ]
  },
  "regular" : true,
  "views" : 1825
}, {
  "spanish" : "ejercitar",
  "translations" : {
    "ENGLISH" : [ "excercise", "train" ]
  },
  "regular" : true,
  "views" : 1821
}, {
  "spanish" : "decrecer",
  "translations" : {
    "ENGLISH" : [ "decline", "decrease" ]
  },
  "regular" : false,
  "views" : 1820
}, {
  "spanish" : "ocultar",
  "translations" : {
    "ENGLISH" : [ "hide" ]
  },
  "regular" : true,
  "views" : 1819
}, {
  "spanish" : "envolver",
  "translations" : {
    "ENGLISH" : [ "wrapup", "wind" ]
  },
  "regular" : false,
  "views" : 1818
}, {
  "spanish" : "tapar",
  "translations" : {
    "ENGLISH" : [ "cover" ]
  },
  "regular" : true,
  "views" : 1817
}, {
  "spanish" : "fichar",
  "translations" : {
    "ENGLISH" : [ "clock in" ]
  },
  "regular" : true,
  "views" : 1816
}, {
  "spanish" : "reaccionar",
  "translations" : {
    "ENGLISH" : [ "react" ]
  },
  "regular" : true,
  "views" : 1816
}, {
  "spanish" : "disminuir",
  "translations" : {
    "ENGLISH" : [ "reduce", "decrease" ]
  },
  "regular" : false,
  "views" : 1813
}, {
  "spanish" : "aumentar",
  "translations" : {
    "ENGLISH" : [ "increase" ]
  },
  "regular" : true,
  "views" : 1813
}, {
  "spanish" : "flotar",
  "translations" : {
    "ENGLISH" : [ "float" ]
  },
  "regular" : true,
  "views" : 1811
}, {
  "spanish" : "fatigar",
  "translations" : {
    "ENGLISH" : [ "weary", "tire" ]
  },
  "regular" : false,
  "views" : 1807
}, {
  "spanish" : "anunciar",
  "translations" : {
    "ENGLISH" : [ "advertise" ]
  },
  "regular" : true,
  "views" : 1806
}, {
  "spanish" : "funcionar",
  "translations" : {
    "ENGLISH" : [ "work", "run" ]
  },
  "regular" : true,
  "views" : 1804
}, {
  "spanish" : "lograr",
  "translations" : {
    "ENGLISH" : [ "achieve" ]
  },
  "regular" : true,
  "views" : 1804
}, {
  "spanish" : "subrayar",
  "translations" : {
    "ENGLISH" : [ "underline" ]
  },
  "regular" : true,
  "views" : 1803
}, {
  "spanish" : "calcular",
  "translations" : {
    "ENGLISH" : [ "calculate" ]
  },
  "regular" : true,
  "views" : 1799
}, {
  "spanish" : "atacar",
  "translations" : {
    "ENGLISH" : [ "attack" ]
  },
  "regular" : false,
  "views" : 1799
}, {
  "spanish" : "fastidiar",
  "translations" : {
    "ENGLISH" : [ "annoy" ]
  },
  "regular" : true,
  "views" : 1798
}, {
  "spanish" : "indicar",
  "translations" : {
    "ENGLISH" : [ "show", "point" ]
  },
  "regular" : false,
  "views" : 1798
}, {
  "spanish" : "deponer",
  "translations" : {
    "ENGLISH" : [ "abandon", "depose", "overthrow" ]
  },
  "regular" : false,
  "views" : 1798
}, {
  "spanish" : "edificar",
  "translations" : {
    "ENGLISH" : [ "build" ]
  },
  "regular" : false,
  "views" : 1796
}, {
  "spanish" : "agarrar",
  "translations" : {
    "ENGLISH" : [ "grab" ]
  },
  "regular" : true,
  "views" : 1796
}, {
  "spanish" : "botar",
  "translations" : {
    "ENGLISH" : [ "bounce" ]
  },
  "regular" : true,
  "views" : 1794
}, {
  "spanish" : "negociar",
  "translations" : {
    "ENGLISH" : [ "negotiate" ]
  },
  "regular" : true,
  "views" : 1794
}, {
  "spanish" : "castigar",
  "translations" : {
    "ENGLISH" : [ "punish" ]
  },
  "regular" : false,
  "views" : 1793
}, {
  "spanish" : "ignorar",
  "translations" : {
    "ENGLISH" : [ "ignore" ]
  },
  "regular" : true,
  "views" : 1789
}, {
  "spanish" : "tejer",
  "translations" : {
    "ENGLISH" : [ "braid", "crochet", "knit", "weave" ]
  },
  "regular" : true,
  "views" : 1787
}, {
  "spanish" : "emborrachar",
  "translations" : {
    "ENGLISH" : [ "get drunk" ]
  },
  "regular" : true,
  "views" : 1787
}, {
  "spanish" : "embellecer",
  "translations" : {
    "ENGLISH" : [ "make beautiful" ]
  },
  "regular" : false,
  "views" : 1787
}, {
  "spanish" : "deprimir",
  "translations" : {
    "ENGLISH" : [ "depress" ]
  },
  "regular" : true,
  "views" : 1786
}, {
  "spanish" : "interrumpir",
  "translations" : {
    "ENGLISH" : [ "interrupt" ]
  },
  "regular" : true,
  "views" : 1786
}, {
  "spanish" : "marear",
  "translations" : {
    "ENGLISH" : [ "get sick" ]
  },
  "regular" : true,
  "views" : 1784
}, {
  "spanish" : "omitir",
  "translations" : {
    "ENGLISH" : [ "omit" ]
  },
  "regular" : true,
  "views" : 1784
}, {
  "spanish" : "valpulear",
  "translations" : {
    "ENGLISH" : [ "give a beating" ]
  },
  "regular" : true,
  "views" : 1784
}, {
  "spanish" : "comprobar",
  "translations" : {
    "ENGLISH" : [ "check" ]
  },
  "regular" : false,
  "views" : 1783
}, {
  "spanish" : "predecir",
  "translations" : {
    "ENGLISH" : [ "predict" ]
  },
  "regular" : false,
  "views" : 1783
}, {
  "spanish" : "coser",
  "translations" : {
    "ENGLISH" : [ "sew" ]
  },
  "regular" : true,
  "views" : 1781
}, {
  "spanish" : "acertar",
  "translations" : {
    "ENGLISH" : [ "get right" ]
  },
  "regular" : false,
  "views" : 1780
}, {
  "spanish" : "unificar",
  "translations" : {
    "ENGLISH" : [ "unite" ]
  },
  "regular" : false,
  "views" : 1779
}, {
  "spanish" : "impresionar",
  "translations" : {
    "ENGLISH" : [ "impress" ]
  },
  "regular" : true,
  "views" : 1777
}, {
  "spanish" : "listar",
  "translations" : {
    "ENGLISH" : [ "list" ]
  },
  "regular" : true,
  "views" : 1777
}, {
  "spanish" : "sangrar",
  "translations" : {
    "ENGLISH" : [ "bleed" ]
  },
  "regular" : true,
  "views" : 1775
}, {
  "spanish" : "seducir",
  "translations" : {
    "ENGLISH" : [ "seduce" ]
  },
  "regular" : false,
  "views" : 1775
}, {
  "spanish" : "pellizcar",
  "translations" : {
    "ENGLISH" : [ "pinch" ]
  },
  "regular" : false,
  "views" : 1772
}, {
  "spanish" : "reciprocar",
  "translations" : {
    "ENGLISH" : [ "reciprocate" ]
  },
  "regular" : false,
  "views" : 1769
}, {
  "spanish" : "balancear",
  "translations" : {
    "ENGLISH" : [ "balance" ]
  },
  "regular" : true,
  "views" : 1767
}, {
  "spanish" : "contener",
  "translations" : {
    "ENGLISH" : [ "contain", "restrain", "hold back" ]
  },
  "regular" : false,
  "views" : 1765
}, {
  "spanish" : "limitar",
  "translations" : {
    "ENGLISH" : [ "limit" ]
  },
  "regular" : true,
  "views" : 1763
}, {
  "spanish" : "evitar",
  "translations" : {
    "ENGLISH" : [ "prevent" ]
  },
  "regular" : true,
  "views" : 1762
}, {
  "spanish" : "legalizar",
  "translations" : {
    "ENGLISH" : [ "legalize" ]
  },
  "regular" : false,
  "views" : 1760
}, {
  "spanish" : "dorar",
  "translations" : {
    "ENGLISH" : [ "brown" ]
  },
  "regular" : true,
  "views" : 1759
}, {
  "spanish" : "informar",
  "translations" : {
    "ENGLISH" : [ "inform" ]
  },
  "regular" : true,
  "views" : 1757
}, {
  "spanish" : "tropezar",
  "translations" : {
    "ENGLISH" : [ "stumble", "come up against", "trip" ]
  },
  "regular" : false,
  "views" : 1757
}, {
  "spanish" : "abordar",
  "translations" : {
    "ENGLISH" : [ "board" ]
  },
  "regular" : true,
  "views" : 1756
}, {
  "spanish" : "apreciar",
  "translations" : {
    "ENGLISH" : [ "esteem", "appreciate", "appraise" ]
  },
  "regular" : true,
  "views" : 1754
}, {
  "spanish" : "granizar",
  "translations" : {
    "ENGLISH" : [ "hail" ]
  },
  "regular" : false,
  "views" : 1753
}, {
  "spanish" : "facilitar",
  "translations" : {
    "ENGLISH" : [ "facilitate" ]
  },
  "regular" : true,
  "views" : 1752
}, {
  "spanish" : "calar",
  "translations" : {
    "ENGLISH" : [ "stall a car", "soak through" ]
  },
  "regular" : true,
  "views" : 1751
}, {
  "spanish" : "efectuar",
  "translations" : {
    "ENGLISH" : [ "effect" ]
  },
  "regular" : false,
  "views" : 1751
}, {
  "spanish" : "recaer",
  "translations" : {
    "ENGLISH" : [ "relapse" ]
  },
  "regular" : false,
  "views" : 1750
}, {
  "spanish" : "reconstruir",
  "translations" : {
    "ENGLISH" : [ "rebuild", "reconstruct" ]
  },
  "regular" : false,
  "views" : 1750
}, {
  "spanish" : "hundir",
  "translations" : {
    "ENGLISH" : [ "sink" ]
  },
  "regular" : true,
  "views" : 1749
}, {
  "spanish" : "blanquecer",
  "translations" : {
    "ENGLISH" : [ "whiten" ]
  },
  "regular" : false,
  "views" : 1748
}, {
  "spanish" : "degollar",
  "translations" : {
    "ENGLISH" : [ "to cut the throat of" ]
  },
  "regular" : false,
  "views" : 1746
}, {
  "spanish" : "vanagloriar",
  "translations" : {
    "ENGLISH" : [ "brag", "boast" ]
  },
  "regular" : false,
  "views" : 1746
}, {
  "spanish" : "ejecutar",
  "translations" : {
    "ENGLISH" : [ "execute" ]
  },
  "regular" : true,
  "views" : 1746
}, {
  "spanish" : "arrancar",
  "translations" : {
    "ENGLISH" : [ "start up (engine)" ]
  },
  "regular" : false,
  "views" : 1745
}, {
  "spanish" : "operar",
  "translations" : {
    "ENGLISH" : [ "operate" ]
  },
  "regular" : true,
  "views" : 1743
}, {
  "spanish" : "eclosionar",
  "translations" : {
    "ENGLISH" : [ "hatch", "open" ]
  },
  "regular" : true,
  "views" : 1743
}, {
  "spanish" : "avergonzar",
  "translations" : {
    "ENGLISH" : [ "make somebody feel ashamed" ]
  },
  "regular" : false,
  "views" : 1740
}, {
  "spanish" : "reparar",
  "translations" : {
    "ENGLISH" : [ "repair" ]
  },
  "regular" : true,
  "views" : 11739
}, {
  "spanish" : "liquefacer",
  "translations" : {
    "ENGLISH" : [ "liquify" ]
  },
  "regular" : false,
  "views" : 1738
}, {
  "spanish" : "deshacer",
  "translations" : {
    "ENGLISH" : [ "undo", "destroy", "unpack" ]
  },
  "regular" : false,
  "views" : 1737
}, {
  "spanish" : "electrificar",
  "translations" : {
    "ENGLISH" : [ "electrify" ]
  },
  "regular" : false,
  "views" : 1737
}, {
  "spanish" : "elogiar",
  "translations" : {
    "ENGLISH" : [ "praise" ]
  },
  "regular" : true,
  "views" : 1736
}, {
  "spanish" : "separar",
  "translations" : {
    "ENGLISH" : [ "separate" ]
  },
  "regular" : true,
  "views" : 1734
}, {
  "spanish" : "aparcar",
  "translations" : {
    "ENGLISH" : [ "park", "shelf" ]
  },
  "regular" : false,
  "views" : 1733
}, {
  "spanish" : "tachar",
  "translations" : {
    "ENGLISH" : [ "cross out" ]
  },
  "regular" : true,
  "views" : 1732
}, {
  "spanish" : "seleccionar",
  "translations" : {
    "ENGLISH" : [ "select" ]
  },
  "regular" : true,
  "views" : 1731
}, {
  "spanish" : "entretener",
  "translations" : {
    "ENGLISH" : [ "keep oneself ammused" ]
  },
  "regular" : false,
  "views" : 1730
}, {
  "spanish" : "guisar",
  "translations" : {
    "ENGLISH" : [ "cook" ]
  },
  "regular" : true,
  "views" : 1726
}, {
  "spanish" : "promover",
  "translations" : {
    "ENGLISH" : [ "promote", "stimulate" ]
  },
  "regular" : false,
  "views" : 1726
}, {
  "spanish" : "planchar",
  "translations" : {
    "ENGLISH" : [ "iron" ]
  },
  "regular" : true,
  "views" : 1725
}, {
  "spanish" : "validar",
  "translations" : {
    "ENGLISH" : [ "validate" ]
  },
  "regular" : true,
  "views" : 1724
}, {
  "spanish" : "valorar",
  "translations" : {
    "ENGLISH" : [ "assess", "value", "evaluate" ]
  },
  "regular" : true,
  "views" : 1724
}, {
  "spanish" : "consentir",
  "translations" : {
    "ENGLISH" : [ "allow", "permit" ]
  },
  "regular" : false,
  "views" : 1723
}, {
  "spanish" : "suceder",
  "translations" : {
    "ENGLISH" : [ "happen" ]
  },
  "regular" : true,
  "views" : 1723
}, {
  "spanish" : "cazar",
  "translations" : {
    "ENGLISH" : [ "hunt" ]
  },
  "regular" : false,
  "views" : 1722
}, {
  "spanish" : "elevar",
  "translations" : {
    "ENGLISH" : [ "raise" ]
  },
  "regular" : true,
  "views" : 1721
}, {
  "spanish" : "taladrar",
  "translations" : {
    "ENGLISH" : [ "drill" ]
  },
  "regular" : true,
  "views" : 1718
}, {
  "spanish" : "idear",
  "translations" : {
    "ENGLISH" : [ "think up" ]
  },
  "regular" : true,
  "views" : 1718
}, {
  "spanish" : "manchar",
  "translations" : {
    "ENGLISH" : [ "get dirty", "stain" ]
  },
  "regular" : true,
  "views" : 1717
}, {
  "spanish" : "admitir",
  "translations" : {
    "ENGLISH" : [ "admit", "accept" ]
  },
  "regular" : true,
  "views" : 1716
}, {
  "spanish" : "financiar",
  "translations" : {
    "ENGLISH" : [ "financiar" ]
  },
  "regular" : true,
  "views" : 1716
}, {
  "spanish" : "elaborar",
  "translations" : {
    "ENGLISH" : [ "prepare", "produce" ]
  },
  "regular" : true,
  "views" : 1715
}, {
  "spanish" : "tolerar",
  "translations" : {
    "ENGLISH" : [ "tolerate" ]
  },
  "regular" : true,
  "views" : 1712
}, {
  "spanish" : "sonrer",
  "translations" : {
    "ENGLISH" : [ "smile" ]
  },
  "regular" : false,
  "views" : 1711
}, {
  "spanish" : "ahogar",
  "translations" : {
    "ENGLISH" : [ "suffocate" ]
  },
  "regular" : false,
  "views" : 1711
}, {
  "spanish" : "vaporizar",
  "translations" : {
    "ENGLISH" : [ "vaporize" ]
  },
  "regular" : false,
  "views" : 1711
}, {
  "spanish" : "distraer",
  "translations" : {
    "ENGLISH" : [ "entertain", "amuse" ]
  },
  "regular" : false,
  "views" : 1710
}, {
  "spanish" : "extender",
  "translations" : {
    "ENGLISH" : [ "stretch out" ]
  },
  "regular" : false,
  "views" : 1710
}, {
  "spanish" : "revisar",
  "translations" : {
    "ENGLISH" : [ "check" ]
  },
  "regular" : true,
  "views" : 1710
}, {
  "spanish" : "resultar",
  "translations" : {
    "ENGLISH" : [ "turn out to be" ]
  },
  "regular" : true,
  "views" : 1710
}, {
  "spanish" : "pesar",
  "translations" : {
    "ENGLISH" : [ "weigh" ]
  },
  "regular" : true,
  "views" : 1709
}, {
  "spanish" : "deformar",
  "translations" : {
    "ENGLISH" : [ "deform" ]
  },
  "regular" : true,
  "views" : 1708
}, {
  "spanish" : "dividir",
  "translations" : {
    "ENGLISH" : [ "divide" ]
  },
  "regular" : true,
  "views" : 1706
}, {
  "spanish" : "malograr",
  "translations" : {
    "ENGLISH" : [ "waste", "spoil", "ruin" ]
  },
  "regular" : true,
  "views" : 1706
}, {
  "spanish" : "incorporar",
  "translations" : {
    "ENGLISH" : [ "include" ]
  },
  "regular" : true,
  "views" : 1704
}, {
  "spanish" : "imitar",
  "translations" : {
    "ENGLISH" : [ "imitate" ]
  },
  "regular" : true,
  "views" : 1703
}, {
  "spanish" : "aborrecer",
  "translations" : {
    "ENGLISH" : [ "loathe" ]
  },
  "regular" : false,
  "views" : 1702
}, {
  "spanish" : "numerar",
  "translations" : {
    "ENGLISH" : [ "number" ]
  },
  "regular" : true,
  "views" : 1700
}, {
  "spanish" : "iluminar",
  "translations" : {
    "ENGLISH" : [ "light up" ]
  },
  "regular" : true,
  "views" : 1698
}, {
  "spanish" : "mojar",
  "translations" : {
    "ENGLISH" : [ "get wet" ]
  },
  "regular" : true,
  "views" : 1698
}, {
  "spanish" : "falsificar",
  "translations" : {
    "ENGLISH" : [ "forge" ]
  },
  "regular" : false,
  "views" : 1697
}, {
  "spanish" : "generalizar",
  "translations" : {
    "ENGLISH" : [ "generalize", "spread" ]
  },
  "regular" : false,
  "views" : 1697
}, {
  "spanish" : "asustar",
  "translations" : {
    "ENGLISH" : [ "get scared", "frighten" ]
  },
  "regular" : true,
  "views" : 1697
}, {
  "spanish" : "entrenar",
  "translations" : {
    "ENGLISH" : [ "train" ]
  },
  "regular" : true,
  "views" : 1697
}, {
  "spanish" : "tragar",
  "translations" : {
    "ENGLISH" : [ "swallow" ]
  },
  "regular" : false,
  "views" : 1696
}, {
  "spanish" : "votar",
  "translations" : {
    "ENGLISH" : [ "vote" ]
  },
  "regular" : true,
  "views" : 1696
}, {
  "spanish" : "tararear",
  "translations" : {
    "ENGLISH" : [ "go la la la" ]
  },
  "regular" : true,
  "views" : 1693
}, {
  "spanish" : "reclamar",
  "translations" : {
    "ENGLISH" : [ "claim", "demand" ]
  },
  "regular" : true,
  "views" : 1692
}, {
  "spanish" : "guarnecer",
  "translations" : {
    "ENGLISH" : [ "garnish", "adorn" ]
  },
  "regular" : false,
  "views" : 1692
}, {
  "spanish" : "salpicar",
  "translations" : {
    "ENGLISH" : [ "splash" ]
  },
  "regular" : false,
  "views" : 1691
}, {
  "spanish" : "vedar",
  "translations" : {
    "ENGLISH" : [ "ban" ]
  },
  "regular" : true,
  "views" : 1690
}, {
  "spanish" : "confundir",
  "translations" : {
    "ENGLISH" : [ "make a mistake" ]
  },
  "regular" : false,
  "views" : 1689
}, {
  "spanish" : "estornudar",
  "translations" : {
    "ENGLISH" : [ "sneeze" ]
  },
  "regular" : true,
  "views" : 1686
}, {
  "spanish" : "rebajar",
  "translations" : {
    "ENGLISH" : [ "reduce" ]
  },
  "regular" : true,
  "views" : 1685
}, {
  "spanish" : "vacunar",
  "translations" : {
    "ENGLISH" : [ "vaccinate" ]
  },
  "regular" : true,
  "views" : 1685
}, {
  "spanish" : "sustituir",
  "translations" : {
    "ENGLISH" : [ "substitute" ]
  },
  "regular" : false,
  "views" : 1684
}, {
  "spanish" : "emplear",
  "translations" : {
    "ENGLISH" : [ "employ" ]
  },
  "regular" : true,
  "views" : 1683
}, {
  "spanish" : "uniformar",
  "translations" : {
    "ENGLISH" : [ "standardise" ]
  },
  "regular" : true,
  "views" : 1683
}, {
  "spanish" : "aplicar",
  "translations" : {
    "ENGLISH" : [ "apply" ]
  },
  "regular" : false,
  "views" : 1682
}, {
  "spanish" : "varar",
  "translations" : {
    "ENGLISH" : [ "run aground" ]
  },
  "regular" : true,
  "views" : 1680
}, {
  "spanish" : "completar",
  "translations" : {
    "ENGLISH" : [ "complete" ]
  },
  "regular" : true,
  "views" : 1679
}, {
  "spanish" : "conjugar",
  "translations" : {
    "ENGLISH" : [ "conjugate" ]
  },
  "regular" : false,
  "views" : 1679
}, {
  "spanish" : "acampar",
  "translations" : {
    "ENGLISH" : [ "camp" ]
  },
  "regular" : true,
  "views" : 1678
}, {
  "spanish" : "enterar",
  "translations" : {
    "ENGLISH" : [ "understand", "find out" ]
  },
  "regular" : true,
  "views" : 1678
}, {
  "spanish" : "desarrollar",
  "translations" : {
    "ENGLISH" : [ "develop" ]
  },
  "regular" : true,
  "views" : 1678
}, {
  "spanish" : "graznar",
  "translations" : {
    "ENGLISH" : [ "croak" ]
  },
  "regular" : true,
  "views" : 1677
}, {
  "spanish" : "vacilar",
  "translations" : {
    "ENGLISH" : [ "hesitate" ]
  },
  "regular" : true,
  "views" : 1676
}, {
  "spanish" : "malinterpretar",
  "translations" : {
    "ENGLISH" : [ "misinterpret" ]
  },
  "regular" : true,
  "views" : 1676
}, {
  "spanish" : "vadear",
  "translations" : {
    "ENGLISH" : [ "cross", "ford", "wade across" ]
  },
  "regular" : true,
  "views" : 1675
}, {
  "spanish" : "amanecer",
  "translations" : {
    "ENGLISH" : [ "dawn" ]
  },
  "regular" : false,
  "views" : 1674
}, {
  "spanish" : "peligrar",
  "translations" : {
    "ENGLISH" : [ "be at risk" ]
  },
  "regular" : true,
  "views" : 1673
}, {
  "spanish" : "perdonar",
  "translations" : {
    "ENGLISH" : [ "forgive" ]
  },
  "regular" : true,
  "views" : 1671
}, {
  "spanish" : "escapar",
  "translations" : {
    "ENGLISH" : [ "escape" ]
  },
  "regular" : true,
  "views" : 1670
}, {
  "spanish" : "cometer",
  "translations" : {
    "ENGLISH" : [ "commit" ]
  },
  "regular" : true,
  "views" : 1670
}, {
  "spanish" : "generar",
  "translations" : {
    "ENGLISH" : [ "generate" ]
  },
  "regular" : true,
  "views" : 1670
}, {
  "spanish" : "prevenir",
  "translations" : {
    "ENGLISH" : [ "prevent" ]
  },
  "regular" : false,
  "views" : 1669
}, {
  "spanish" : "valorizar",
  "translations" : {
    "ENGLISH" : [ "increase in value" ]
  },
  "regular" : false,
  "views" : 1669
}, {
  "spanish" : "tallar",
  "translations" : {
    "ENGLISH" : [ "carve", "sculpt" ]
  },
  "regular" : true,
  "views" : 1669
}, {
  "spanish" : "consumir",
  "translations" : {
    "ENGLISH" : [ "consume" ]
  },
  "regular" : false,
  "views" : 1668
}, {
  "spanish" : "despegar",
  "translations" : {
    "ENGLISH" : [ "take off" ]
  },
  "regular" : false,
  "views" : 1667
}, {
  "spanish" : "garantizar",
  "translations" : {
    "ENGLISH" : [ "guarantee" ]
  },
  "regular" : false,
  "views" : 1665
}, {
  "spanish" : "superar",
  "translations" : {
    "ENGLISH" : [ "overcome", "get over" ]
  },
  "regular" : true,
  "views" : 1662
}, {
  "spanish" : "surgir",
  "translations" : {
    "ENGLISH" : [ "arise" ]
  },
  "regular" : false,
  "views" : 1658
}, {
  "spanish" : "analizar",
  "translations" : {
    "ENGLISH" : [ "analyze" ]
  },
  "regular" : false,
  "views" : 1655
}, {
  "spanish" : "convivir",
  "translations" : {
    "ENGLISH" : [ "live together" ]
  },
  "regular" : true,
  "views" : 1652
}, {
  "spanish" : "ascender",
  "translations" : {
    "ENGLISH" : [ "goup" ]
  },
  "regular" : false,
  "views" : 1651
}, {
  "spanish" : "abrigar",
  "translations" : {
    "ENGLISH" : [ "keep warm" ]
  },
  "regular" : false,
  "views" : 1651
}, {
  "spanish" : "excluir",
  "translations" : {
    "ENGLISH" : [ "exclude" ]
  },
  "regular" : false,
  "views" : 1648
}, {
  "spanish" : "bisbisear",
  "translations" : {
    "ENGLISH" : [ "whisper" ]
  },
  "regular" : true,
  "views" : 1648
}, {
  "spanish" : "valladar",
  "translations" : {
    "ENGLISH" : [ "fence" ]
  },
  "regular" : true,
  "views" : 1647
}, {
  "spanish" : "escalar",
  "translations" : {
    "ENGLISH" : [ "climb" ]
  },
  "regular" : true,
  "views" : 1646
}, {
  "spanish" : "examinar",
  "translations" : {
    "ENGLISH" : [ "examine" ]
  },
  "regular" : true,
  "views" : 1646
}, {
  "spanish" : "inaugurar",
  "translations" : {
    "ENGLISH" : [ "inaugurate" ]
  },
  "regular" : true,
  "views" : 1646
}, {
  "spanish" : "envidiar",
  "translations" : {
    "ENGLISH" : [ "envy" ]
  },
  "regular" : true,
  "views" : 1643
}, {
  "spanish" : "rendir",
  "translations" : {
    "ENGLISH" : [ "surrender" ]
  },
  "regular" : false,
  "views" : 1639
}, {
  "spanish" : "pelotear",
  "translations" : {
    "ENGLISH" : [ "kick a ball around" ]
  },
  "regular" : true,
  "views" : 1639
}, {
  "spanish" : "incurrir",
  "translations" : {
    "ENGLISH" : [ "incur" ]
  },
  "regular" : true,
  "views" : 1638
}, {
  "spanish" : "maliciar",
  "translations" : {
    "ENGLISH" : [ "suspect" ]
  },
  "regular" : true,
  "views" : 1637
}, {
  "spanish" : "inclinar",
  "translations" : {
    "ENGLISH" : [ "lean", "tilt" ]
  },
  "regular" : true,
  "views" : 1636
}, {
  "spanish" : "orientar",
  "translations" : {
    "ENGLISH" : [ "orientate" ]
  },
  "regular" : true,
  "views" : 1636
}, {
  "spanish" : "legislar",
  "translations" : {
    "ENGLISH" : [ "legislate" ]
  },
  "regular" : true,
  "views" : 1636
}, {
  "spanish" : "apretar",
  "translations" : {
    "ENGLISH" : [ "press" ]
  },
  "regular" : false,
  "views" : 1635
}, {
  "spanish" : "alojar",
  "translations" : {
    "ENGLISH" : [ "accommodate", "put up" ]
  },
  "regular" : true,
  "views" : 1634
}, {
  "spanish" : "bañar",
  "translations" : {
    "ENGLISH" : [ "have a bath", "go for a swim" ]
  },
  "regular" : true,
  "views" : 1634
}, {
  "spanish" : "litigar",
  "translations" : {
    "ENGLISH" : [ "litigate" ]
  },
  "regular" : false,
  "views" : 1634
}, {
  "spanish" : "empujar",
  "translations" : {
    "ENGLISH" : [ "push" ]
  },
  "regular" : true,
  "views" : 1630
}, {
  "spanish" : "encargar",
  "translations" : {
    "ENGLISH" : [ "ask somebody to do something" ]
  },
  "regular" : false,
  "views" : 1630
}, {
  "spanish" : "valsear",
  "translations" : {
    "ENGLISH" : [ "waltz" ]
  },
  "regular" : true,
  "views" : 1628
}, {
  "spanish" : "platicar",
  "translations" : {
    "ENGLISH" : [ "chat" ]
  },
  "regular" : false,
  "views" : 1627
}, {
  "spanish" : "cobrar",
  "translations" : {
    "ENGLISH" : [ "charge" ]
  },
  "regular" : true,
  "views" : 1626
}, {
  "spanish" : "afectar",
  "translations" : {
    "ENGLISH" : [ "affect" ]
  },
  "regular" : true,
  "views" : 1624
}, {
  "spanish" : "asegurar",
  "translations" : {
    "ENGLISH" : [ "ensure" ]
  },
  "regular" : true,
  "views" : 1623
}, {
  "spanish" : "aplaudir",
  "translations" : {
    "ENGLISH" : [ "applaud" ]
  },
  "regular" : true,
  "views" : 1622
}, {
  "spanish" : "memorizar",
  "translations" : {
    "ENGLISH" : [ "memorize" ]
  },
  "regular" : false,
  "views" : 1620
}, {
  "spanish" : "acudir",
  "translations" : {
    "ENGLISH" : [ "come go attend" ]
  },
  "regular" : true,
  "views" : 1620
}, {
  "spanish" : "impulsar",
  "translations" : {
    "ENGLISH" : [ "drive", "stimulate" ]
  },
  "regular" : true,
  "views" : 1617
}, {
  "spanish" : "formar",
  "translations" : {
    "ENGLISH" : [ "form" ]
  },
  "regular" : true,
  "views" : 1615
}, {
  "spanish" : "recitar",
  "translations" : {
    "ENGLISH" : [ "recite" ]
  },
  "regular" : true,
  "views" : 1615
}, {
  "spanish" : "destacar",
  "translations" : {
    "ENGLISH" : [ "point out" ]
  },
  "regular" : false,
  "views" : 1611
}, {
  "spanish" : "engordar",
  "translations" : {
    "ENGLISH" : [ "put on weight" ]
  },
  "regular" : true,
  "views" : 1610
}, {
  "spanish" : "controlar",
  "translations" : {
    "ENGLISH" : [ "control" ]
  },
  "regular" : true,
  "views" : 1609
}, {
  "spanish" : "emitir",
  "translations" : {
    "ENGLISH" : [ "broadcast" ]
  },
  "regular" : true,
  "views" : 1608
}, {
  "spanish" : "rescatar",
  "translations" : {
    "ENGLISH" : [ "rescue" ]
  },
  "regular" : true,
  "views" : 1606
}, {
  "spanish" : "descargar",
  "translations" : {
    "ENGLISH" : [ "unload" ]
  },
  "regular" : false,
  "views" : 1605
}, {
  "spanish" : "incendiar",
  "translations" : {
    "ENGLISH" : [ "set fire to" ]
  },
  "regular" : true,
  "views" : 1604
}, {
  "spanish" : "contraer",
  "translations" : {
    "ENGLISH" : [ "contract", "catch", "acquire" ]
  },
  "regular" : false,
  "views" : 1603
}, {
  "spanish" : "manar",
  "translations" : {
    "ENGLISH" : [ "be rich in", "pour" ]
  },
  "regular" : true,
  "views" : 1603
}, {
  "spanish" : "humillar",
  "translations" : {
    "ENGLISH" : [ "humiliate" ]
  },
  "regular" : true,
  "views" : 1602
}, {
  "spanish" : "abusar",
  "translations" : {
    "ENGLISH" : [ "abuse" ]
  },
  "regular" : true,
  "views" : 1599
}, {
  "spanish" : "iniciar",
  "translations" : {
    "ENGLISH" : [ "begin" ]
  },
  "regular" : true,
  "views" : 1599
}, {
  "spanish" : "admirar",
  "translations" : {
    "ENGLISH" : [ "admire" ]
  },
  "regular" : true,
  "views" : 1598
}, {
  "spanish" : "criticar",
  "translations" : {
    "ENGLISH" : [ "criticize", "gossip" ]
  },
  "regular" : false,
  "views" : 1596
}, {
  "spanish" : "gandulear",
  "translations" : {
    "ENGLISH" : [ "loaf around" ]
  },
  "regular" : true,
  "views" : 1596
}, {
  "spanish" : "tablear",
  "translations" : {
    "ENGLISH" : [ "pleat" ]
  },
  "regular" : true,
  "views" : 1594
}, {
  "spanish" : "considerar",
  "translations" : {
    "ENGLISH" : [ "consider" ]
  },
  "regular" : true,
  "views" : 1594
}, {
  "spanish" : "sostener",
  "translations" : {
    "ENGLISH" : [ "support" ]
  },
  "regular" : false,
  "views" : 1589
}, {
  "spanish" : "intuir",
  "translations" : {
    "ENGLISH" : [ "sense" ]
  },
  "regular" : false,
  "views" : 1589
}, {
  "spanish" : "acoger",
  "translations" : {
    "ENGLISH" : [ "receive", "takein", "welcome" ]
  },
  "regular" : false,
  "views" : 1587
}, {
  "spanish" : "explotar",
  "translations" : {
    "ENGLISH" : [ "explode", "exploit" ]
  },
  "regular" : true,
  "views" : 1587
}, {
  "spanish" : "valsar",
  "translations" : {
    "ENGLISH" : [ "waltz" ]
  },
  "regular" : true,
  "views" : 1586
}, {
  "spanish" : "encerrar",
  "translations" : {
    "ENGLISH" : [ "shut in", "shut up" ]
  },
  "regular" : false,
  "views" : 1582
}, {
  "spanish" : "solicitar",
  "translations" : {
    "ENGLISH" : [ "request", "apply" ]
  },
  "regular" : true,
  "views" : 1581
}, {
  "spanish" : "alimentar",
  "translations" : {
    "ENGLISH" : [ "feed" ]
  },
  "regular" : true,
  "views" : 1579
}, {
  "spanish" : "salivar",
  "translations" : {
    "ENGLISH" : [ "salivate" ]
  },
  "regular" : true,
  "views" : 1577
}, {
  "spanish" : "inventar",
  "translations" : {
    "ENGLISH" : [ "invent" ]
  },
  "regular" : true,
  "views" : 1573
}, {
  "spanish" : "acostumbrar",
  "translations" : {
    "ENGLISH" : [ "get used to" ]
  },
  "regular" : true,
  "views" : 1572
}, {
  "spanish" : "aplazar",
  "translations" : {
    "ENGLISH" : [ "postpone" ]
  },
  "regular" : false,
  "views" : 1570
}, {
  "spanish" : "prevaler",
  "translations" : {
    "ENGLISH" : [ "prevail" ]
  },
  "regular" : false,
  "views" : 1568
}, {
  "spanish" : "adivinar",
  "translations" : {
    "ENGLISH" : [ "guess" ]
  },
  "regular" : true,
  "views" : 1563
}, {
  "spanish" : "contactar",
  "translations" : {
    "ENGLISH" : [ "contact" ]
  },
  "regular" : true,
  "views" : 1563
}, {
  "spanish" : "tostar",
  "translations" : {
    "ENGLISH" : [ "toast" ]
  },
  "regular" : false,
  "views" : 1562
}, {
  "spanish" : "animar",
  "translations" : {
    "ENGLISH" : [ "cheer somebody up" ]
  },
  "regular" : true,
  "views" : 1561
}, {
  "spanish" : "inscribir",
  "translations" : {
    "ENGLISH" : [ "inscribe" ]
  },
  "regular" : false,
  "views" : 1560
}, {
  "spanish" : "agotar",
  "translations" : {
    "ENGLISH" : [ "run out", "exhaust" ]
  },
  "regular" : true,
  "views" : 1559
}, {
  "spanish" : "acentuar",
  "translations" : {
    "ENGLISH" : [ "stress", "accent" ]
  },
  "regular" : false,
  "views" : 1556
}, {
  "spanish" : "colaborar",
  "translations" : {
    "ENGLISH" : [ "collaborate" ]
  },
  "regular" : true,
  "views" : 1554
}, {
  "spanish" : "adorar",
  "translations" : {
    "ENGLISH" : [ "adore", "worship" ]
  },
  "regular" : true,
  "views" : 1551
}, {
  "spanish" : "resfriar",
  "translations" : {
    "ENGLISH" : [ "catch a cold" ]
  },
  "regular" : false,
  "views" : 1551
}, {
  "spanish" : "absolver",
  "translations" : {
    "ENGLISH" : [ "absolve" ]
  },
  "regular" : false,
  "views" : 1549
}, {
  "spanish" : "prever",
  "translations" : {
    "ENGLISH" : [ "foresee" ]
  },
  "regular" : false,
  "views" : 1549
}, {
  "spanish" : "divorciar",
  "translations" : {
    "ENGLISH" : [ "divorce" ]
  },
  "regular" : true,
  "views" : 1549
}, {
  "spanish" : "acariciar",
  "translations" : {
    "ENGLISH" : [ "caress", "stroke" ]
  },
  "regular" : true,
  "views" : 1546
}, {
  "spanish" : "agitar",
  "translations" : {
    "ENGLISH" : [ "shake" ]
  },
  "regular" : true,
  "views" : 1546
}, {
  "spanish" : "contagiar",
  "translations" : {
    "ENGLISH" : [ "transmit" ]
  },
  "regular" : true,
  "views" : 1545
}, {
  "spanish" : "malinformar",
  "translations" : {
    "ENGLISH" : [ "misinform" ]
  },
  "regular" : true,
  "views" : 1545
}, {
  "spanish" : "aguantar",
  "translations" : {
    "ENGLISH" : [ "put up with something", "stand" ]
  },
  "regular" : true,
  "views" : 1543
}, {
  "spanish" : "derretir",
  "translations" : {
    "ENGLISH" : [ "melt" ]
  },
  "regular" : false,
  "views" : 1543
}, {
  "spanish" : "asir",
  "translations" : {
    "ENGLISH" : [ "grasp", "seize" ]
  },
  "regular" : false,
  "views" : 1539
}, {
  "spanish" : "retirar",
  "translations" : {
    "ENGLISH" : [ "retire", "withdraw" ]
  },
  "regular" : true,
  "views" : 1538
}, {
  "spanish" : "broncear",
  "translations" : {
    "ENGLISH" : [ "get a suntan" ]
  },
  "regular" : true,
  "views" : 1537
}, {
  "spanish" : "atribuir",
  "translations" : {
    "ENGLISH" : [ "attribute" ]
  },
  "regular" : false,
  "views" : 1537
}, {
  "spanish" : "repartir",
  "translations" : {
    "ENGLISH" : [ "distribute" ]
  },
  "regular" : true,
  "views" : 1535
}, {
  "spanish" : "mascar",
  "translations" : {
    "ENGLISH" : [ "chew" ]
  },
  "regular" : false,
  "views" : 1534
}, {
  "spanish" : "dimitir",
  "translations" : {
    "ENGLISH" : [ "resign" ]
  },
  "regular" : true,
  "views" : 1531
}, {
  "spanish" : "disponer",
  "translations" : {
    "ENGLISH" : [ "haveavailable" ]
  },
  "regular" : false,
  "views" : 1530
}, {
  "spanish" : "refrescar",
  "translations" : {
    "ENGLISH" : [ "chill", "refresh" ]
  },
  "regular" : false,
  "views" : 1530
}, {
  "spanish" : "espiar",
  "translations" : {
    "ENGLISH" : [ "spyon" ]
  },
  "regular" : false,
  "views" : 1527
}, {
  "spanish" : "consistir",
  "translations" : {
    "ENGLISH" : [ "consist of" ]
  },
  "regular" : true,
  "views" : 1527
}, {
  "spanish" : "respirar",
  "translations" : {
    "ENGLISH" : [ "breathe" ]
  },
  "regular" : true,
  "views" : 1524
}, {
  "spanish" : "revelar",
  "translations" : {
    "ENGLISH" : [ "reveal" ]
  },
  "regular" : true,
  "views" : 1524
}, {
  "spanish" : "alejar",
  "translations" : {
    "ENGLISH" : [ "move away" ]
  },
  "regular" : true,
  "views" : 1523
}, {
  "spanish" : "apellidar",
  "translations" : {
    "ENGLISH" : [ "Verb say the surname" ]
  },
  "regular" : true,
  "views" : 1522
}, {
  "spanish" : "contradecir",
  "translations" : {
    "ENGLISH" : [ "contradict" ]
  },
  "regular" : false,
  "views" : 1521
}, {
  "spanish" : "posponer",
  "translations" : {
    "ENGLISH" : [ "postpone" ]
  },
  "regular" : false,
  "views" : 1521
}, {
  "spanish" : "pronunciar",
  "translations" : {
    "ENGLISH" : [ "pronounce" ]
  },
  "regular" : true,
  "views" : 1521
}, {
  "spanish" : "comparar",
  "translations" : {
    "ENGLISH" : [ "compare" ]
  },
  "regular" : true,
  "views" : 1521
}, {
  "spanish" : "contaminar",
  "translations" : {
    "ENGLISH" : [ "contaminate" ]
  },
  "regular" : true,
  "views" : 1521
}, {
  "spanish" : "reflexionar",
  "translations" : {
    "ENGLISH" : [ "reflect" ]
  },
  "regular" : true,
  "views" : 1520
}, {
  "spanish" : "forzar",
  "translations" : {
    "ENGLISH" : [ "force" ]
  },
  "regular" : false,
  "views" : 1520
}, {
  "spanish" : "mentar",
  "translations" : {
    "ENGLISH" : [ "mention" ]
  },
  "regular" : false,
  "views" : 1519
}, {
  "spanish" : "alentar",
  "translations" : {
    "ENGLISH" : [ "encourage" ]
  },
  "regular" : false,
  "views" : 1517
}, {
  "spanish" : "alzar",
  "translations" : {
    "ENGLISH" : [ "raise", "lift", "hoist" ]
  },
  "regular" : false,
  "views" : 1517
}, {
  "spanish" : "abrochar",
  "translations" : {
    "ENGLISH" : [ "do up" ]
  },
  "regular" : true,
  "views" : 1517
}, {
  "spanish" : "estresar",
  "translations" : {
    "ENGLISH" : [ "stress" ]
  },
  "regular" : true,
  "views" : 1517
}, {
  "spanish" : "averiar",
  "translations" : {
    "ENGLISH" : [ "break down" ]
  },
  "regular" : false,
  "views" : 1513
}, {
  "spanish" : "cultivar",
  "translations" : {
    "ENGLISH" : [ "cultivate" ]
  },
  "regular" : true,
  "views" : 1512
}, {
  "spanish" : "combatir",
  "translations" : {
    "ENGLISH" : [ "combat", "fight" ]
  },
  "regular" : true,
  "views" : 1511
}, {
  "spanish" : "aguar",
  "translations" : {
    "ENGLISH" : [ "water down" ]
  },
  "regular" : false,
  "views" : 1510
}, {
  "spanish" : "absorber",
  "translations" : {
    "ENGLISH" : [ "absorb" ]
  },
  "regular" : true,
  "views" : 1510
}, {
  "spanish" : "curar",
  "translations" : {
    "ENGLISH" : [ "cure" ]
  },
  "regular" : true,
  "views" : 1508
}, {
  "spanish" : "anochecer",
  "translations" : {
    "ENGLISH" : [ "get dark" ]
  },
  "regular" : false,
  "views" : 1507
}, {
  "spanish" : "deslizar",
  "translations" : {
    "ENGLISH" : [ "slip" ]
  },
  "regular" : false,
  "views" : 1506
}, {
  "spanish" : "absorver",
  "translations" : {
    "ENGLISH" : [ "absorb" ]
  },
  "regular" : true,
  "views" : 1504
}, {
  "spanish" : "corresponder",
  "translations" : {
    "ENGLISH" : [ "correspond" ]
  },
  "regular" : true,
  "views" : 1504
}, {
  "spanish" : "descender",
  "translations" : {
    "ENGLISH" : [ "go down" ]
  },
  "regular" : false,
  "views" : 1501
}, {
  "spanish" : "ampliar",
  "translations" : {
    "ENGLISH" : [ "extend" ]
  },
  "regular" : false,
  "views" : 1501
}, {
  "spanish" : "almacenar",
  "translations" : {
    "ENGLISH" : [ "store" ]
  },
  "regular" : true,
  "views" : 1500
}, {
  "spanish" : "citar",
  "translations" : {
    "ENGLISH" : [ "arrange to meet" ]
  },
  "regular" : true,
  "views" : 1500
}, {
  "spanish" : "derramar",
  "translations" : {
    "ENGLISH" : [ "spill" ]
  },
  "regular" : true,
  "views" : 1500
}, {
  "spanish" : "soltar",
  "translations" : {
    "ENGLISH" : [ "letgoof" ]
  },
  "regular" : false,
  "views" : 1499
}, {
  "spanish" : "estirar",
  "translations" : {
    "ENGLISH" : [ "stretch" ]
  },
  "regular" : true,
  "views" : 1499
}, {
  "spanish" : "comercializar",
  "translations" : {
    "ENGLISH" : [ "commercialize" ]
  },
  "regular" : false,
  "views" : 1498
}, {
  "spanish" : "registrar",
  "translations" : {
    "ENGLISH" : [ "register" ]
  },
  "regular" : true,
  "views" : 1498
}, {
  "spanish" : "bullir",
  "translations" : {
    "ENGLISH" : [ "boil" ]
  },
  "regular" : false,
  "views" : 1497
}, {
  "spanish" : "conservar",
  "translations" : {
    "ENGLISH" : [ "preserve", "conserve" ]
  },
  "regular" : true,
  "views" : 1497
}, {
  "spanish" : "representar",
  "translations" : {
    "ENGLISH" : [ "represent" ]
  },
  "regular" : true,
  "views" : 1496
}, {
  "spanish" : "torear",
  "translations" : {
    "ENGLISH" : [ "fight a bull" ]
  },
  "regular" : true,
  "views" : 1496
}, {
  "spanish" : "aterrorizar",
  "translations" : {
    "ENGLISH" : [ "terrorize" ]
  },
  "regular" : false,
  "views" : 1495
}, {
  "spanish" : "apostar",
  "translations" : {
    "ENGLISH" : [ "bet" ]
  },
  "regular" : false,
  "views" : 1495
}, {
  "spanish" : "enfadar",
  "translations" : {
    "ENGLISH" : [ "get angry" ]
  },
  "regular" : true,
  "views" : 1495
}, {
  "spanish" : "ingresar",
  "translations" : {
    "ENGLISH" : [ "pay in" ]
  },
  "regular" : true,
  "views" : 1495
}, {
  "spanish" : "desafiar",
  "translations" : {
    "ENGLISH" : [ "challenge" ]
  },
  "regular" : false,
  "views" : 1495
}, {
  "spanish" : "abrasar",
  "translations" : {
    "ENGLISH" : [ "burn" ]
  },
  "regular" : true,
  "views" : 1494
}, {
  "spanish" : "autorizar",
  "translations" : {
    "ENGLISH" : [ "authorize" ]
  },
  "regular" : false,
  "views" : 1494
}, {
  "spanish" : "regatear",
  "translations" : {
    "ENGLISH" : [ "haggle" ]
  },
  "regular" : true,
  "views" : 1493
}, {
  "spanish" : "contratar",
  "translations" : {
    "ENGLISH" : [ "contract for" ]
  },
  "regular" : true,
  "views" : 1493
}, {
  "spanish" : "esforzar",
  "translations" : {
    "ENGLISH" : [ "strain" ]
  },
  "regular" : false,
  "views" : 1493
}, {
  "spanish" : "pisar",
  "translations" : {
    "ENGLISH" : [ "step on" ]
  },
  "regular" : true,
  "views" : 1491
}, {
  "spanish" : "acusar",
  "translations" : {
    "ENGLISH" : [ "accuse" ]
  },
  "regular" : true,
  "views" : 1489
}, {
  "spanish" : "exponer",
  "translations" : {
    "ENGLISH" : [ "expose" ]
  },
  "regular" : false,
  "views" : 1488
}, {
  "spanish" : "agriar",
  "translations" : {
    "ENGLISH" : [ "turn sour" ]
  },
  "regular" : false,
  "views" : 1487
}, {
  "spanish" : "chupar",
  "translations" : {
    "ENGLISH" : [ "suck" ]
  },
  "regular" : true,
  "views" : 1486
}, {
  "spanish" : "adornar",
  "translations" : {
    "ENGLISH" : [ "decorate", "adorn" ]
  },
  "regular" : true,
  "views" : 1486
}, {
  "spanish" : "certificar",
  "translations" : {
    "ENGLISH" : [ "confirm" ]
  },
  "regular" : false,
  "views" : 1485
}, {
  "spanish" : "asesinar",
  "translations" : {
    "ENGLISH" : [ "murder" ]
  },
  "regular" : true,
  "views" : 1484
}, {
  "spanish" : "acompañar",
  "translations" : {
    "ENGLISH" : [ "accompany", "go with" ]
  },
  "regular" : true,
  "views" : 1482
}, {
  "spanish" : "ajustar",
  "translations" : {
    "ENGLISH" : [ "adjust" ]
  },
  "regular" : true,
  "views" : 1481
}, {
  "spanish" : "atropellar",
  "translations" : {
    "ENGLISH" : [ "run over" ]
  },
  "regular" : true,
  "views" : 1481
}, {
  "spanish" : "disculpar",
  "translations" : {
    "ENGLISH" : [ "forgive", "apologize" ]
  },
  "regular" : true,
  "views" : 1481
}, {
  "spanish" : "concebir",
  "translations" : {
    "ENGLISH" : [ "conceive" ]
  },
  "regular" : false,
  "views" : 1481
}, {
  "spanish" : "intervenir",
  "translations" : {
    "ENGLISH" : [ "takepart", "get involved" ]
  },
  "regular" : false,
  "views" : 1480
}, {
  "spanish" : "rehacer",
  "translations" : {
    "ENGLISH" : [ "rebuild", "doagain" ]
  },
  "regular" : false,
  "views" : 1480
}, {
  "spanish" : "explorar",
  "translations" : {
    "ENGLISH" : [ "explore" ]
  },
  "regular" : true,
  "views" : 1479
}, {
  "spanish" : "cascar",
  "translations" : {
    "ENGLISH" : [ "crack" ]
  },
  "regular" : false,
  "views" : 1476
}, {
  "spanish" : "alumbrar",
  "translations" : {
    "ENGLISH" : [ "light" ]
  },
  "regular" : true,
  "views" : 1475
}, {
  "spanish" : "soplar",
  "translations" : {
    "ENGLISH" : [ "blow" ]
  },
  "regular" : true,
  "views" : 1475
}, {
  "spanish" : "adoptar",
  "translations" : {
    "ENGLISH" : [ "adopt" ]
  },
  "regular" : true,
  "views" : 1475
}, {
  "spanish" : "arrestar",
  "translations" : {
    "ENGLISH" : [ "arrest" ]
  },
  "regular" : true,
  "views" : 1475
}, {
  "spanish" : "suplicar",
  "translations" : {
    "ENGLISH" : [ "plead" ]
  },
  "regular" : false,
  "views" : 1474
}, {
  "spanish" : "apuntar",
  "translations" : {
    "ENGLISH" : [ "note down" ]
  },
  "regular" : true,
  "views" : 1474
}, {
  "spanish" : "recuperar",
  "translations" : {
    "ENGLISH" : [ "recover" ]
  },
  "regular" : true,
  "views" : 1474
}, {
  "spanish" : "trepar",
  "translations" : {
    "ENGLISH" : [ "climb" ]
  },
  "regular" : true,
  "views" : 1474
}, {
  "spanish" : "repasar",
  "translations" : {
    "ENGLISH" : [ "revise" ]
  },
  "regular" : true,
  "views" : 1473
}, {
  "spanish" : "arrendar",
  "translations" : {
    "ENGLISH" : [ "rent out" ]
  },
  "regular" : false,
  "views" : 1472
}, {
  "spanish" : "aclarar",
  "translations" : {
    "ENGLISH" : [ "clarify" ]
  },
  "regular" : true,
  "views" : 1472
}, {
  "spanish" : "suspender",
  "translations" : {
    "ENGLISH" : [ "fail" ]
  },
  "regular" : true,
  "views" : 1472
}, {
  "spanish" : "abreviar",
  "translations" : {
    "ENGLISH" : [ "abbreviate" ]
  },
  "regular" : true,
  "views" : 1471
}, {
  "spanish" : "rodear",
  "translations" : {
    "ENGLISH" : [ "surround" ]
  },
  "regular" : true,
  "views" : 1471
}, {
  "spanish" : "rehuir",
  "translations" : {
    "ENGLISH" : [ "avoid" ]
  },
  "regular" : false,
  "views" : 1470
}, {
  "spanish" : "asentir",
  "translations" : {
    "ENGLISH" : [ "agree", "consent" ]
  },
  "regular" : false,
  "views" : 1470
}, {
  "spanish" : "anular",
  "translations" : {
    "ENGLISH" : [ "cancel" ]
  },
  "regular" : true,
  "views" : 1469
}, {
  "spanish" : "involucrar",
  "translations" : {
    "ENGLISH" : [ "involve" ]
  },
  "regular" : true,
  "views" : 1469
}, {
  "spanish" : "aislar",
  "translations" : {
    "ENGLISH" : [ "isolate" ]
  },
  "regular" : false,
  "views" : 1468
}, {
  "spanish" : "arriesgar",
  "translations" : {
    "ENGLISH" : [ "risk" ]
  },
  "regular" : false,
  "views" : 1468
}, {
  "spanish" : "pudrir",
  "translations" : {
    "ENGLISH" : [ "rot" ]
  },
  "regular" : false,
  "views" : 1467
}, {
  "spanish" : "brillar",
  "translations" : {
    "ENGLISH" : [ "shine" ]
  },
  "regular" : true,
  "views" : 1467
}, {
  "spanish" : "tronar",
  "translations" : {
    "ENGLISH" : [ "thunder" ]
  },
  "regular" : false,
  "views" : 1466
}, {
  "spanish" : "acelerar",
  "translations" : {
    "ENGLISH" : [ "accelerate" ]
  },
  "regular" : true,
  "views" : 1466
}, {
  "spanish" : "clasificar",
  "translations" : {
    "ENGLISH" : [ "clasify" ]
  },
  "regular" : false,
  "views" : 1466
}, {
  "spanish" : "pulir",
  "translations" : {
    "ENGLISH" : [ "polish" ]
  },
  "regular" : true,
  "views" : 1466
}, {
  "spanish" : "revivir",
  "translations" : {
    "ENGLISH" : [ "relive", "revive" ]
  },
  "regular" : true,
  "views" : 1466
}, {
  "spanish" : "arruinar",
  "translations" : {
    "ENGLISH" : [ "ruin" ]
  },
  "regular" : true,
  "views" : 1465
}, {
  "spanish" : "evaluar",
  "translations" : {
    "ENGLISH" : [ "assess" ]
  },
  "regular" : false,
  "views" : 1465
}, {
  "spanish" : "atrapar",
  "translations" : {
    "ENGLISH" : [ "trap" ]
  },
  "regular" : true,
  "views" : 1465
}, {
  "spanish" : "aprehender",
  "translations" : {
    "ENGLISH" : [ "apprehend" ]
  },
  "regular" : true,
  "views" : 1464
}, {
  "spanish" : "bromear",
  "translations" : {
    "ENGLISH" : [ "joke" ]
  },
  "regular" : true,
  "views" : 1463
}, {
  "spanish" : "situar",
  "translations" : {
    "ENGLISH" : [ "place" ]
  },
  "regular" : true,
  "views" : 1462
}, {
  "spanish" : "sofocar",
  "translations" : {
    "ENGLISH" : [ "smother", "put out" ]
  },
  "regular" : false,
  "views" : 1462
}, {
  "spanish" : "provocar",
  "translations" : {
    "ENGLISH" : [ "incite" ]
  },
  "regular" : false,
  "views" : 1460
}, {
  "spanish" : "recubrir",
  "translations" : {
    "ENGLISH" : [ "cover" ]
  },
  "regular" : false,
  "views" : 1459
}, {
  "spanish" : "instalar",
  "translations" : {
    "ENGLISH" : [ "install" ]
  },
  "regular" : true,
  "views" : 1458
}, {
  "spanish" : "arder",
  "translations" : {
    "ENGLISH" : [ "burn" ]
  },
  "regular" : true,
  "views" : 1457
}, {
  "spanish" : "adscribir",
  "translations" : {
    "ENGLISH" : [ "assign" ]
  },
  "regular" : false,
  "views" : 1456
}, {
  "spanish" : "ensayar",
  "translations" : {
    "ENGLISH" : [ "practise" ]
  },
  "regular" : true,
  "views" : 1456
}, {
  "spanish" : "localizar",
  "translations" : {
    "ENGLISH" : [ "locate" ]
  },
  "regular" : false,
  "views" : 1456
}, {
  "spanish" : "batir",
  "translations" : {
    "ENGLISH" : [ "beat", "churn", "whisk", "whip" ]
  },
  "regular" : true,
  "views" : 1455
}, {
  "spanish" : "adelantar",
  "translations" : {
    "ENGLISH" : [ "move forward", "overtake" ]
  },
  "regular" : true,
  "views" : 1453
}, {
  "spanish" : "circular",
  "translations" : {
    "ENGLISH" : [ "run" ]
  },
  "regular" : true,
  "views" : 1452
}, {
  "spanish" : "sobrar",
  "translations" : {
    "ENGLISH" : [ "be too much" ]
  },
  "regular" : true,
  "views" : 1452
}, {
  "spanish" : "colar",
  "translations" : {
    "ENGLISH" : [ "strain", "sneak in" ]
  },
  "regular" : false,
  "views" : 1451
}, {
  "spanish" : "agravar",
  "translations" : {
    "ENGLISH" : [ "make worse" ]
  },
  "regular" : true,
  "views" : 1451
}, {
  "spanish" : "escupir",
  "translations" : {
    "ENGLISH" : [ "spit" ]
  },
  "regular" : true,
  "views" : 1451
}, {
  "spanish" : "mimar",
  "translations" : {
    "ENGLISH" : [ "pamper", "spoil" ]
  },
  "regular" : true,
  "views" : 1451
}, {
  "spanish" : "presuponer",
  "translations" : {
    "ENGLISH" : [ "assume" ]
  },
  "regular" : false,
  "views" : 1450
}, {
  "spanish" : "remover",
  "translations" : {
    "ENGLISH" : [ "stir" ]
  },
  "regular" : false,
  "views" : 1450
}, {
  "spanish" : "roncar",
  "translations" : {
    "ENGLISH" : [ "snore" ]
  },
  "regular" : false,
  "views" : 1450
}, {
  "spanish" : "aclamar",
  "translations" : {
    "ENGLISH" : [ "acclaim", "applaud" ]
  },
  "regular" : true,
  "views" : 1449
}, {
  "spanish" : "coludir",
  "translations" : {
    "ENGLISH" : [ "collude" ]
  },
  "regular" : true,
  "views" : 1449
}, {
  "spanish" : "brindar",
  "translations" : {
    "ENGLISH" : [ "drink a toast to" ]
  },
  "regular" : true,
  "views" : 1448
}, {
  "spanish" : "protestar",
  "translations" : {
    "ENGLISH" : [ "complain" ]
  },
  "regular" : true,
  "views" : 1448
}, {
  "spanish" : "afirmar",
  "translations" : {
    "ENGLISH" : [ "affirm" ]
  },
  "regular" : true,
  "views" : 1448
}, {
  "spanish" : "coincidir",
  "translations" : {
    "ENGLISH" : [ "coincide" ]
  },
  "regular" : true,
  "views" : 1448
}, {
  "spanish" : "entrevistar",
  "translations" : {
    "ENGLISH" : [ "interview" ]
  },
  "regular" : true,
  "views" : 1447
}, {
  "spanish" : "expulsar",
  "translations" : {
    "ENGLISH" : [ "expel" ]
  },
  "regular" : false,
  "views" : 1446
}, {
  "spanish" : "transformar",
  "translations" : {
    "ENGLISH" : [ "transform" ]
  },
  "regular" : true,
  "views" : 1446
}, {
  "spanish" : "insultar",
  "translations" : {
    "ENGLISH" : [ "insult" ]
  },
  "regular" : true,
  "views" : 1445
}, {
  "spanish" : "lloviznar",
  "translations" : {
    "ENGLISH" : [ "drizzle" ]
  },
  "regular" : true,
  "views" : 1444
}, {
  "spanish" : "coquetear",
  "translations" : {
    "ENGLISH" : [ "flirt" ]
  },
  "regular" : true,
  "views" : 1444
}, {
  "spanish" : "aliviar",
  "translations" : {
    "ENGLISH" : [ "unburden", "lighten", "relieve", "alleviate" ]
  },
  "regular" : true,
  "views" : 1442
}, {
  "spanish" : "changar",
  "translations" : {
    "ENGLISH" : [ "break down" ]
  },
  "regular" : false,
  "views" : 1442
}, {
  "spanish" : "acceder",
  "translations" : {
    "ENGLISH" : [ "access", "agree" ]
  },
  "regular" : true,
  "views" : 1442
}, {
  "spanish" : "empatar",
  "translations" : {
    "ENGLISH" : [ "equalize", "draw" ]
  },
  "regular" : true,
  "views" : 1441
}, {
  "spanish" : "apartar",
  "translations" : {
    "ENGLISH" : [ "separate", "part", "divide" ]
  },
  "regular" : true,
  "views" : 1441
}, {
  "spanish" : "agachar",
  "translations" : {
    "ENGLISH" : [ "lower" ]
  },
  "regular" : true,
  "views" : 1440
}, {
  "spanish" : "burlar",
  "translations" : {
    "ENGLISH" : [ "make fun of" ]
  },
  "regular" : true,
  "views" : 1440
}, {
  "spanish" : "anteponer",
  "translations" : {
    "ENGLISH" : [ "to put before" ]
  },
  "regular" : false,
  "views" : 1439
}, {
  "spanish" : "arrojar",
  "translations" : {
    "ENGLISH" : [ "throw", "hurl", "fling" ]
  },
  "regular" : true,
  "views" : 1439
}, {
  "spanish" : "esparcir",
  "translations" : {
    "ENGLISH" : [ "scatter" ]
  },
  "regular" : false,
  "views" : 1438
}, {
  "spanish" : "envejecer",
  "translations" : {
    "ENGLISH" : [ "get old" ]
  },
  "regular" : false,
  "views" : 1438
}, {
  "spanish" : "rellenar",
  "translations" : {
    "ENGLISH" : [ "fill" ]
  },
  "regular" : true,
  "views" : 1438
}, {
  "spanish" : "amordazar",
  "translations" : {
    "ENGLISH" : [ "muzzle", "gag" ]
  },
  "regular" : false,
  "views" : 1437
}, {
  "spanish" : "derrotar",
  "translations" : {
    "ENGLISH" : [ "beat" ]
  },
  "regular" : true,
  "views" : 1437
}, {
  "spanish" : "extraviar",
  "translations" : {
    "ENGLISH" : [ "go missing", "to get lost" ]
  },
  "regular" : false,
  "views" : 1436
}, {
  "spanish" : "tranquilizar",
  "translations" : {
    "ENGLISH" : [ "calm somebody down" ]
  },
  "regular" : false,
  "views" : 1436
}, {
  "spanish" : "invadir",
  "translations" : {
    "ENGLISH" : [ "invade" ]
  },
  "regular" : true,
  "views" : 1436
}, {
  "spanish" : "convocar",
  "translations" : {
    "ENGLISH" : [ "convoke" ]
  },
  "regular" : false,
  "views" : 1436
}, {
  "spanish" : "volcar",
  "translations" : {
    "ENGLISH" : [ "knock over", "spill" ]
  },
  "regular" : false,
  "views" : 1436
}, {
  "spanish" : "descomponer",
  "translations" : {
    "ENGLISH" : [ "decompose" ]
  },
  "regular" : false,
  "views" : 1435
}, {
  "spanish" : "asombrar",
  "translations" : {
    "ENGLISH" : [ "be amazed" ]
  },
  "regular" : true,
  "views" : 1435
}, {
  "spanish" : "plantar",
  "translations" : {
    "ENGLISH" : [ "plant" ]
  },
  "regular" : true,
  "views" : 1435
}, {
  "spanish" : "soportar",
  "translations" : {
    "ENGLISH" : [ "stand" ]
  },
  "regular" : true,
  "views" : 1435
}, {
  "spanish" : "contabilizar",
  "translations" : {
    "ENGLISH" : [ "enter in the accounts" ]
  },
  "regular" : false,
  "views" : 1435
}, {
  "spanish" : "podar",
  "translations" : {
    "ENGLISH" : [ "prune" ]
  },
  "regular" : true,
  "views" : 1435
}, {
  "spanish" : "fotografiar",
  "translations" : {
    "ENGLISH" : [ "take a photograph of" ]
  },
  "regular" : false,
  "views" : 1434
}, {
  "spanish" : "alabar",
  "translations" : {
    "ENGLISH" : [ "praise" ]
  },
  "regular" : true,
  "views" : 1434
}, {
  "spanish" : "experimentar",
  "translations" : {
    "ENGLISH" : [ "experiment", "experience" ]
  },
  "regular" : true,
  "views" : 1434
}, {
  "spanish" : "estrechar",
  "translations" : {
    "ENGLISH" : [ "narrow" ]
  },
  "regular" : true,
  "views" : 1433
}, {
  "spanish" : "comportar",
  "translations" : {
    "ENGLISH" : [ "behave" ]
  },
  "regular" : true,
  "views" : 1433
}, {
  "spanish" : "conquistar",
  "translations" : {
    "ENGLISH" : [ "conquer" ]
  },
  "regular" : true,
  "views" : 1433
}, {
  "spanish" : "desnudar",
  "translations" : {
    "ENGLISH" : [ "undress" ]
  },
  "regular" : true,
  "views" : 1432
}, {
  "spanish" : "ensuciar",
  "translations" : {
    "ENGLISH" : [ "get dirty" ]
  },
  "regular" : true,
  "views" : 1431
}, {
  "spanish" : "conceder",
  "translations" : {
    "ENGLISH" : [ "concede", "grant", "bestow" ]
  },
  "regular" : true,
  "views" : 1431
}, {
  "spanish" : "clarificar",
  "translations" : {
    "ENGLISH" : [ "clarify" ]
  },
  "regular" : false,
  "views" : 1430
}, {
  "spanish" : "fraguar",
  "translations" : {
    "ENGLISH" : [ "forge" ]
  },
  "regular" : false,
  "views" : 1429
}, {
  "spanish" : "archivar",
  "translations" : {
    "ENGLISH" : [ "file" ]
  },
  "regular" : true,
  "views" : 1429
}, {
  "spanish" : "arrugar",
  "translations" : {
    "ENGLISH" : [ "wrinkle" ]
  },
  "regular" : false,
  "views" : 1429
}, {
  "spanish" : "aunar",
  "translations" : {
    "ENGLISH" : [ "unite", "join forces" ]
  },
  "regular" : false,
  "views" : 1429
}, {
  "spanish" : "derrocar",
  "translations" : {
    "ENGLISH" : [ "topple", "overthrow" ]
  },
  "regular" : false,
  "views" : 1429
}, {
  "spanish" : "reproducir",
  "translations" : {
    "ENGLISH" : [ "reproduce" ]
  },
  "regular" : false,
  "views" : 1428
}, {
  "spanish" : "consultar",
  "translations" : {
    "ENGLISH" : [ "consult" ]
  },
  "regular" : true,
  "views" : 1428
}, {
  "spanish" : "fracasar",
  "translations" : {
    "ENGLISH" : [ "fail" ]
  },
  "regular" : true,
  "views" : 1427
}, {
  "spanish" : "trasladar",
  "translations" : {
    "ENGLISH" : [ "move" ]
  },
  "regular" : true,
  "views" : 1427
}, {
  "spanish" : "ansiar",
  "translations" : {
    "ENGLISH" : [ "long for", "yearn for" ]
  },
  "regular" : false,
  "views" : 1426
}, {
  "spanish" : "civilizar",
  "translations" : {
    "ENGLISH" : [ "civilize" ]
  },
  "regular" : false,
  "views" : 1425
}, {
  "spanish" : "encubrir",
  "translations" : {
    "ENGLISH" : [ "cover" ]
  },
  "regular" : false,
  "views" : 1424
}, {
  "spanish" : "agrupar",
  "translations" : {
    "ENGLISH" : [ "form groups", "group" ]
  },
  "regular" : true,
  "views" : 1424
}, {
  "spanish" : "alarmar",
  "translations" : {
    "ENGLISH" : [ "alarm" ]
  },
  "regular" : true,
  "views" : 1424
}, {
  "spanish" : "apurar",
  "translations" : {
    "ENGLISH" : [ "worry", "fret", "grieve" ]
  },
  "regular" : true,
  "views" : 1424
}, {
  "spanish" : "antedecir",
  "translations" : {
    "ENGLISH" : [ "to say before" ]
  },
  "regular" : false,
  "views" : 1423
}, {
  "spanish" : "alternar",
  "translations" : {
    "ENGLISH" : [ "socialize", "alternate" ]
  },
  "regular" : true,
  "views" : 1423
}, {
  "spanish" : "progresar",
  "translations" : {
    "ENGLISH" : [ "progress" ]
  },
  "regular" : true,
  "views" : 1423
}, {
  "spanish" : "apaciguar",
  "translations" : {
    "ENGLISH" : [ "calm down" ]
  },
  "regular" : false,
  "views" : 1422
}, {
  "spanish" : "frenar",
  "translations" : {
    "ENGLISH" : [ "brake" ]
  },
  "regular" : true,
  "views" : 1422
}, {
  "spanish" : "transmitir",
  "translations" : {
    "ENGLISH" : [ "broadcast", "transmit" ]
  },
  "regular" : true,
  "views" : 1422
}, {
  "spanish" : "chantajear",
  "translations" : {
    "ENGLISH" : [ "blackmail" ]
  },
  "regular" : true,
  "views" : 1421
}, {
  "spanish" : "resbalar",
  "translations" : {
    "ENGLISH" : [ "slip" ]
  },
  "regular" : true,
  "views" : 1421
}, {
  "spanish" : "serrar",
  "translations" : {
    "ENGLISH" : [ "saw" ]
  },
  "regular" : false,
  "views" : 1420
}, {
  "spanish" : "solucionar",
  "translations" : {
    "ENGLISH" : [ "solve" ]
  },
  "regular" : true,
  "views" : 1420
}, {
  "spanish" : "chillar",
  "translations" : {
    "ENGLISH" : [ "shout" ]
  },
  "regular" : true,
  "views" : 1419
}, {
  "spanish" : "proceder",
  "translations" : {
    "ENGLISH" : [ "proceed" ]
  },
  "regular" : true,
  "views" : 1419
}, {
  "spanish" : "excitar",
  "translations" : {
    "ENGLISH" : [ "excite" ]
  },
  "regular" : true,
  "views" : 1419
}, {
  "spanish" : "aplastar",
  "translations" : {
    "ENGLISH" : [ "flatten", "crush" ]
  },
  "regular" : true,
  "views" : 1419
}, {
  "spanish" : "suprimir",
  "translations" : {
    "ENGLISH" : [ "leave something out" ]
  },
  "regular" : true,
  "views" : 1418
}, {
  "spanish" : "fruncir",
  "translations" : {
    "ENGLISH" : [ "frown" ]
  },
  "regular" : false,
  "views" : 1417
}, {
  "spanish" : "ahorcar",
  "translations" : {
    "ENGLISH" : [ "hang" ]
  },
  "regular" : false,
  "views" : 1417
}, {
  "spanish" : "digerir",
  "translations" : {
    "ENGLISH" : [ "digest", "assimilate" ]
  },
  "regular" : false,
  "views" : 1415
}, {
  "spanish" : "concordar",
  "translations" : {
    "ENGLISH" : [ "reconcile", "agree" ]
  },
  "regular" : false,
  "views" : 1414
}, {
  "spanish" : "cojear",
  "translations" : {
    "ENGLISH" : [ "limp" ]
  },
  "regular" : true,
  "views" : 1414
}, {
  "spanish" : "insinuar",
  "translations" : {
    "ENGLISH" : [ "insinuate" ]
  },
  "regular" : false,
  "views" : 1413
}, {
  "spanish" : "propolar",
  "translations" : {
    "ENGLISH" : [ "divulge" ]
  },
  "regular" : true,
  "views" : 1412
}, {
  "spanish" : "adaptar",
  "translations" : {
    "ENGLISH" : [ "adapt" ]
  },
  "regular" : true,
  "views" : 1410
}, {
  "spanish" : "devenir",
  "translations" : {
    "ENGLISH" : [ "turn into", "become" ]
  },
  "regular" : false,
  "views" : 1409
}, {
  "spanish" : "anotar",
  "translations" : {
    "ENGLISH" : [ "take down", "make a note of" ]
  },
  "regular" : true,
  "views" : 1409
}, {
  "spanish" : "persuadir",
  "translations" : {
    "ENGLISH" : [ "persuade" ]
  },
  "regular" : true,
  "views" : 1409
}, {
  "spanish" : "persistir",
  "translations" : {
    "ENGLISH" : [ "persist" ]
  },
  "regular" : true,
  "views" : 1409
}, {
  "spanish" : "suscribir",
  "translations" : {
    "ENGLISH" : [ "subscribe" ]
  },
  "regular" : false,
  "views" : 1408
}, {
  "spanish" : "enchufar",
  "translations" : {
    "ENGLISH" : [ "plug" ]
  },
  "regular" : true,
  "views" : 1408
}, {
  "spanish" : "exportar",
  "translations" : {
    "ENGLISH" : [ "export" ]
  },
  "regular" : true,
  "views" : 1408
}, {
  "spanish" : "matricular",
  "translations" : {
    "ENGLISH" : [ "enrol" ]
  },
  "regular" : true,
  "views" : 1408
}, {
  "spanish" : "acumular",
  "translations" : {
    "ENGLISH" : [ "accumulate" ]
  },
  "regular" : true,
  "views" : 1408
}, {
  "spanish" : "sobrevolar",
  "translations" : {
    "ENGLISH" : [ "overfly", "fly over" ]
  },
  "regular" : false,
  "views" : 1408
}, {
  "spanish" : "programar",
  "translations" : {
    "ENGLISH" : [ "plan" ]
  },
  "regular" : true,
  "views" : 1406
}, {
  "spanish" : "cegar",
  "translations" : {
    "ENGLISH" : [ "blind", "dazzle" ]
  },
  "regular" : false,
  "views" : 1405
}, {
  "spanish" : "confluir",
  "translations" : {
    "ENGLISH" : [ "converge", "meet" ]
  },
  "regular" : false,
  "views" : 1404
}, {
  "spanish" : "cotizar",
  "translations" : {
    "ENGLISH" : [ "quote", "price" ]
  },
  "regular" : false,
  "views" : 1404
}, {
  "spanish" : "disparar",
  "translations" : {
    "ENGLISH" : [ "shoot" ]
  },
  "regular" : true,
  "views" : 1404
}, {
  "spanish" : "triunfar",
  "translations" : {
    "ENGLISH" : [ "succeed", "win" ]
  },
  "regular" : true,
  "views" : 1404
}, {
  "spanish" : "vomitar",
  "translations" : {
    "ENGLISH" : [ "vomit" ]
  },
  "regular" : true,
  "views" : 1404
}, {
  "spanish" : "presidir",
  "translations" : {
    "ENGLISH" : [ "preside" ]
  },
  "regular" : true,
  "views" : 1404
}, {
  "spanish" : "complicar",
  "translations" : {
    "ENGLISH" : [ "complicate" ]
  },
  "regular" : false,
  "views" : 1403
}, {
  "spanish" : "aguardar",
  "translations" : {
    "ENGLISH" : [ "expect", "wait for" ]
  },
  "regular" : true,
  "views" : 1403
}, {
  "spanish" : "diluir",
  "translations" : {
    "ENGLISH" : [ "dilute" ]
  },
  "regular" : false,
  "views" : 1402
}, {
  "spanish" : "disfrazar",
  "translations" : {
    "ENGLISH" : [ "dress up" ]
  },
  "regular" : false,
  "views" : 1402
}, {
  "spanish" : "detestar",
  "translations" : {
    "ENGLISH" : [ "detest" ]
  },
  "regular" : true,
  "views" : 1402
}, {
  "spanish" : "expedir",
  "translations" : {
    "ENGLISH" : [ "dispatch", "issue", "send" ]
  },
  "regular" : false,
  "views" : 1401
}, {
  "spanish" : "renovar",
  "translations" : {
    "ENGLISH" : [ "renovate" ]
  },
  "regular" : false,
  "views" : 1401
}, {
  "spanish" : "serpentear",
  "translations" : {
    "ENGLISH" : [ "meander", "snake", "wind" ]
  },
  "regular" : true,
  "views" : 1401
}, {
  "spanish" : "culturizar",
  "translations" : {
    "ENGLISH" : [ "educate" ]
  },
  "regular" : false,
  "views" : 1401
}, {
  "spanish" : "saltear",
  "translations" : {
    "ENGLISH" : [ "saute", "fry lightly" ]
  },
  "regular" : true,
  "views" : 1401
}, {
  "spanish" : "arrastrar",
  "translations" : {
    "ENGLISH" : [ "drag" ]
  },
  "regular" : true,
  "views" : 1400
}, {
  "spanish" : "percibir",
  "translations" : {
    "ENGLISH" : [ "perceive" ]
  },
  "regular" : true,
  "views" : 1400
}, {
  "spanish" : "fortalecer",
  "translations" : {
    "ENGLISH" : [ "strengthen" ]
  },
  "regular" : false,
  "views" : 1400
}, {
  "spanish" : "desvestir",
  "translations" : {
    "ENGLISH" : [ "undress" ]
  },
  "regular" : false,
  "views" : 1399
}, {
  "spanish" : "desviar",
  "translations" : {
    "ENGLISH" : [ "divert" ]
  },
  "regular" : false,
  "views" : 1399
}, {
  "spanish" : "exclamar",
  "translations" : {
    "ENGLISH" : [ "exclaim" ]
  },
  "regular" : true,
  "views" : 1399
}, {
  "spanish" : "derivar",
  "translations" : {
    "ENGLISH" : [ "derive" ]
  },
  "regular" : true,
  "views" : 1399
}, {
  "spanish" : "reponer",
  "translations" : {
    "ENGLISH" : [ "replace" ]
  },
  "regular" : false,
  "views" : 1398
}, {
  "spanish" : "expresar",
  "translations" : {
    "ENGLISH" : [ "express" ]
  },
  "regular" : true,
  "views" : 1397
}, {
  "spanish" : "transbordar",
  "translations" : {
    "ENGLISH" : [ "change trains" ]
  },
  "regular" : true,
  "views" : 1397
}, {
  "spanish" : "disputar",
  "translations" : {
    "ENGLISH" : [ "dispute" ]
  },
  "regular" : true,
  "views" : 1396
}, {
  "spanish" : "aflojar",
  "translations" : {
    "ENGLISH" : [ "loosen" ]
  },
  "regular" : true,
  "views" : 1395
}, {
  "spanish" : "resistir",
  "translations" : {
    "ENGLISH" : [ "withstand" ]
  },
  "regular" : true,
  "views" : 1395
}, {
  "spanish" : "confirmar",
  "translations" : {
    "ENGLISH" : [ "confirm" ]
  },
  "regular" : true,
  "views" : 1395
}, {
  "spanish" : "disgustar",
  "translations" : {
    "ENGLISH" : [ "disgust" ],
    "FRENCH" : [ "dégoûter" ]
  },
  "regular" : true,
  "views" : 11394
}, {
  "spanish" : "atestiguar",
  "translations" : {
    "ENGLISH" : [ "testify to" ]
  },
  "regular" : false,
  "views" : 1393
}, {
  "spanish" : "rozar",
  "translations" : {
    "ENGLISH" : [ "brush", "graze", "rub" ]
  },
  "regular" : false,
  "views" : 1393
}, {
  "spanish" : "agrandar",
  "translations" : {
    "ENGLISH" : [ "enlarge" ]
  },
  "regular" : true,
  "views" : 1392
}, {
  "spanish" : "empaquetar",
  "translations" : {
    "ENGLISH" : [ "package" ]
  },
  "regular" : true,
  "views" : 1392
}, {
  "spanish" : "vigilar",
  "translations" : {
    "ENGLISH" : [ "keep an eye on something or somebody" ]
  },
  "regular" : true,
  "views" : 1392
}, {
  "spanish" : "auditar",
  "translations" : {
    "ENGLISH" : [ "audit" ]
  },
  "regular" : true,
  "views" : 1392
}, {
  "spanish" : "perpetuar",
  "translations" : {
    "ENGLISH" : [ "perpetuate" ]
  },
  "regular" : false,
  "views" : 1391
}, {
  "spanish" : "retorcer",
  "translations" : {
    "ENGLISH" : [ "twist" ]
  },
  "regular" : false,
  "views" : 1391
}, {
  "spanish" : "desobedecer",
  "translations" : {
    "ENGLISH" : [ "disobey" ]
  },
  "regular" : false,
  "views" : 1391
}, {
  "spanish" : "sumar",
  "translations" : {
    "ENGLISH" : [ "add" ]
  },
  "regular" : true,
  "views" : 1391
}, {
  "spanish" : "disolver",
  "translations" : {
    "ENGLISH" : [ "breakup", "dissolve" ]
  },
  "regular" : false,
  "views" : 1389
}, {
  "spanish" : "emocionar",
  "translations" : {
    "ENGLISH" : [ "be moved by" ]
  },
  "regular" : true,
  "views" : 1389
}, {
  "spanish" : "estropear",
  "translations" : {
    "ENGLISH" : [ "break", "spoil" ]
  },
  "regular" : true,
  "views" : 1389
}, {
  "spanish" : "accionar",
  "translations" : {
    "ENGLISH" : [ "operate", "activate" ]
  },
  "regular" : true,
  "views" : 1389
}, {
  "spanish" : "desordenar",
  "translations" : {
    "ENGLISH" : [ "make untidy", "mess up" ]
  },
  "regular" : true,
  "views" : 1388
}, {
  "spanish" : "cooperar",
  "translations" : {
    "ENGLISH" : [ "cooperate" ]
  },
  "regular" : true,
  "views" : 1388
}, {
  "spanish" : "modificar",
  "translations" : {
    "ENGLISH" : [ "modify" ]
  },
  "regular" : false,
  "views" : 1388
}, {
  "spanish" : "agujerear",
  "translations" : {
    "ENGLISH" : [ "puncture", "perforate", "pierce", "bore" ]
  },
  "regular" : true,
  "views" : 1386
}, {
  "spanish" : "excusar",
  "translations" : {
    "ENGLISH" : [ "excuse" ]
  },
  "regular" : true,
  "views" : 1386
}, {
  "spanish" : "extraer",
  "translations" : {
    "ENGLISH" : [ "extract" ]
  },
  "regular" : false,
  "views" : 1385
}, {
  "spanish" : "pinchar",
  "translations" : {
    "ENGLISH" : [ "pick" ]
  },
  "regular" : true,
  "views" : 1385
}, {
  "spanish" : "clavar",
  "translations" : {
    "ENGLISH" : [ "hammer", "nail" ]
  },
  "regular" : true,
  "views" : 1384
}, {
  "spanish" : "administrar",
  "translations" : {
    "ENGLISH" : [ "administer" ]
  },
  "regular" : true,
  "views" : 1384
}, {
  "spanish" : "centrifugar",
  "translations" : {
    "ENGLISH" : [ "spin-dry", "centrifuge", "spin" ]
  },
  "regular" : false,
  "views" : 1383
}, {
  "spanish" : "amueblar",
  "translations" : {
    "ENGLISH" : [ "furnish" ]
  },
  "regular" : true,
  "views" : 1382
}, {
  "spanish" : "esculpir",
  "translations" : {
    "ENGLISH" : [ "sculpt" ]
  },
  "regular" : true,
  "views" : 1382
}, {
  "spanish" : "contemplar",
  "translations" : {
    "ENGLISH" : [ "contemplate" ]
  },
  "regular" : true,
  "views" : 1381
}, {
  "spanish" : "exhibir",
  "translations" : {
    "ENGLISH" : [ "exhibit" ]
  },
  "regular" : true,
  "views" : 1381
}, {
  "spanish" : "transcurrir",
  "translations" : {
    "ENGLISH" : [ "pass time" ]
  },
  "regular" : true,
  "views" : 1380
}, {
  "spanish" : "extenuar",
  "translations" : {
    "ENGLISH" : [ "exhaust", "drain" ]
  },
  "regular" : false,
  "views" : 1379
}, {
  "spanish" : "alargar",
  "translations" : {
    "ENGLISH" : [ "extend" ]
  },
  "regular" : false,
  "views" : 1378
}, {
  "spanish" : "asociar",
  "translations" : {
    "ENGLISH" : [ "associate" ]
  },
  "regular" : true,
  "views" : 1378
}, {
  "spanish" : "aficionar",
  "translations" : {
    "ENGLISH" : [ "be keen on" ]
  },
  "regular" : true,
  "views" : 1377
}, {
  "spanish" : "amarrar",
  "translations" : {
    "ENGLISH" : [ "tie", "moor" ]
  },
  "regular" : true,
  "views" : 1377
}, {
  "spanish" : "resumir",
  "translations" : {
    "ENGLISH" : [ "summarize" ]
  },
  "regular" : true,
  "views" : 1377
}, {
  "spanish" : "tramitar",
  "translations" : {
    "ENGLISH" : [ "process" ]
  },
  "regular" : true,
  "views" : 1377
}, {
  "spanish" : "compeler",
  "translations" : {
    "ENGLISH" : [ "compel" ]
  },
  "regular" : true,
  "views" : 1376
}, {
  "spanish" : "despreciar",
  "translations" : {
    "ENGLISH" : [ "despise" ]
  },
  "regular" : true,
  "views" : 1376
}, {
  "spanish" : "achicharrar",
  "translations" : {
    "ENGLISH" : [ "burn", "frazzle" ]
  },
  "regular" : true,
  "views" : 1375
}, {
  "spanish" : "asemejar",
  "translations" : {
    "ENGLISH" : [ "be alike" ]
  },
  "regular" : true,
  "views" : 1374
}, {
  "spanish" : "trastear",
  "translations" : {
    "ENGLISH" : [ "rummage through" ]
  },
  "regular" : true,
  "views" : 1374
}, {
  "spanish" : "etiquetar",
  "translations" : {
    "ENGLISH" : [ "label" ]
  },
  "regular" : true,
  "views" : 1373
}, {
  "spanish" : "apilar",
  "translations" : {
    "ENGLISH" : [ "pile up" ]
  },
  "regular" : true,
  "views" : 1373
}, {
  "spanish" : "concentrar",
  "translations" : {
    "ENGLISH" : [ "concentrate" ]
  },
  "regular" : true,
  "views" : 1373
}, {
  "spanish" : "reventar",
  "translations" : {
    "ENGLISH" : [ "burst" ]
  },
  "regular" : false,
  "views" : 1371
}, {
  "spanish" : "amontonar",
  "translations" : {
    "ENGLISH" : [ "pile something up", "amass" ]
  },
  "regular" : true,
  "views" : 1371
}, {
  "spanish" : "estrenar",
  "translations" : {
    "ENGLISH" : [ "do for the first time" ]
  },
  "regular" : true,
  "views" : 1371
}, {
  "spanish" : "censar",
  "translations" : {
    "ENGLISH" : [ "take a census of" ]
  },
  "regular" : true,
  "views" : 1371
}, {
  "spanish" : "circunscribir",
  "translations" : {
    "ENGLISH" : [ "circumscribe" ]
  },
  "regular" : false,
  "views" : 1370
}, {
  "spanish" : "aparentar",
  "translations" : {
    "ENGLISH" : [ "pretend" ]
  },
  "regular" : true,
  "views" : 1370
}, {
  "spanish" : "relacionar",
  "translations" : {
    "ENGLISH" : [ "relate" ]
  },
  "regular" : true,
  "views" : 1370
}, {
  "spanish" : "abundar",
  "translations" : {
    "ENGLISH" : [ "abound" ]
  },
  "regular" : true,
  "views" : 1370
}, {
  "spanish" : "susurrar",
  "translations" : {
    "ENGLISH" : [ "whisper" ]
  },
  "regular" : true,
  "views" : 1369
}, {
  "spanish" : "arropar",
  "translations" : {
    "ENGLISH" : [ "cover" ]
  },
  "regular" : true,
  "views" : 1369
}, {
  "spanish" : "pervertir",
  "translations" : {
    "ENGLISH" : [ "corrupt" ]
  },
  "regular" : false,
  "views" : 1368
}, {
  "spanish" : "prescribir",
  "translations" : {
    "ENGLISH" : [ "prescribe" ]
  },
  "regular" : false,
  "views" : 1368
}, {
  "spanish" : "sesgar",
  "translations" : {
    "ENGLISH" : [ "bias", "slant" ]
  },
  "regular" : false,
  "views" : 1368
}, {
  "spanish" : "multiplicar",
  "translations" : {
    "ENGLISH" : [ "multiply" ]
  },
  "regular" : false,
  "views" : 1367
}, {
  "spanish" : "formalizar",
  "translations" : {
    "ENGLISH" : [ "formalize" ]
  },
  "regular" : false,
  "views" : 1367
}, {
  "spanish" : "dictar",
  "translations" : {
    "ENGLISH" : [ "dictate" ]
  },
  "regular" : true,
  "views" : 1365
}, {
  "spanish" : "infectar",
  "translations" : {
    "ENGLISH" : [ "infect" ]
  },
  "regular" : true,
  "views" : 1365
}, {
  "spanish" : "castrar",
  "translations" : {
    "ENGLISH" : [ "castrate" ]
  },
  "regular" : true,
  "views" : 1365
}, {
  "spanish" : "destrozar",
  "translations" : {
    "ENGLISH" : [ "destroy" ]
  },
  "regular" : false,
  "views" : 1364
}, {
  "spanish" : "cualificar",
  "translations" : {
    "ENGLISH" : [ "qualify" ]
  },
  "regular" : false,
  "views" : 1363
}, {
  "spanish" : "encoger",
  "translations" : {
    "ENGLISH" : [ "shrink" ]
  },
  "regular" : false,
  "views" : 1363
}, {
  "spanish" : "asear",
  "translations" : {
    "ENGLISH" : [ "tidy up", "clean" ]
  },
  "regular" : true,
  "views" : 1361
}, {
  "spanish" : "emprender",
  "translations" : {
    "ENGLISH" : [ "undertake" ]
  },
  "regular" : true,
  "views" : 1361
}, {
  "spanish" : "transcribir",
  "translations" : {
    "ENGLISH" : [ "transcribe" ]
  },
  "regular" : false,
  "views" : 1360
}, {
  "spanish" : "agobiar",
  "translations" : {
    "ENGLISH" : [ "overwhelm", "get too much for" ]
  },
  "regular" : true,
  "views" : 1359
}, {
  "spanish" : "cavar",
  "translations" : {
    "ENGLISH" : [ "dig" ]
  },
  "regular" : true,
  "views" : 1359
}, {
  "spanish" : "disuadir",
  "translations" : {
    "ENGLISH" : [ "dissuade" ]
  },
  "regular" : true,
  "views" : 1358
}, {
  "spanish" : "exceder",
  "translations" : {
    "ENGLISH" : [ "exceed" ]
  },
  "regular" : true,
  "views" : 1358
}, {
  "spanish" : "tumbar",
  "translations" : {
    "ENGLISH" : [ "lie down" ]
  },
  "regular" : true,
  "views" : 1356
}, {
  "spanish" : "trabar",
  "translations" : {
    "ENGLISH" : [ "hobble", "hold shut" ]
  },
  "regular" : true,
  "views" : 1356
}, {
  "spanish" : "aceitar",
  "translations" : {
    "ENGLISH" : [ "oil" ]
  },
  "regular" : true,
  "views" : 1356
}, {
  "spanish" : "sobreexponer",
  "translations" : {
    "ENGLISH" : [ "overexpose" ]
  },
  "regular" : false,
  "views" : 1355
}, {
  "spanish" : "acuchillar",
  "translations" : {
    "ENGLISH" : [ "knife", "cut", "slash", "cut open" ]
  },
  "regular" : true,
  "views" : 1355
}, {
  "spanish" : "alterar",
  "translations" : {
    "ENGLISH" : [ "alter" ]
  },
  "regular" : true,
  "views" : 1354
}, {
  "spanish" : "desconectar",
  "translations" : {
    "ENGLISH" : [ "disconnect", "switch off" ]
  },
  "regular" : true,
  "views" : 1354
}, {
  "spanish" : "desplazar",
  "translations" : {
    "ENGLISH" : [ "displace", "move around" ]
  },
  "regular" : false,
  "views" : 1354
}, {
  "spanish" : "diferenciar",
  "translations" : {
    "ENGLISH" : [ "differenciate" ]
  },
  "regular" : true,
  "views" : 1354
}, {
  "spanish" : "estremecer",
  "translations" : {
    "ENGLISH" : [ "shudder" ]
  },
  "regular" : false,
  "views" : 1354
}, {
  "spanish" : "alborotar",
  "translations" : {
    "ENGLISH" : [ "mess" ]
  },
  "regular" : true,
  "views" : 1353
}, {
  "spanish" : "presumir",
  "translations" : {
    "ENGLISH" : [ "presume" ]
  },
  "regular" : true,
  "views" : 1353
}, {
  "spanish" : "apestar",
  "translations" : {
    "ENGLISH" : [ "corrupt", "sicken", "nauseate", "plague" ]
  },
  "regular" : true,
  "views" : 1352
}, {
  "spanish" : "designar",
  "translations" : {
    "ENGLISH" : [ "designate" ]
  },
  "regular" : true,
  "views" : 1352
}, {
  "spanish" : "retrasar",
  "translations" : {
    "ENGLISH" : [ "delay" ]
  },
  "regular" : true,
  "views" : 1351
}, {
  "spanish" : "reelegir",
  "translations" : {
    "ENGLISH" : [ "reelect" ]
  },
  "regular" : true,
  "views" : 1351
}, {
  "spanish" : "descuidar",
  "translations" : {
    "ENGLISH" : [ "neglect" ]
  },
  "regular" : true,
  "views" : 1350
}, {
  "spanish" : "descontar",
  "translations" : {
    "ENGLISH" : [ "deduct" ]
  },
  "regular" : false,
  "views" : 1349
}, {
  "spanish" : "sujetar",
  "translations" : {
    "ENGLISH" : [ "hold" ]
  },
  "regular" : true,
  "views" : 1349
}, {
  "spanish" : "propagar",
  "translations" : {
    "ENGLISH" : [ "propogate" ]
  },
  "regular" : false,
  "views" : 1349
}, {
  "spanish" : "sobornar",
  "translations" : {
    "ENGLISH" : [ "bribe" ]
  },
  "regular" : true,
  "views" : 1349
}, {
  "spanish" : "alertar",
  "translations" : {
    "ENGLISH" : [ "alert" ]
  },
  "regular" : true,
  "views" : 1347
}, {
  "spanish" : "descalzar",
  "translations" : {
    "ENGLISH" : [ "take your shoes off" ]
  },
  "regular" : false,
  "views" : 1347
}, {
  "spanish" : "retribuir",
  "translations" : {
    "ENGLISH" : [ "pay back" ]
  },
  "regular" : false,
  "views" : 1347
}, {
  "spanish" : "boxear",
  "translations" : {
    "ENGLISH" : [ "box" ]
  },
  "regular" : true,
  "views" : 1347
}, {
  "spanish" : "curiosear",
  "translations" : {
    "ENGLISH" : [ "glance at" ]
  },
  "regular" : true,
  "views" : 1347
}, {
  "spanish" : "profetizar",
  "translations" : {
    "ENGLISH" : [ "prophesy" ]
  },
  "regular" : false,
  "views" : 1347
}, {
  "spanish" : "sobreponer",
  "translations" : {
    "ENGLISH" : [ "superimpose" ]
  },
  "regular" : false,
  "views" : 1346
}, {
  "spanish" : "atrasar",
  "translations" : {
    "ENGLISH" : [ "slow down" ]
  },
  "regular" : true,
  "views" : 1346
}, {
  "spanish" : "acrisolar",
  "translations" : {
    "ENGLISH" : [ "refine", "purify" ]
  },
  "regular" : true,
  "views" : 1346
}, {
  "spanish" : "desaprobar",
  "translations" : {
    "ENGLISH" : [ "disapprove of", "reject" ]
  },
  "regular" : false,
  "views" : 1345
}, {
  "spanish" : "asimilar",
  "translations" : {
    "ENGLISH" : [ "assimilate" ]
  },
  "regular" : true,
  "views" : 1345
}, {
  "spanish" : "destituir",
  "translations" : {
    "ENGLISH" : [ "dismiss" ]
  },
  "regular" : false,
  "views" : 1344
}, {
  "spanish" : "murmurar",
  "translations" : {
    "ENGLISH" : [ "mutter", "gossip" ]
  },
  "regular" : true,
  "views" : 1344
}, {
  "spanish" : "desvariar",
  "translations" : {
    "ENGLISH" : [ "be delirious", "talk nonsense" ]
  },
  "regular" : false,
  "views" : 1342
}, {
  "spanish" : "asesorar",
  "translations" : {
    "ENGLISH" : [ "advise" ]
  },
  "regular" : true,
  "views" : 1342
}, {
  "spanish" : "derruir",
  "translations" : {
    "ENGLISH" : [ "knockdown", "demolish" ]
  },
  "regular" : false,
  "views" : 1341
}, {
  "spanish" : "trucar",
  "translations" : {
    "ENGLISH" : [ "rig", "fix", "tamper with" ]
  },
  "regular" : false,
  "views" : 1340
}, {
  "spanish" : "descentralizar",
  "translations" : {
    "ENGLISH" : [ "decentralise" ]
  },
  "regular" : false,
  "views" : 1337
}, {
  "spanish" : "excavar",
  "translations" : {
    "ENGLISH" : [ "dig" ]
  },
  "regular" : true,
  "views" : 1336
}, {
  "spanish" : "argumentar",
  "translations" : {
    "ENGLISH" : [ "reason", "dispute" ]
  },
  "regular" : true,
  "views" : 1336
}, {
  "spanish" : "contrastar",
  "translations" : {
    "ENGLISH" : [ "contrast" ]
  },
  "regular" : true,
  "views" : 1336
}, {
  "spanish" : "originar",
  "translations" : {
    "ENGLISH" : [ "originate" ]
  },
  "regular" : true,
  "views" : 1335
}, {
  "spanish" : "destapar",
  "translations" : {
    "ENGLISH" : [ "uncover" ]
  },
  "regular" : true,
  "views" : 1335
}, {
  "spanish" : "pronosticar",
  "translations" : {
    "ENGLISH" : [ "forecast" ]
  },
  "regular" : false,
  "views" : 1335
}, {
  "spanish" : "tamizar",
  "translations" : {
    "ENGLISH" : [ "sift", "sieve" ]
  },
  "regular" : false,
  "views" : 1335
}, {
  "spanish" : "predisponer",
  "translations" : {
    "ENGLISH" : [ "predispose" ]
  },
  "regular" : false,
  "views" : 1334
}, {
  "spanish" : "punzar",
  "translations" : {
    "ENGLISH" : [ "prick", "stab" ]
  },
  "regular" : false,
  "views" : 1334
}, {
  "spanish" : "restringir",
  "translations" : {
    "ENGLISH" : [ "restrict" ]
  },
  "regular" : false,
  "views" : 1333
}, {
  "spanish" : "transportar",
  "translations" : {
    "ENGLISH" : [ "carry" ]
  },
  "regular" : true,
  "views" : 1331
}, {
  "spanish" : "revolucionar",
  "translations" : {
    "ENGLISH" : [ "revolutionize" ]
  },
  "regular" : true,
  "views" : 1331
}, {
  "spanish" : "fomentar",
  "translations" : {
    "ENGLISH" : [ "promote", "encourage", "boost" ]
  },
  "regular" : true,
  "views" : 1331
}, {
  "spanish" : "proscribir",
  "translations" : {
    "ENGLISH" : [ "proscribe" ]
  },
  "regular" : false,
  "views" : 1330
}, {
  "spanish" : "turbar",
  "translations" : {
    "ENGLISH" : [ "worry", "disturb", "alarm" ]
  },
  "regular" : true,
  "views" : 1330
}, {
  "spanish" : "indignar",
  "translations" : {
    "ENGLISH" : [ "make angry", "outrage" ]
  },
  "regular" : true,
  "views" : 1330
}, {
  "spanish" : "presionar",
  "translations" : {
    "ENGLISH" : [ "pressurize", "press" ]
  },
  "regular" : true,
  "views" : 1330
}, {
  "spanish" : "repintar",
  "translations" : {
    "ENGLISH" : [ "repaint" ]
  },
  "regular" : true,
  "views" : 1329
}, {
  "spanish" : "loar",
  "translations" : {
    "ENGLISH" : [ "laud", "praise" ]
  },
  "regular" : true,
  "views" : 1328
}, {
  "spanish" : "rutilar",
  "translations" : {
    "ENGLISH" : [ "twinkle", "gleam", "shine" ]
  },
  "regular" : true,
  "views" : 1326
}, {
  "spanish" : "supervisar",
  "translations" : {
    "ENGLISH" : [ "supervise" ]
  },
  "regular" : true,
  "views" : 1325
}, {
  "spanish" : "ambicionar",
  "translations" : {
    "ENGLISH" : [ "aspire to" ]
  },
  "regular" : true,
  "views" : 1325
}, {
  "spanish" : "acedar",
  "translations" : {
    "ENGLISH" : [ "turn sour" ]
  },
  "regular" : true,
  "views" : 1325
}, {
  "spanish" : "compareceer",
  "translations" : {
    "ENGLISH" : [ "appear" ]
  },
  "regular" : false,
  "views" : 1324
}, {
  "spanish" : "plantear",
  "translations" : {
    "ENGLISH" : [ "raise" ]
  },
  "regular" : true,
  "views" : 1323
}, {
  "spanish" : "retroceder",
  "translations" : {
    "ENGLISH" : [ "go back" ]
  },
  "regular" : true,
  "views" : 1323
}, {
  "spanish" : "engrosar",
  "translations" : {
    "ENGLISH" : [ "swell" ]
  },
  "regular" : false,
  "views" : 1322
}, {
  "spanish" : "enunciar",
  "translations" : {
    "ENGLISH" : [ "enunciate", "express", "state" ]
  },
  "regular" : true,
  "views" : 1322
}, {
  "spanish" : "permutar",
  "translations" : {
    "ENGLISH" : [ "exchange" ]
  },
  "regular" : true,
  "views" : 1322
}, {
  "spanish" : "rotular",
  "translations" : {
    "ENGLISH" : [ "label" ]
  },
  "regular" : true,
  "views" : 1321
}, {
  "spanish" : "enmendar",
  "translations" : {
    "ENGLISH" : [ "rectify", "improve", "amend" ]
  },
  "regular" : false,
  "views" : 1321
}, {
  "spanish" : "intensificar",
  "translations" : {
    "ENGLISH" : [ "intensify" ]
  },
  "regular" : false,
  "views" : 1320
}, {
  "spanish" : "interponer",
  "translations" : {
    "ENGLISH" : [ "interpose" ]
  },
  "regular" : false,
  "views" : 1320
}, {
  "spanish" : "excretar",
  "translations" : {
    "ENGLISH" : [ "excrete" ]
  },
  "regular" : true,
  "views" : 1320
}, {
  "spanish" : "vislumbrar",
  "translations" : {
    "ENGLISH" : [ "discern", "make out" ]
  },
  "regular" : true,
  "views" : 1320
}, {
  "spanish" : "lloriquear",
  "translations" : {
    "ENGLISH" : [ "whimper", "whine" ]
  },
  "regular" : true,
  "views" : 1318
}, {
  "spanish" : "derribar",
  "translations" : {
    "ENGLISH" : [ "demolish" ]
  },
  "regular" : true,
  "views" : 1317
}, {
  "spanish" : "resguardar",
  "translations" : {
    "ENGLISH" : [ "safeguard" ]
  },
  "regular" : true,
  "views" : 1316
}, {
  "spanish" : "restablecer",
  "translations" : {
    "ENGLISH" : [ "reestablish" ]
  },
  "regular" : false,
  "views" : 1315
}, {
  "spanish" : "desistir",
  "translations" : {
    "ENGLISH" : [ "desist" ]
  },
  "regular" : true,
  "views" : 1315
}, {
  "spanish" : "superponer",
  "translations" : {
    "ENGLISH" : [ "superimpose" ]
  },
  "regular" : false,
  "views" : 1314
}, {
  "spanish" : "exhalar",
  "translations" : {
    "ENGLISH" : [ "exhale" ]
  },
  "regular" : true,
  "views" : 1314
}, {
  "spanish" : "amparar",
  "translations" : {
    "ENGLISH" : [ "shelter", "protect" ]
  },
  "regular" : true,
  "views" : 1314
}, {
  "spanish" : "desenfadar",
  "translations" : {
    "ENGLISH" : [ "soothe", "appease", "calm down" ]
  },
  "regular" : true,
  "views" : 1313
}, {
  "spanish" : "aturrullar",
  "translations" : {
    "ENGLISH" : [ "get in a state" ]
  },
  "regular" : true,
  "views" : 1313
}, {
  "spanish" : "exfoliar",
  "translations" : {
    "ENGLISH" : [ "exfoliate" ]
  },
  "regular" : true,
  "views" : 1312
}, {
  "spanish" : "trascribir",
  "translations" : {
    "ENGLISH" : [ "trascribe" ]
  },
  "regular" : false,
  "views" : 1311
}, {
  "spanish" : "conceptuar",
  "translations" : {
    "ENGLISH" : [ "consider", "judge" ]
  },
  "regular" : false,
  "views" : 1310
}, {
  "spanish" : "conllevar",
  "translations" : {
    "ENGLISH" : [ "convey" ]
  },
  "regular" : true,
  "views" : 1309
}, {
  "spanish" : "desandar",
  "translations" : {
    "ENGLISH" : [ "go back over", "retrace the trail" ]
  },
  "regular" : false,
  "views" : 1308
}, {
  "spanish" : "plumear",
  "translations" : {
    "ENGLISH" : [ "hatch" ]
  },
  "regular" : true,
  "views" : 1307
}, {
  "spanish" : "enganchar",
  "translations" : {
    "ENGLISH" : [ "hitch", "become addicted" ]
  },
  "regular" : true,
  "views" : 1306
}, {
  "spanish" : "recusar",
  "translations" : {
    "ENGLISH" : [ "reject", "challenge" ]
  },
  "regular" : true,
  "views" : 1306
}, {
  "spanish" : "desagradar",
  "translations" : {
    "ENGLISH" : [ "displease" ]
  },
  "regular" : true,
  "views" : 1306
}, {
  "spanish" : "reforzar",
  "translations" : {
    "ENGLISH" : [ "reinforce" ]
  },
  "regular" : false,
  "views" : 1305
}, {
  "spanish" : "exagerar",
  "translations" : {
    "ENGLISH" : [ "exaggerate" ]
  },
  "regular" : true,
  "views" : 1305
}, {
  "spanish" : "reformar",
  "translations" : {
    "ENGLISH" : [ "reform" ]
  },
  "regular" : true,
  "views" : 1302
}, {
  "spanish" : "exhumar",
  "translations" : {
    "ENGLISH" : [ "exhume" ]
  },
  "regular" : true,
  "views" : 1302
}, {
  "spanish" : "eructar",
  "translations" : {
    "ENGLISH" : [ "belch", "burp" ]
  },
  "regular" : true,
  "views" : 1300
}, {
  "spanish" : "repercutir",
  "translations" : {
    "ENGLISH" : [ "have an impact on" ]
  },
  "regular" : true,
  "views" : 1299
}, {
  "spanish" : "escurrir",
  "translations" : {
    "ENGLISH" : [ "wring" ]
  },
  "regular" : true,
  "views" : 1298
}, {
  "spanish" : "descifrar",
  "translations" : {
    "ENGLISH" : [ "decipher" ]
  },
  "regular" : true,
  "views" : 1298
}, {
  "spanish" : "enturbiar",
  "translations" : {
    "ENGLISH" : [ "go cloudy (liquids)" ]
  },
  "regular" : true,
  "views" : 1298
}, {
  "spanish" : "turbinar",
  "translations" : {
    "ENGLISH" : [ "harness" ]
  },
  "regular" : true,
  "views" : 1297
}, {
  "spanish" : "enrollar",
  "translations" : {
    "ENGLISH" : [ "roll", "get invloved" ]
  },
  "regular" : true,
  "views" : 1295
}, {
  "spanish" : "preceder",
  "translations" : {
    "ENGLISH" : [ "precede" ]
  },
  "regular" : true,
  "views" : 1295
}, {
  "spanish" : "preservar",
  "translations" : {
    "ENGLISH" : [ "preservar" ]
  },
  "regular" : true,
  "views" : 1293
}, {
  "spanish" : "regenerar",
  "translations" : {
    "ENGLISH" : [ "regenerate" ]
  },
  "regular" : true,
  "views" : 1291
}, {
  "spanish" : "desempolvar",
  "translations" : {
    "ENGLISH" : [ "revive", "resurrect", "dust off" ]
  },
  "regular" : true,
  "views" : 1291
}, {
  "spanish" : "tripular",
  "translations" : {
    "ENGLISH" : [ "man", "crew" ]
  },
  "regular" : true,
  "views" : 1288
}, {
  "spanish" : "consolidar",
  "translations" : {
    "ENGLISH" : [ "consolidate" ]
  },
  "regular" : true,
  "views" : 1287
}, {
  "spanish" : "ensanchar",
  "translations" : {
    "ENGLISH" : [ "widen" ]
  },
  "regular" : true,
  "views" : 1286
}, {
  "spanish" : "colorear",
  "translations" : {
    "ENGLISH" : [ "dye", "colour", "stain" ]
  },
  "regular" : true,
  "views" : 1284
}, {
  "spanish" : "entubar",
  "translations" : {
    "ENGLISH" : [ "channel", "pipe" ]
  },
  "regular" : true,
  "views" : 1279
}, {
  "spanish" : "desenroscar",
  "translations" : {
    "ENGLISH" : [ "unscrew" ]
  },
  "regular" : false,
  "views" : 1278
}, {
  "spanish" : "profanar",
  "translations" : {
    "ENGLISH" : [ "desecrate", "defile" ]
  },
  "regular" : true,
  "views" : 1276
}, {
  "spanish" : "resoplar",
  "translations" : {
    "ENGLISH" : [ "puff", "snort" ]
  },
  "regular" : true,
  "views" : 1274
}, {
  "spanish" : "trasvasar",
  "translations" : {
    "ENGLISH" : [ "decant" ]
  },
  "regular" : true,
  "views" : 1272
}, {
  "spanish" : "serruchar",
  "translations" : {
    "ENGLISH" : [ "saw" ]
  },
  "regular" : true,
  "views" : 1270
}, {
  "spanish" : "derrochar",
  "translations" : {
    "ENGLISH" : [ "waste", "squander" ]
  },
  "regular" : true,
  "views" : 1269
}, {
  "spanish" : "exprimir",
  "translations" : {
    "ENGLISH" : [ "squeeze" ]
  },
  "regular" : true,
  "views" : 1263
}, {
  "spanish" : "sobresaltar",
  "translations" : {
    "ENGLISH" : [ "startle", "make jump" ]
  },
  "regular" : true,
  "views" : 1262
}, {
  "spanish" : "derrumbar",
  "translations" : {
    "ENGLISH" : [ "knock down", "demolish" ]
  },
  "regular" : true,
  "views" : 1262
}, {
  "spanish" : "tunelar",
  "translations" : {
    "ENGLISH" : [ "tunnel" ]
  },
  "regular" : true,
  "views" : 1254
}, {
  "spanish" : "desperdigar",
  "translations" : {
    "ENGLISH" : [ "scatter" ]
  },
  "regular" : false,
  "views" : 1252
}, {
  "spanish" : "desenganchar",
  "translations" : {
    "ENGLISH" : [ "uncouple", "get off drugs" ]
  },
  "regular" : true,
  "views" : 1251
}, {
  "spanish" : "equilibrar",
  "translations" : {
    "ENGLISH" : [ "balance" ]
  },
  "regular" : true,
  "views" : 1250
}, {
  "spanish" : "sobrescribir",
  "translations" : {
    "ENGLISH" : [ "overwrite" ]
  },
  "regular" : false,
  "views" : 1246
}, {
  "spanish" : "regengtar",
  "translations" : {
    "ENGLISH" : [ "run a business" ]
  },
  "regular" : true,
  "views" : 1241
}, {
  "spanish" : "rebozar",
  "translations" : {
    "ENGLISH" : [ "coat in batter" ]
  },
  "regular" : false,
  "views" : 1240
}, {
  "spanish" : "expectorar",
  "translations" : {
    "ENGLISH" : [ "expectorate" ]
  },
  "regular" : true,
  "views" : 1238
}, {
  "spanish" : "untar",
  "translations" : {
    "ENGLISH" : [ "spread" ]
  },
  "regular" : true,
  "views" : 1237
}, {
  "spanish" : "exonerar",
  "translations" : {
    "ENGLISH" : [ "exonerate" ]
  },
  "regular" : true,
  "views" : 1227
}, {
  "spanish" : "lamer",
  "translations" : {
    "ENGLISH" : [ "lick" ]
  },
  "regular" : true,
  "views" : 1217
}, {
  "spanish" : "dañar",
  "translations" : {
    "ENGLISH" : [ "damage", "hurt" ]
  },
  "regular" : true,
  "views" : 11201
} ]);