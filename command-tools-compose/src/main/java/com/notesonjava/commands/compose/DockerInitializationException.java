package com.notesonjava.commands.compose;

public class DockerInitializationException extends Exception {

	public DockerInitializationException() {
		super();
	}

	public DockerInitializationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DockerInitializationException(String message, Throwable cause) {
		super(message, cause);
	}

	public DockerInitializationException(String message) {
		super(message);
	}

	public DockerInitializationException(Throwable cause) {
		super(cause);
	}

}
