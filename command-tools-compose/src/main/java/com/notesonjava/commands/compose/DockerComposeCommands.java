package com.notesonjava.commands.compose;

import java.util.ArrayList;
import java.util.List;

import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.CommandRunner;

public class DockerComposeCommands {


	
	
	public static CommandResult build(String... fileNames) {
		
		List<String> items = new ArrayList<>();
		items.add("docker-compose");
		items.addAll(buildFileParam(fileNames));
		items.add("build");
		return CommandRunner.execute(items.toArray(new String[items.size()]));
	}
	

	
	public static CommandResult up(String... fileNames) {
		
		List<String> items = new ArrayList<>();
		items.add("docker-compose");
		items.addAll(buildFileParam(fileNames));
		items.add("up");
		items.add("-d");
		return CommandRunner.execute(items.toArray(new String[items.size()]));
	}
	
	public static CommandResult down(String... fileNames) {
		
		List<String> items = new ArrayList<>();
		items.add("docker-compose");
		items.addAll(buildFileParam(fileNames));
		items.add("down");
		return CommandRunner.execute(items.toArray(new String[items.size()]));
	}

	
	private static List<String> buildFileParam(String...fileNames){
		List<String> items = new ArrayList<>();
		if(fileNames.length>0) {
			for(int i=0; i<fileNames.length; i++) {
				items.add("-f");
				items.add(fileNames[i]);
			}
		}
		return items;
	}
	
	
}
