package com.notesonjava.commands.compose;

import java.util.Optional;

import org.junit.rules.ExternalResource;

import com.notesonjava.commands.CommandResult;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DockerComposeRule extends ExternalResource {
	private boolean useCompose;

	@NonNull
	private String[] composeFiles;
	
	public DockerComposeRule(String... composeFiles) {
		this.composeFiles = composeFiles;
	}
	
	@Override
	protected void before() throws Throwable {
		PropertyMap props = PropertyLoader.loadProperties();
		useCompose = Boolean.parseBoolean(props.get("use.compose").orElse("true"));
		
		if (useCompose) {
			CommandResult buildResult = DockerComposeCommands.build(composeFiles);
			if (buildResult != null) {
				buildResult.getOutput().forEach(s -> log.info(s));
			}
			CommandResult composeResult = DockerComposeCommands.up(composeFiles);
			if (composeResult != null) {
				composeResult.getOutput().forEach(s -> log.info(s));
			}
		
			Optional<String> error = composeResult.getOutput().stream().filter(l -> l.contains("ERROR")).findFirst();
			if(error.isPresent()) {
				throw new DockerInitializationException(error.get());
			}
		}
	}

	@Override
	protected void after() {
		if (useCompose) {
			DockerComposeCommands.down(composeFiles);
		}
	}

}
