package com.notesonjava.commands.compose;

import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.docker.DockerCommands;
import com.notesonjava.commands.docker.DockerImage;
import com.notesonjava.commands.docker.DockerProcess;
import com.notesonjava.commands.docker.DockerResultParser;

public class DockerComposeCommandTest {
	
	@Test
	public void buildMongoImageWithSeedData() {
		
		
		String file = "testData/docker-compose-build.yml";
		
		String imageName = "testdata_verbs-test-db";
		CommandResult listImages = DockerCommands.listImages();
		List<DockerImage> images = DockerResultParser.parseImageLines(listImages.getOutput());
		boolean imageExist = images.stream().anyMatch(image -> image.getRepository().equals(imageName));
		if(imageExist) {
			DockerCommands.removeImage(imageName);
			
			listImages = DockerCommands.listImages();
			images = DockerResultParser.parseImageLines(listImages.getOutput());
			imageExist = images.stream().anyMatch(image -> image.getRepository().equals(imageName));
			if(imageExist) {
				Assertions.fail("Image exist and cannot be deleted");
			}
		}
		
		CommandResult buildResult = DockerComposeCommands.build(file);
		Assertions.assertThat(buildResult.getExitValue()).isEqualTo(0);
		
		
		CommandResult psResult = DockerCommands.listRunningProcesses();
		
		List<DockerProcess> runningProcesses = DockerResultParser.parseProcessLines(psResult.getOutput());
		Assertions.assertThat(runningProcesses.stream().noneMatch(process -> process.getImage().equals(imageName))).isTrue();
		
		CommandResult psAllResult = DockerCommands.listAllProcesses();
		List<DockerProcess> allProcesses = DockerResultParser.parseProcessLines(psAllResult.getOutput());
		Assertions.assertThat(allProcesses.stream().noneMatch(process -> process.getImage().equals(imageName))).isTrue();
		
		CommandResult imagesResult = DockerCommands.listImages();
		List<DockerImage> imageList = DockerResultParser.parseImageLines(imagesResult.getOutput());
		Assertions.assertThat(imageList.stream().anyMatch(image -> image.getRepository().equals(imageName))).isTrue();
		
		DockerComposeCommands.down(file);
		
	}

	@Test
	public void startAndStop() {
		CommandResult upResult = DockerComposeCommands.up("testData/docker-compose-mongo.yml");
		upResult.getOutput().forEach(System.out::println);
		CommandResult psResult = DockerCommands.listRunningProcesses();
		psResult.getOutput().forEach(System.out::println);
		
		CommandResult psAllResult = DockerCommands.listAllProcesses();
		psAllResult.getOutput().forEach(System.out::println);
		
		CommandResult downResult = DockerComposeCommands.down("testData/docker-compose-mongo.yml");
		downResult.getOutput().forEach(System.out::println);
				
	}
	
	@Test
	public void buildMultiFiles() {
		String dir = "testData/multifiles/";
		CommandResult upResult = DockerComposeCommands.up(dir+"docker-compose-db.yml", dir+"docker-compose-redis.yml", dir+"docker-compose-proxy.yml");
		upResult.getOutput().forEach(System.out::println);
		CommandResult psResult = DockerCommands.listRunningProcesses();
		psResult.getOutput().forEach(System.out::println);
		
		Optional<String> line = psResult.getOutput().stream().filter(l -> l.contains("test-db")).findAny();
		Assertions.assertThat(line.isPresent()).isTrue();
		
		String dbLine = line.get();
		Optional<DockerProcess> ps = DockerResultParser.parseProcessLine(dbLine);
		Assertions.assertThat(ps.isPresent()).isTrue();
		Assertions.assertThat(ps.get().getPorts()).isEqualTo("0.0.0.0:27019->27017/tcp");
		
		line = psResult.getOutput().stream().filter(l -> l.contains("redis")).findAny();
		Assertions.assertThat(line.isPresent()).isTrue();
		ps = DockerResultParser.parseProcessLine(line.get());
		
		Assertions.assertThat(ps.get().getPorts()).isEqualTo("6379-6380/tcp");
		
		CommandResult downResult = DockerComposeCommands.down(dir+"docker-compose-db.yml", dir+"docker-compose-redis.yml", dir+"docker-compose-proxy.yml");
		downResult.getOutput().forEach(System.out::println);
		
	}
	
	@Test
	public void buildOverride() {
		String dir = "testData/overrides/";
		CommandResult upResult = DockerComposeCommands.up(dir+"docker-compose-db.yml", dir+"docker-compose-override.yml");
		upResult.getOutput().forEach(System.out::println);
		
		CommandResult psResult = DockerCommands.listRunningProcesses();
		psResult.getOutput().forEach(System.out::println);
		Optional<String> line = psResult.getOutput().stream().filter(l -> l.contains("test-db")).findAny();
		Assertions.assertThat(line.isPresent()).isTrue();
		String dbLine = line.get();
		Optional<DockerProcess> ps = DockerResultParser.parseProcessLine(dbLine);
		Assertions.assertThat(ps.isPresent()).isTrue();
		Assertions.assertThat(ps.get().getPorts()).isEqualTo("0.0.0.0:27019->27017/tcp");
		
		CommandResult downResult = DockerComposeCommands.down(dir+"docker-compose-db.yml", dir+"docker-compose-override.yml");
		downResult.getOutput().forEach(System.out::println);
		
	}
	
	@Test
	public void buildDuplicate() {
		CommandResult upResult = DockerComposeCommands.up("testData/error/docker-compose-duplicate.yml");
		upResult.getOutput().forEach(System.out::println);
		Optional<String> error = upResult.getOutput().stream().filter(l -> l.contains("ERROR")).findFirst();
		Assertions.assertThat(error.isPresent()).isTrue();
	}
	
	@Test
	public void buildWrongImage() {
		CommandResult upResult = DockerComposeCommands.up("testData/error/docker-compose-wrong-image.yml");
		upResult.getOutput().forEach(System.out::println);
		Assertions.assertThat(upResult.isSuccess()).isFalse();
		Optional<String> error = upResult.getOutput().stream().filter(l -> l.contains("ERROR")).findFirst();
		Assertions.assertThat(error.isPresent()).isTrue();
	}
}
