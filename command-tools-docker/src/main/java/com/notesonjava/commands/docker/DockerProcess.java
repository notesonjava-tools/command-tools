package com.notesonjava.commands.docker;

import lombok.Data;

@Data
public class DockerProcess {

	private String containerId;
	private String image;
	private String command;
	private String created;
	private String status;
	private String ports;
	private String names;
}
