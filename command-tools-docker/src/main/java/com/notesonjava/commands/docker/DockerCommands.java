package com.notesonjava.commands.docker;

import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.CommandRunner;

public class DockerCommands {

	public static CommandResult listRunningProcesses() {
		return CommandRunner.executeCommand("docker ps");
	}

	public static CommandResult listAllProcesses() {
		return CommandRunner.executeCommand("docker ps -a");
	}
	
	public static CommandResult listImages() {
		return CommandRunner.executeCommand("docker images");
	}

	public static CommandResult removeImage(String nameOrId) {
		return CommandRunner.execute("docker", "rmi", nameOrId);
	}
	
	public static CommandResult removeProcess(String nameOrId) {
		return CommandRunner.execute("docker", "rm", nameOrId);
	}

	
	public static CommandResult listDanglingImages() {
		return CommandRunner.executeCommand("docker images -f dangling=true");		
	}
	
	public static CommandResult pruneDanglingImages() {
		return CommandRunner.execute("docker", "prune");
	}
	
	public static CommandResult pruneDanglingAndUnusedImages() {
		return CommandRunner.execute("docker", "prune", "-a");
	}
	
	public static CommandResult listNetworks() {
		return CommandRunner.executeCommand("docker network ls");
	}

	
	
}
