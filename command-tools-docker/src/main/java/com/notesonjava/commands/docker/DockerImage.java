package com.notesonjava.commands.docker;

import lombok.Data;

@Data
public class DockerImage {

	private String repository;
	private String tag;
	private String id;
	private String created;
	private String size;
}
