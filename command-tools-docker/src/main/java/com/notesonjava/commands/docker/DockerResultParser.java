package com.notesonjava.commands.docker;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DockerResultParser {

	
	private static final String DOUBLE_SPACE = "  ";
	private static final String TITLE_IMAGE_START = "REPOSITORY";
	
	
	private static final String TITLE_PROCESS_START = "CONTAINER";
	
	
	public static List<DockerImage> parseImageLines(List<String> lines){
		return lines.stream().map(line -> parseImageLine(line))
				.filter(image -> image.isPresent())
				.map(image -> image.get())
				.collect(Collectors.toList());
		
	}
	
	public static Optional<DockerImage> parseImageLine(String line){
		if(line.startsWith(TITLE_IMAGE_START)) {
			return Optional.empty();
		}
		
		//@formatter:off
		List<String> items = Arrays.asList(line.split(DOUBLE_SPACE))
				.stream().map(s -> s.trim())
				.filter(s -> s.length() > 0 )
				.collect(Collectors.toList());
		//@formatter:on

		if(items.size() == 5 ) {
			DockerImage image = new DockerImage();
			image.setRepository(items.get(0));
			image.setTag(items.get(1));
			image.setId(items.get(2));
			image.setCreated(items.get(3));
			image.setSize(items.get(4));
			return Optional.of(image);
		}
		return Optional.empty();
	}
	
	public static List<DockerProcess> parseProcessLines(List<String> lines){
		return lines.stream().map(line -> parseProcessLine(line))
				.filter(process -> process.isPresent())
				.map(process -> process.get())
				.collect(Collectors.toList());
		
	}
	
	
	public static Optional<DockerProcess> parseProcessLine(String line){
		if(line.startsWith(TITLE_PROCESS_START)) {
			return Optional.empty();
		}
		//@formatter:off
		List<String> items = Arrays.asList(line.split(DOUBLE_SPACE))
				.stream().map(s -> s.trim())
				.filter(s -> s.length() > 0 )
				.filter(s -> !s.startsWith(TITLE_PROCESS_START))
				.collect(Collectors.toList());
		//@formatter:on
		if(items.size() == 7 ) {
			DockerProcess process = new DockerProcess();
			process.setContainerId(items.get(0));
			process.setImage(items.get(1));
			process.setCommand(items.get(2));
			process.setCreated(items.get(3));
			process.setStatus(items.get(4));
			process.setPorts(items.get(5));
			process.setNames(items.get(6));
			
			return Optional.of(process);
		}
		return Optional.empty();
	}
	
	
	
	
}
