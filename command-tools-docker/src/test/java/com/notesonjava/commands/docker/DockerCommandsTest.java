package com.notesonjava.commands.docker;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.docker.DockerCommands;

public class DockerCommandsTest {

	@Test
	public void listImages() {
		CommandResult result = DockerCommands.listImages();
		result.getOutput().forEach(System.out::println);
		Assertions.assertThat(result.getExitValue()).isEqualTo(0);
		Assertions.assertThat(result.getOutput()).isNotEmpty();
		Assertions.assertThat(result.getOutput().get(0)).startsWith("REPOSITORY");
		
		
	}
	
	@Test
	public void listAllProcesses() {
		CommandResult result = DockerCommands.listAllProcesses();
		result.getOutput().forEach(System.out::println);
		Assertions.assertThat(result.getExitValue()).isEqualTo(0);
		Assertions.assertThat(result.getOutput()).isNotEmpty();
		Assertions.assertThat(result.getOutput().get(0)).startsWith("CONTAINER ID");
		
	}
	
	@Test
	public void listRunningProcesses() {
		CommandResult result = DockerCommands.listRunningProcesses();
		result.getOutput().forEach(System.out::println);
		Assertions.assertThat(result.getExitValue()).isEqualTo(0);
		Assertions.assertThat(result.getOutput()).isNotEmpty();
		Assertions.assertThat(result.getOutput().get(0)).startsWith("CONTAINER ID");
		
	}
	
	@Test
	public void listDanglingImages() {
		CommandResult result = DockerCommands.listDanglingImages();
		result.getOutput().forEach(System.out::println);
		Assertions.assertThat(result.getExitValue()).isEqualTo(0);
		Assertions.assertThat(result.getOutput()).isNotEmpty();
		Assertions.assertThat(result.getOutput().get(0)).startsWith("REPOSITORY");
	}
	
}
