package com.notesonjava.commands.docker;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class DockerProcessResultParserTest {
	
	private static final String TITLE_LINE = "CONTAINER ID        IMAGE         COMMAND        CREATED        STATUS         PORTS     NAMES"; 

	@Test
	public void titleLineReturnEmptyList() {
		Optional<DockerProcess> result = DockerResultParser.parseProcessLine(TITLE_LINE);
		Assertions.assertThat(result).isNotPresent();
	}
	
	@Test
	public void singleLine() {
		String line = "c2d3f8adc252  redis    \"docker-entrypoint.s…\"  2 months ago   Exited (255) 8 weeks ago   0.0.0.0:6380->6379/tcp     redis";

		Optional<DockerProcess> result = DockerResultParser.parseProcessLine(line);
		Assertions.assertThat(result).isPresent();
		DockerProcess process= result.get();
		Assertions.assertThat(process.getContainerId()).isEqualTo("c2d3f8adc252");
		Assertions.assertThat(process.getImage()).isEqualTo("redis");
		Assertions.assertThat(process.getCommand()).startsWith("\"docker-entrypoint");
		Assertions.assertThat(process.getCreated()).isEqualTo("2 months ago");
		Assertions.assertThat(process.getStatus()).startsWith("Exited (255)");
		Assertions.assertThat(process.getPorts()).isEqualTo("0.0.0.0:6380->6379/tcp");
		Assertions.assertThat(process.getNames()).startsWith("redis");
		
			
	}
	
	
	@Test
	public void multipleLines() {
		
		List<String> lines = new ArrayList<>();
		lines.add(TITLE_LINE);
		lines.add("c2d3f8adc252        redis      \"docker-entrypoint.s…\"   2 months ago        Exited (255) 8 weeks ago   0.0.0.0:6380->6379/tcp     redis");
		lines.add("4a29ae5449ad        mongo      \"docker-entrypoint.s…\"   2 months ago        Exited (255) 8 weeks ago   0.0.0.0:27017->27017/tcp   transactions-db");	
		
		List<DockerProcess> result = DockerResultParser.parseProcessLines(lines);
		Assertions.assertThat(result).hasSize(2);
		
	}
}
