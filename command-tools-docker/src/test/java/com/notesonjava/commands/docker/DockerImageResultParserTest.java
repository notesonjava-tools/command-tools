package com.notesonjava.commands.docker;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class DockerImageResultParserTest {
	private static final String TITLE_LINE = "REPOSITORY        TAG    IMAGE ID   CREATED   SIZE";

	@Test
	public void titleLineReturnEmptyList() {
		Optional<DockerImage> result = DockerResultParser.parseImageLine(TITLE_LINE);
		Assertions.assertThat(result).isNotPresent();
	}
	
	@Test
	public void singleLineWithLatestTag() {
		String line = "testdata_verbs-test-db      latest     511c2671547d    2 hours ago   366MB";

		Optional<DockerImage> result = DockerResultParser.parseImageLine(line);
		Assertions.assertThat(result).isPresent();
		DockerImage image = result.get();
		Assertions.assertThat(image.getRepository()).isEqualTo("testdata_verbs-test-db");
		Assertions.assertThat(image.getTag()).isEqualTo("latest");
		Assertions.assertThat(image.getId()).isEqualTo("511c2671547d");
		Assertions.assertThat(image.getCreated()).isEqualTo("2 hours ago");
		Assertions.assertThat(image.getSize()).isEqualTo("366MB");
		
		
			
	}
	
	@Test
	public void singleLineWithNumericTag() {
		String line = "notesonjava/bitstamp-tx-reader       0.0.1        969442893fb2     5 weeks ago      98.4MB";
		Optional<DockerImage> result = DockerResultParser.parseImageLine(line);
		Assertions.assertThat(result).isPresent();
		DockerImage image = result.get();
		Assertions.assertThat(image.getRepository()).isEqualTo("notesonjava/bitstamp-tx-reader");
		Assertions.assertThat(image.getTag()).isEqualTo("0.0.1");
		Assertions.assertThat(image.getId()).isEqualTo("969442893fb2");
		Assertions.assertThat(image.getCreated()).isEqualTo("5 weeks ago");
		Assertions.assertThat(image.getSize()).isEqualTo("98.4MB");
		
	}
	
	
	@Test
	public void multipleLines() {
		
		List<String> lines = new ArrayList<>();
		lines.add(TITLE_LINE);
		lines.add("testdata_verbs-test-db            latest         511c2671547d    2 hours ago     366MB");		
		lines.add("verbsservice_verbs-db             latest         8b848e23a6b3    6 days ago      366MB");		
		lines.add("notesonjava/bitstamp-tx-reader    0.0.1          969442893fb2    5 weeks ago     98.4MB");		
		
		
		List<DockerImage> result = DockerResultParser.parseImageLines(lines);
		Assertions.assertThat(result).hasSize(3);
		
		
		
		
	}
	
	
}
