package com.notesonjava.commands;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class StringSplitTest {
	
	@Test
	public void splitMultipleSpaces() {
		String cmd = "a  b";
		List<String> tokenList = CommandRunner.tokenize(cmd);
		Assertions.assertThat(tokenList).hasSize(2);
		
	}

}
