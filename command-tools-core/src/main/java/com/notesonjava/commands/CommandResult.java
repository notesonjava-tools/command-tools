package com.notesonjava.commands;

import java.util.ArrayList;
import java.util.List;

//@Data
public class CommandResult {
	
	private boolean success;
	private int exitValue;
	
	private List<String> output;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public int getExitValue() {
		return exitValue;
	}

	public void setExitValue(int exitValue) {
		this.exitValue = exitValue;
	}

	public List<String> getOutput() {
		if(output == null) {
			return new ArrayList<>();
		}
		return output;
	}

	public void setOutput(List<String> output) {
		this.output = output;
	}

}
