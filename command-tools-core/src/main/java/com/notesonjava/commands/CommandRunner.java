package com.notesonjava.commands;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommandRunner {

	public static CommandResult executeCommand(String command) {
		List<String> tokenList = tokenize(command);
		return execute(tokenList.toArray(new String[tokenList.size()]));
	}

	public static CommandResult execute(String... command) {

		CommandResult result = new CommandResult();

		ProcessBuilder pb = new ProcessBuilder(command);

		pb.redirectErrorStream(true);
		pb.directory(Paths.get(".").toFile());

		List<String> output = new ArrayList<>();
		ExecutorService executor = Executors.newSingleThreadExecutor();
		
		try {
			Process pr = pb.start();

			Scanner scanner = new Scanner(pr.getInputStream());
			while (hasNextLine(scanner, 1, TimeUnit.MINUTES, executor)) {
				String token = scanner.nextLine();
				log.info(token);
				output.add(token);
			}
			scanner.close();
			int exitVal = pr.waitFor();

			result.setExitValue(exitVal);
			if (exitVal == 0) {
				result.setSuccess(true);
			}
	
			result.setOutput(output);
		} catch (TimeoutException e) {
			log.error("The process didn't produce any output for more than 1 minute");
			output.add("ERROR: The process didn't produce any output for more than 1 minute.  Verify that it doesn't ask a question.  It can happen if you ask an docker image that do not exist.");
			result.setSuccess(false);
			result.setOutput(output);
		} catch (Exception e) {
			log.error("Failed to execute command", e);
			output.add("ERROR: " + e.getMessage());
			result.setSuccess(false);
			result.setOutput(output);
		}

		return result;

	}

	public static List<String> tokenize(String command) {
		String[] tokens = command.split(" ");
		return Arrays.asList(tokens).stream().map(s -> s.trim()).filter(s -> s.length() > 0)
				.collect(Collectors.toList());
	}

	public static List<String> fileList(String directory) {
		List<String> fileNames = new ArrayList<>();
		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(directory))) {
			for (Path path : directoryStream) {
				fileNames.add(path.toString());
			}
		} catch (IOException ex) {
			
		}
		return fileNames;
	}
	
	private static boolean hasNextLine(Scanner scanner, long timeout, TimeUnit timeoutUnit, ExecutorService executor) throws Exception {
		
		log.debug("Reading line : " + ZonedDateTime.now(ZoneOffset.UTC));
		long start = System.currentTimeMillis();
		Future<Boolean> future = executor.submit(() -> scanner.hasNextLine());
		boolean result = future.get(timeout, timeoutUnit);
		if(log.isDebugEnabled()) {
			long end = System.currentTimeMillis();
			log.debug("Total time : " + (end - start));
		}
		return result;
	}

}
